#!/usr/bin/python -u
# -*- mode: python; coding: utf-8 -*-

import sys
import gtk

import atheist
from atheist.const import *
from atheist.manager import Manager
import pyarco.UI

pyarco.UI.get_ostream().color = False


def pretty(value):

    RESULT_STR = {\
        FAIL:      ('#f77', 'FAIL'),
        OK:        ('green',   'OK'),
        NOEXEC:    ('#fff',  '--'),
        ERROR:     ('#f00',  '!!'),
        UNKNOWN:   ('#ccc' , '??'),
        TODO:      ('#fbf',  'ToDo'),
        mild_FAIL: ('#fcc',  'fail')}

    return RESULT_STR[value]


class AtheistGui:

    def __init__(self):
        gui = gtk.Builder()
        gui.add_from_file('gui.xml')

        window = gui.get_object('window')
        window.connect('delete-event', gtk.main_quit)
        window.show_all()

        mng = Manager(sys.argv[1:])
        mng.reload()
        i = 0

        self.store = gui.get_object('tasks')

        for case in mng.itercases():
            bgcolor, result = pretty(case.result)

            iter_ = self.store.append(None)
            self.store.set(iter_,
                           1, settings.relpath(case.fname),
                           2, "Case",
                           4, result,
                           5, bgcolor,
                           6, False)

            for task in case.tasks:
                cmd_desc = task.cmd if hasattr(task, 'cmd') else task.desc

                bgcolor, result = pretty(i)
                if i < 6: i += 1

                child_iter = self.store.append(iter_)
                self.store.set(child_iter,
                               0, task.indx,
                               1, pyarco.UI.ellipsis(cmd_desc),
                               2, task.acro,
                               3, cmd_desc.replace('\n', '\n\t') + '\n---\n' + task.desc,
                               4, result,
                               5, bgcolor,
                               6, False)



AtheistGui()
gtk.main()
