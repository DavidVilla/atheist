Source: atheist
Section: python
Priority: optional
Maintainer: David Villa Alises <David.Villa@gmail.com>
Uploaders: Francisco Moya <paco@debian.org>
Homepage: https://savannah.nongnu.org/projects/atheist/
Build-Depends: cdbs, debhelper (>= 7.0.50~), python-sphinx (>= 0.6.2),
 python-support, python, xsltproc (>= 1.1.24), python-docutils, python-jinja2
Standards-Version: 3.9.3
Vcs-Svn: svn://anonscm.debian.org/python-apps/packages/atheist/trunk/
Vcs-Browser: http://anonscm.debian.org/viewvc/python-apps/packages/atheist/trunk/


Package: python-pyarco
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}
Replaces: atheist (<< 0.20110402-1)
Breaks: atheist (<< 0.20110402-1)
Description: Useful utilities library for Python programmers
 PyArco is a set of classes, functions and utilities for Python
 programming. It is divided in different packages for multiples
 purposes: terminal and console formatting, network programming, file
 system helpers, high level threading structures like ThreadPool,
 multiples useful data structures, etc.

Package: atheist
Architecture: all
Suggests: atheist-plugins (>= 0.20110402-1)
Depends: ${misc:Depends}, ${python:Depends},
 libjs-jquery, nmap, python-pyarco (= ${binary:Version}), python-configobj
Description: General purpose command-line testing tool
 Atheist allows you to specify the behaviour of your tests in a
 declarative way using many predefined structures (such as Task,
 Conditions, Daemons, etc.) and provides detailed statistics.
 .
 The programmer describes the test behaviour in a .test file and
 atheist runs every test found. It is possible to check and prepare
 the system by writing setup and teardown files that are executed
 after and before each test.

Package: atheist-plugins
Architecture: all
Replaces: atheist (<< 0.20110402-1)
Breaks: atheist (<< 0.20110402-1)
Depends: ${misc:Depends},
 	 atheist (= ${binary:Version}),
 	 atheist-cxxtest (= ${binary:Version}),
 	 atheist-debian (= ${binary:Version}),
 	 atheist-extra-notifiers (= ${binary:Version}),
 	 atheist-inotify-runner (= ${binary:Version}),
 	 atheist-junit (= ${binary:Version}),
 	 atheist-webtest (= ${binary:Version}),
Description: Metapackage for Atheist plugins
 Atheist allows you to specify the behaviour of your tests in a
 declarative way using many predefined structures (such as Task,
 Conditions, Daemons, etc.) and provides detailed statistics.
 .
 The programmer describes the test behaviour in a .test file and
 atheist runs every test found. It is possible to check and prepare
 the system by writing setup and teardown files that are executed
 after and before each test.

Package: atheist-debian
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, atheist, pbuilder, piuparts
Conflicts: atheist (= 0.20110402-1)
Description: Plugin for testing common tasks in Debian environments and packaging.
 Atheist allows you to specify the behaviour of your tests in a
 declarative way using many predefined structures (such as Task,
 Conditions, Daemons, etc.) and provides detailed statistics.
 .
 The programmer describes the test behaviour in a .test file and
 atheist runs every test found. It is possible to check and prepare
 the system by writing setup and teardown files that are executed
 after and before each test.
 .
 This plugin provides utilities for testing applications in a
 Debian-specific environment:
  * DebPkgInstalled: condition that checks if a certain package is installed.
  * DebPkgBuild: task for testing the building process of a package.
  * DebPkgInstall: task for checking the install process of a package.


Package: atheist-extra-notifiers
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, atheist, python-loggingx
Replaces: atheist (<< 0.20110402-1)
Breaks: atheist (<< 0.20110402-1)
Description: More notifiers and loggers for Atheist
 Atheist allows you to specify the behaviour of your tests in a
 declarative way using many predefined structures (such as Task,
 Conditions, Daemons, etc.) and provides detailed statistics.
 .
 The programmer describes the test behaviour in a .test file and
 atheist runs every test found. It is possible to check and prepare
 the system by writing setup and teardown files that are executed
 after and before each test.
 .
 This plugin for Atheist allows one to test access to webserver and apply
 content conditions to the files that it holds.
 .
 This package includes the next plugins:
 - TSL capable SMTP notifier.
 - Jabber XMPP notifier.

Package: atheist-inotify-runner
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, atheist, python-pyinotify
Replaces: atheist (<< 0.20110402-1)
Breaks: atheist (<< 0.20110402-1)
Description: Runner plugin for re-executing tests when files change
 Atheist allows you to specify the behaviour of your tests in a
 declarative way using many predefined structures (such as Task,
 Conditions, Daemons, etc.) and provides detailed statistics.
 .
 The programmer describes the test behaviour in a .test file and
 atheist runs every test found. It is possible to check and prepare
 the system by writing setup and teardown files that are executed
 after and before each test.
 .
 This plugin allows one to watch files (also the tests themselves) and
 execute tests whenever they change.

Package: atheist-cxxtest
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, atheist, libcxxtest-dev
Description: CxxTest plugin for Atheist
 This plugin for Atheist integrates C/C++ unit tests by means the cxxtest
 framework.

Package: atheist-webtest
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, atheist, curl
Replaces: atheist (<< 0.20110402-1)
Breaks: atheist (<< 0.20110402-1)
Description: Web-testing plugins for Atheist
 Atheist allows you to specify the behaviour of your tests in a
 declarative way using many predefined structures (such as Task,
 Conditions, Daemons, etc.) and provides detailed statistics.
 .
 The programmer describes the test behaviour in a .test file and
 atheist runs every test found. It is possible to check and prepare
 the system by writing setup and teardown files that are executed
 after and before each test.
 .
 This plugin for Atheist allows one to test access to webserver and apply
 content conditions to the files that it holds.

Package: atheist-junit
Architecture: all
Depends: ${misc:Depends}, ${python:Depends}, atheist (= ${binary:Version}), junit4
Description: JUnit plugin for Atheist
 This plugin for Atheist integrates Java unit tests by means the JUnit
 framework.
