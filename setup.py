#!/usr/bin/env python

# Copyright (C) 2009 David Villa Alises
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


from distutils.core import setup
from atheist.const import VERSION

setup(name         = 'atheist',
      version      = VERSION,
      description  = 'Atheist is a general purpose test framework written in Python.',
      author       = 'David Villa Alises',
      author_email = '<David.Villa@uclm.es>',
      url          = 'https://savannah.nongnu.org/projects/atheist/',
      license      = 'GPL v2 or later',
      data_files   = [('share/man/man1', ['debian/atheist.1']),
                      ('share/atheist',
                       ['atheist/plugins/HTML_Reporter/html-report.tmpl',
                        'atheist/plugins/HTML_Reporter/style.css'])
                      ],
      packages     = ['atheist', 'atheist.plugins',
                      'atheist.plugins.HTML_Reporter', 'pyarco']
      )
