# -*- mode:python; coding:utf-8 -*-

import os

def resolve_path(fname, paths, find_all=False):
    '''Busca fname en las rutas indicadas y devuelve la primera ruta
    completa dónde lo encuentre. Todas si se indica find_all'''

    retval = []
    for p in paths:
        path = os.path.join(p, fname)
        if os.path.exists(path):
            if find_all:
                retval.append(path)
            else:
                return [path]

    return retval
