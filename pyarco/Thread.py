# -*- mode: python; coding: utf-8 -*-

"""threads module provides useful structures for concurrent and
threading programming tools.

.. moduleauthor:: Arco Research Group

"""
from __future__ import with_statement

import threading
import logging
import time
import Queue


# there is a equivalente class in commodity.thread_
class ThreadFunc(threading.Thread):
    """If it is needed to execute the function on another thread and stop is
    requiered. Otherwise, use thread.start_new_thread
    """

    def __init__(self, target, *args, **kargs):
        start = kargs.get('start', True)
        if 'start' in kargs:
            del kargs['start']

        threading.Thread.__init__(self,
                                  target=target,
                                  name=target.__name__,
                                  args=args, kwargs=kargs)
        if start:
            self.start()


class ThreadTimeout(threading.Thread):
    """ Run a function each interval seconds. Function must return
    'True' to continue executing. Based on threding.Timer class """

    def __init__(self, interval, function, args=[], at_init=False):
        threading.Thread.__init__(self, name=function.__name__)
        self.interval = interval  # secs
        self.function = function
        self.args = args
        self.at_init = at_init
        self.finished = threading.Event()
        self.play = threading.Event()
        self.start()

    def __del__(self):
        self.cancel()

    def cancel(self):
        "terminates the thread"
        self.play.set()
        self.finished.set()

    def pause(self):
        self.play.clear()

    def play(self):
        self.play.set()

    def run(self):
        if self.at_init and not self.function(*self.args):
            return

        while 1:
            self.finished.wait(self.interval)
            if self.finished.isSet():
                break

            if not self.function(*self.args):
                break


class EventPool(threading.Thread):
    '''Event pool based exclusively upon threads.'''

    class Task:
        def __init__(self, ctx, interval, func, args=[]):
            self.ctx = ctx
            self.interval = interval
            self.function = func
            self.args = args
            self.diff = interval

        def __repr__(self):
            return "%s.%s i:%s, d:%s" % (self.ctx.name, self.function.__name__,
                                          self.interval, self.diff)

    class Context:
        def __init__(self, poll, persistent, name=''):
            self.pool = poll
            self.persistent = persistent
            self.name = name
            self.tasks = []
            self.reset = self.add

        def add_func(self, interval, func, *args):
            retval = EventPool.Task(self, interval, func, args)
            self.add(retval)
            return retval

        def new_task(self, interval, function, *args):
            return EventPool.Task(self, interval, function, args)

        def add(self, task):
            if task not in self.tasks:
                self.tasks.append(task)

            self.pool.add_task(task)
            return task

        def cancel(self, task=None):
            if isinstance(task, EventPool.Task):
                try:
                    self.tasks.remove(task)
                except ValueError:
                    return

                self.pool.cancel_task_now(task)
                return

            if task == None:
                task = self.tasks[:]

            if isinstance(task, list):
                for t in task:
                    self.cancel(t)

        def __repr__(self):
            return repr(self.tasks)

    def __init__(self, persistent=True, name=None):
        threading.Thread.__init__(self, name=name)
        self.persistent = persistent
        self.name = name
        self.lock = threading.RLock()
        self.finished = threading.Event()
        self.__contexts = []
        self.__tasks = []
        self.__added = []

    def new_context(self, persistent=True, name=''):
        # deny if thread is running
        ctx = EventPool.Context(self, persistent, name)
        self.__contexts.append(ctx)
        return ctx

    def add_task(self, task):
        self.__added.append(task)

    def cancel_task_now(self, task):
        self.lock.acquire()

        if task in self.__added:
            self.__added.remove(task)
        else:
            self.__cancel_task(task)

        self.lock.release()

    def close(self):
        self.finished.set()

    def __add_task(self, new_task):
        total = 0
        for i, t in enumerate(self.__tasks):
            if total + t.diff > new_task.interval:
                new_task.diff = new_task.interval - total
                t.diff -= new_task.diff
                self.__tasks.insert(i,  new_task)
                return

            total += t.diff

        new_task.diff = new_task.interval - total
        self.__tasks.append(new_task)

    def __cancel_task(self, task):
        if task not in self.__tasks:
            return

        i = self.__tasks.index(task)
        if len(self.__tasks) > i + 1:
            self.__tasks[i + 1].diff += task.diff

        self.__tasks.remove(task)

    def __pending(self):
        for t in self.__added:
            if t in self.__tasks:
                self.__cancel_task(t)
            self.__add_task(t)

        self.__added = []

        # clear empty __contexts
        for c in self.__contexts:
            if not c.persistent and not c.tasks:
                self.__contexts.remove(c)

        if not self.persistent and not self.__contexts:
            self.finished.set()

    def run(self):
        while not self.finished.isSet():
            self.lock.acquire()
            self.__pending()

            while self.__tasks and self.__tasks[0].diff <= 0:
                task = self.__tasks[0]
                self.__tasks.remove(task)

                result = task.function(*task.args)

                if result:
                    self.__add_task(task)
                else:
                    try:
                        task.ctx.tasks.remove(task)
                    except ValueError:
                        pass

            if self.__tasks:
                self.__tasks[0].diff -= 1

            self.lock.release()
            time.sleep(1)


# from http://code.activestate.com/recipes/203871/
class ThreadPool:

    class JoiningEx:
        pass

    """ Flexible thread pool class.  Creates a pool of threads, then
    accepts tasks that will be dispatched to the next available
    thread """

    def __init__(self, numThreads):
        """Initialize the thread pool with numThreads workers """

        self.__threads = []
        self.__resizeLock = threading.Lock()
        self.__taskLock = threading.Condition(threading.Lock())
        self.__tasks = []
        self.__isJoining = False
        self.resize(numThreads)

    def resize(self, newsize):
        """ public method to set the current pool size """

        if self.__isJoining:
            raise ThreadPool.JoiningEx()

        with self.__resizeLock:
            self.__resize(newsize)

        return True

    def __resize(self, newsize):
        """Set the current pool size, spawning or terminating threads
        if necessary.  Internal use only; assumes the resizing lock is
        held."""

        diff = newsize - len(self.__threads)

        # If we need to grow the pool, do so
        for i in range(diff):
            self.__threads.append(ThreadPool.Worker(self))

        # If we need to shrink the pool, do so
        for i in range(-diff):
            thread = self.__threads.pop()
            thread.stop = True

    def __len__(self):
        """Return the number of threads in the pool."""

        with self.__resizeLock:
            return len(self.__threads)

    def add(self, task, args=(), callback=None):
        """Insert a task into the queue.  task must be callable;
        args and taskCallback can be None."""

        assert callable(task)

        if self.__isJoining:
            raise ThreadPool.JoiningEx()

        with self.__taskLock:
            self.__tasks.append((task, args, callback))
            self.__taskLock.notify()
            return True

    def nextTask(self):
        """ Retrieve the next task from the task queue.  For use
        only by ThreadPoolWorker objects contained in the pool """

        with self.__taskLock:
            while not self.__tasks:
                if self.__isJoining:
                    raise ThreadPool.JoiningEx()

                self.__taskLock.wait()

            assert self.__tasks
            return self.__tasks.pop(0)

    def join(self, waitForTasks=True, waitForThreads=True):
        """ Clear the task queue and terminate all pooled threads,
        optionally allowing the tasks and threads to finish """

        self.__isJoining = True  # prevent more task queueing

        if waitForTasks:
            while self.__tasks:
                time.sleep(0.1)

        with self.__resizeLock:
            if waitForThreads:
                with self.__taskLock:
                    self.__taskLock.notifyAll()

                for t in self.__threads:
                    t.join()

            # ready to reuse
            del self.__threads[:]
            self.__isJoining = False

    class Worker(threading.Thread):
        """ Pooled thread class """

        def __init__(self, pool):
            """ Initialize the thread and remember the pool. """

            threading.Thread.__init__(self)
            self.__pool = pool
            self.stop = False
            self.start()

        def run(self):
            """ Until told to quit, retrieve the next task and execute
            it, calling the callback if any.  """

            while not self.stop:
                try:
                    cmd, args, callback = self.__pool.nextTask()
                except ThreadPool.JoiningEx:
                    break

                logging.debug("thread %s taken %s" % (self, cmd))

                result = cmd(*args)
                if callback:
                    callback(result)


# migrated to commodity.thread_
class SimpleThreadPool:
    def __init__(self, numThreads):
        self.tasks = Queue.Queue()
        self.threads = [SimpleThreadPool.Worker(self.tasks) for x  in range(numThreads)]

    def add(self, func, args=(), callback=lambda x: x):
        assert callable(func)
        self.tasks.put((func, args, callback))

    def map(self, func, sequence):
        holders = [SimpleThreadPool.Holder() for x in range(len(sequence))]

        for value, callback in zip(sequence, holders):
            self.add(func, (value,), callback)
        self.join()

        return [x.value for x in holders]

    def join(self):
        self.tasks.join()

    class Worker(threading.Thread):
        def __init__(self, tasks):
            threading.Thread.__init__(self)
            self.tasks = tasks
            self.daemon = True
            self.start()

        def run(self):
            while 1:
                self.run_next()

        def run_next(self):
            func, args, callback = self.tasks.get()

            logging.debug("thread %s taken %s", self, func)
            result = func(*args)
            self.tasks.task_done()

            callback(result)

    class Holder:
        def __init__(self):
            self.value = None

        def __call__(self, arg):
            self.value = arg
