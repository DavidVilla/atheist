#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import ConfigParser


'''
sample file for methods docstrings:
[DEFAULT]
a = 1
[other]
b = 2
'''


class DictConfigParser(ConfigParser.SafeConfigParser):

    default_section = 'DEFAULT'

    class Section(object):
        def __init__(self, parser, name):
            self.parser = parser
            self.section_name = name

        def keys(self):
            return [k for k,v in self.items()]

        def items(self):
            return self.parser.items(self.section_name)

        def get(self, key):
            try:
                return self.parser.get(self.section_name, key)
            except (ConfigParser.NoOptionError, ConfigParser.NoSectionError):
                raise AttributeError(key)

        def set(self, key, value):
            self.parser.set(self.section_name, key, value)

        def __getattr__(self, key):
            return self.get(key)

        def __getitem__(self, key):
            return self.get(key)

        def __setitem__(self, key, value):
            self.set(key, value)

        def __str__(self):
            return "<Section '%s:%s'>" % (self.parser.fname, self.section_name)



    def __init__(self, fname=None):
        self.fname = fname
        ConfigParser.SafeConfigParser.__init__(self)
        if self.default_section != 'DEFAULT':
            self.new_section(self.default_section)
        if fname:
            self.read(fname)

    def keys(self):
        return self.Section(self, self.default_section).keys()


    def __resolve_path(self, path):
        if '.' in path:
            return path.split('.')

        return self.default_section, path


    def __getitem__(self, path):
        '''
        conf[a]
        conf[other.b]
        '''
        section, key = self.__resolve_path(path)
        return self.Section(self, section).get(key)


    def __setitem__(self, path, value):
        '''
        conf[a] = 5
        conf[other.b] = 5
        conf[new] = X
        conf[other.new] = X
        '''
        section, key = self.__resolve_path(path)
        self.Section(self, section).set(key, value)


    def __getattr__(self, section):
        '''
        conf.other
        '''
        if section.startswith('__'):
            raise AttributeError(section)

        if not self.has_section(section):
            raise ConfigParser.NoSectionError(section)

        return self.Section(self, section)


    def get_section(self, name):
        if not self.has_section(name):
            raise ConfigParser.NoSectionError(name)

        return self.Section(self, name)

    def new_section(self, name):
        if self.has_section(name):
            raise ConfigParser.DuplicateSectionError(name)

        self.add_section(name)
        return self.Section(self, name)


    def safe_get(self, path, default=None):
        try:
            return self[path]
        except AttributeError, e:
            if default is not None:
                return default
            raise
