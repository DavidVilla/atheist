# -*- mode: python; coding: utf-8 -*-

"""
Command line User Interface utilities

.. moduleauthor:: Arco Research Group
"""

import sys, os, string
import time
import logging

from pyarco.Conio import *

Log = logging.getLogger('pyarco.UI')


class ProgressBar:
    def __init__(self, max_val=100, width=80, label='', disable=False):
        self.max = max_val     # total item amount
        self.label = label       # prefix for the bar
        self.width = min(100, width - 37)  # bar width

        disable = disable or max_val < 4
        if disable:
            self.__disable()

        self.alfa = 0.2
        self.logger = None

        self.clean_len = 0
        self.reset()

    def reset(self):
        self.val = 0
        self.blk = 0
        self.pct = 0

        self.tinit = time.time()
        self.tlast = self.tinit
        self.eta = '~'
        self.lasteta = 0
#        self.inc()

    def __disable(self):
        self._render = lambda x: None
        self.clean = lambda: None
        self.inc = lambda: None
        self.abort = lambda: None

    def inc(self, val=1, cad=''):
        self.val += val
        if self.val > self.max:
            self.reset()

        self.blk = self.val * self.width / self.max
        self.pct = self.val * 100 / self.max
        current = time.time()
        if self.val > 3 and (current - self.lasteta > 1):
            per_item = (1-self.alfa) * ((current - self.tinit) / self.val) \
                + self.alfa * (current-self.tlast)
            remain = self.max  - self.val
            self.eta = 1 + int(1.1 * remain * per_item)
            self.lasteta = current
        self._render(cad)
        self.tlast = current

    def _render(self, ustr):
        cad = ':%4s [ %s' % (self.label[:4], self.blk * '#')
        cad += (self.width - self.blk) * '-'
        cad += " ] {0:{1}}/{2} ({3:0>3}%) {4:>3}s:{5:>3}s {6}\r".format(
            self.val,
            len(str(self.max)),
            self.max,
            self.pct,
            int(time.time() - self.tinit),
            self.eta,
            ellipsis(unicode(ustr)))

        self.clean_len = max(self.clean_len, len(cad))
        sys.__stdout__.write(cad)
        sys.__stdout__.flush()

    def clean(self):
        "clean the whole bar from screen"
#        clean_line = "%s%s\r" % (' ' * self.clean_len, '&')
        clean_line = "%s\r" % (' ' * self.clean_len)
        sys.__stdout__.write(clean_line)
        sys.__stdout__.flush()

    def listen_logger(self, logger, level):
        '''
        Register a monitor handler in the given logger.
        If it is invoqued, progress bar is aborted.
        '''
        class DummyHandler(logging.Handler):
            def __init__(self, pb, level):
                self._pb = pb
                self.level = level

            def handle(self, *args):
                self._pb.abort()

        if self.logger is None:
            self.logger = logger
            logger.addHandler(DummyHandler(self, level))

    def abort(self):
        print "progress-bar canceled by logging events."
        self.__disable()


def ellipsis(text, width=0, just=False, char=u'…'):
    """
    >>> ellipsis("this is a sentence", width=10, char='_')
    u'this is a_'
    >>> ellipsis("this is a ", width=12, just=True, char='_')
    u'this is a   '
    """

    if not text:
        text = u''

    if not isinstance(text, unicode):
        text = unicode(text, encoding='utf-8', errors='replace')

    cad = text.strip()
    retval = cad.split('\n')[0]

    if width > 0:
        width = int(width)
        retval = retval[:width - 1]

    if retval != cad:
        retval += char

    if just and width > 0:
        retval = retval.ljust(width)

    return retval


def clear():
    sys.stdout.write(CLS)
    sys.stdout.flush()
