# -*- mode: python; coding: utf-8 -*-

"""types module provides more powerful structures and functions than built-in

.. moduleauthor:: Arco Research Group

"""

import string
import csv
import inspect
import warnings
import collections
import itertools


class Record(object):
    "kargs are automatic attributes"

    def __init__(self, **kargs):
        self._attrs = kargs.keys()

        for k, v in kargs.items():
            setattr(self, k, v)

    def __str__(self):
        return "<{0!r}>".format(self)

    def __repr__(self):
        items = []
        for k in self._attrs:
            items.append("%s:'%s' " % (k, getattr(self, k)))

        return "Record({0})".format(str.join(', ', items))


class Bunch(dict):
    """dict keys are available as attributes
    >>> d = dict(aaa = dict(bbb=1), ccc=2)
    >>> deco = Bunch(d)
    >>> deco.ccc
    2
    >>> deco.ddd = 3
    >>> deco['ddd']
    3
    >>> deco['eee'] = 4
    >>> deco.eee
    4
    """
    def __init__(self, *args, **kw):
        super(Bunch, self).__init__(*args, **kw)
        self.__dict__ = self


# FIXME: this will be replaced with collections.OrderedDict in python2.7
class SortedDict(dict):
    """A fixed-position dictionary. The keys will be stored on a list in the
    same order as are inserted."""

    def __init__(self, other={}, default=None, **kargs):
        super(SortedDict, self).__init__()

        self.__keylist = []
        self.default = default
        if isinstance(other, dict):
            self.update(other)
        if isinstance(other, list):
            for key, val in other:
                self[key] = val

        for key, val in kargs.items():
            self[key] = val

    def __getitem__(self, key):
        if not key in dict.keys(self):
            if self.default is None:
                raise KeyError(key)

            self[key] = self.default

        return dict.__getitem__(self, key)

    def __setitem__(self, key, value):
        dict.__setitem__(self, key, value)
        if key in self.__keylist:
            self.__keylist.remove(key)
        self.__keylist.append(key)

    def update(self, other):
        for k, v in other.items():
            self[k] = v

    def keys(self):
        return self.__keylist

    def values(self):
        return [self[k] for k in self.__keylist]

    def items(self):
        return [(k, self[k]) for k in self.__keylist]

    def __iter__(self):
        return self.__keylist.__iter__()

    def clear(self):
        self.__keylist = []
        dict.clear(self)

    def __str__(self):
        return repr(self)

    def __repr__(self):
        items = []
        for key, val in self.items():
            items.append("'{0}': {1}".format(key, val))
        return '{' + str.join(', ', items) + '}'


#
# DEPRECATED: Use 'default' param in SortedDict
#
#class ListDict(SortedDict):
#    """A special case of a SortedDict. Here, if a key is unknown, it is created
#    en empty list for it."""
#
#    def __getitem__(self, key):
#        if not key in dict.keys(self):
#            self[key] = []
#        return SortedDict.__getitem__(self, key)


class DictTemplate:
    '''Read a template from file and generate a dict allowing substitutions

    template_file:
       full_name = Mr. $firstname %surname
       The $title = $value

    dt = DictTemplate('template_file', delimiter='=')
    result = t.substitute(dict(firstname='John', surname='Doe', title='job', value='fireman'))
    DictTemplate.render(result, 'out_file')

    out_file:
       full_name = Mr. John Doe
       The job = fireman
    '''

    def __init__(self, file_, delimiter=','):
        if isinstance(file_, str):
            self.fd = open(file_)
        else:
            self.fd = file_

        self.delimiter = delimiter

    def substitute(self, values):
        self.fd.seek(0)
        result = string.Template(self.fd.read()).safe_substitute(values)

        csv_file = csv.reader(result.split('\n'), delimiter=self.delimiter)

        retval = SortedDict()
        for row in csv_file:
            if not row or len(row) < 2:
                continue

            if any(['$' in x for x in row]):
                continue

            retval[row[0].strip()] = self.delimiter.join((row[1:])).strip()

        return retval

    @classmethod
    def render(cls, data, fname, delimiter=','):
        """Render dict 'data' for file 'fname' using 'delimiter' """
        assert isinstance(data, dict)

        with file(fname, 'w') as fd:
            for x in data.items():
                fd.write("%s = %s\n" % x)


def striplit(val, sep=' ', require_len=None):
    '''
    >>> striplit(" this -  is a -  sample  ", sep='-')
    ['this', 'is a', 'sample']
    '''

    val = val.strip(sep)
    retval = [x.strip() for x in val.split(sep)]
    if require_len is not None and len(retval) != require_len:
        raise ValueError("Incorrect size != %s" % require_len)
    return retval


def uniq(alist):
    '''
    >>> list(uniq([1, 2, 2, 3, 2, 3, 5]))
    [1, 2, 3, 5]
    '''

    s = set()
    for i in alist:
        if i in s:
            continue

        s.add(i)
        yield i


def merge(*args):
    """
    >>> merge([1,2], [2,4], [5, 6])
    [1, 2, 2, 4, 5, 6]
    >>> merge([[1,2], [2,4]])
    [[1, 2], [2, 4]]
    >>> merge(*[[1,2], [2,4]])
    [1, 2, 2, 4]
    """
    return reduce(list.__add__, args, list())


def merge_uniq(*args):
    """
    >>> merge_uniq([1,2], [2,4], [5, 6])
    [1, 2, 4, 5, 6]
    >>> merge_uniq([1,2])
    [1, 2]
    >>> merge_uniq(*[[1,2], [2,4]])
    [1, 2, 4]
    >>> merge_uniq([1, 2, 2, 3, 2, 3, 5])
    [1, 2, 3, 5]
    """
    return list(set(merge(*args)))


def split_if(seq, pred):
    """
    Split an iterable based on the a predicate.
    url: http://stackoverflow.com/questions/949098/python-split-a-list-based-on-a-condition

    >>> split_if(['a', '2', 'c', 'z', '5'], str.isdigit)
    [['2', '5'], ['a', 'c', 'z']]
    """

    retval = []
    for key, group in itertools.groupby(sorted(seq, key=pred, reverse=True),
                                       key=pred):
        retval.append(list(group))
    return retval


def get_supercls(cls):
    "Returns all ancestor superclasses as a list"
    return [cls] + merge(*[get_supercls(x) for x in cls.__bases__])


class FullWrapper(object):
    '''
    Simple modification over:

    module: method_decorator
    author: denis@ryzhkov.org
    license: free
    url: http://denis.ryzhkov.org/soft/python_lib/method_decorator.py

    Usage:
        class my_deco(FullWrapper):
            def pre(self, function, *args, **kwargs):
                print 'calling', function

        @my_deco
        def func_or_method(...)
    '''

    def __init__(self, func, obj=None, cls=None, method_type='function'):
        self.func = func
        self.obj = obj
        self.cls = cls
        self.method_type = method_type

    def __get__(self, obj=None, cls=None):
        if self.obj == obj and self.cls == cls:
            return self

        if isinstance(self.func, staticmethod):
            method_type = 'staticmethod'
        elif isinstance(self.func, classmethod):
            method_type = 'classmethod'
        else:
            method_type = 'instancemethod'

        return object.__getattribute__(self, '__class__')(
            self.func.__get__(obj, cls), obj, cls, method_type)

    def __call__(self, *args, **kwargs):
        self.decoration(self.func, *args, **kwargs)
        return self.func(*args, **kwargs)

    def decoration(self, func, *args, **kargs):
        pass

    def __getattribute__(self, attr_name):
        if attr_name in ('__init__', '__get__', '__call__', '__getattribute__',
                         'func', 'obj', 'cls', 'decoration', 'method_type'):

            return object.__getattribute__(self, attr_name)

        return getattr(self.func, attr_name)

    def __repr__(self):
        return self.func.__repr__()


class accept:
    '''
    usage:
    '''

    level = 'error'  # None, 'error', 'warn'

    def __init__(self, *types_as_values, **types_as_dict):
        self.types_as_values = types_as_values
        self.types_as_dict = types_as_dict

        if self.level is None:
            return

        class decorator(FullWrapper):
            def decoration(deco, function, *args, **kargs):
                self.check_types(deco, function, *args, **kargs)

        self.__call__ = decorator

    def __call__(self, function):
        'no decoration by default'
        return function

    def check_types(self, deco, function, *args, **kargs):

        def typerepr(typespec):
            if isinstance(typespec, type):
                return typespec.__name__

            if isinstance(typespec, tuple):
                return str.join('/', [typerepr(x) for x in typespec])

            raise TypeError("'{0}' is not a type or tuple of types".format(typespec))

        def get_argspec():
            args, varargs, keywords, defaults = inspect.getargspec(function)
            if deco.method_type in ['instancemethod', 'classmethod']:
                args = args[1:]  # skip self/cls

            return inspect.ArgSpec(args, varargs, keywords, defaults)

        def get_type_map():
            if self.types_as_dict:
                if self.types_as_values:
                    self.error("mix key and non-key args is forbidden")
                return self.types_as_dict

            if len(spec.args) != len(self.types_as_values):
                self.error("{0}() takes {1} arguments ({2} types given)".format(
                    function.__name__, len(spec.args), len(self.types_as_values)))

            return dict(zip(spec.args, self.types_as_values))

        def check_type_map():
            for arg_name in types_map.keys():
                if not arg_name in spec.args and arg_name != spec.varargs:
                    self.error("{0}() has not an argument '{1}'".format(
                            function.__name__, arg_name))

        def check_args():
#            print 'args 1:%s %s' % (len(args), args[1:])

            for i, arg_name in enumerate(spec.args):
                try:
                    value = kargs[arg_name]
                except KeyError:
                    try:
                        value = args[i]
                    except IndexError:
                        continue

                try:
                    expected_type = types_map[arg_name]
                except KeyError:
                    continue

#                print "%s %s '%s' %s" % (i, arg_name, value, expected_type)

                if not isinstance(value, expected_type):
                    self.error("Argument '{0}' should be '{1}' ('{2}' given)".format(\
                        arg_name, typerepr(expected_type), type(value).__name__))

        def check_varargs():
            if not spec.varargs:
                return

#           print types_map

            expected_type = types_map[spec.varargs]

            for value in args[len(spec.args):]:
                if not isinstance(value, expected_type):
                    self.error("Argument '{0}' should be '{1}' ('{2}' given)".format(\
                            value, expected_type.__name__, type(value).__name__))

        spec = get_argspec()
#        print spec

        types_map = get_type_map()
#        print types_map

        check_type_map()
        check_args()
        check_varargs()

    @classmethod
    def error(cls, msg):
        if cls.level == 'error':
            raise TypeError(msg)

        if cls.level == 'warn':
            warnings.warn(msg, RuntimeWarning)
            return

        raise TypeError("accept.level must be one of ['error', 'warn', None]")


def attributes(*arg_names):

    class decorator(FullWrapper):
        def decoration(self, function, *args, **kargs):

#            print inspect.getargspec(function)
            formal_args = inspect.getargspec(function).args[1:]  # skip self

            defaults = inspect.getargspec(function).defaults or []
            n_positionals = len(formal_args) - len(defaults)

            for name in arg_names:
                if not name in formal_args:
                    raise TypeError("'{0}()' method has no argument '{1}'".format(
                        function.__name__, name))

                pos = formal_args.index(name)

#                print "--\n%s %s %s" % (name, pos, n_positionals)
#                print args
#                print kargs

                try:
                    value = args[pos]
                except IndexError:
                    try:
                        value = kargs[name]
                    except KeyError:
                        value = defaults[pos - n_positionals]

                setattr(self.obj, name, value)

    return decorator


def check_type(val, cls):
    print 'pyarco.Type.check_type is deprecated, use checked_type'
    return checked_type(cls, val)


def checked_type(cls, val):
    if isinstance(val, cls):
        return val

    if isinstance(tuple, cls):
        expected = str.join(', ', [x.__name__ for x in cls])
    else:
        expected = cls.__name__

    raise TypeError("A {0} is required, not {1}: '{2}'.".format(
            expected,
            val.__class__.__name__, val))


#def checked_cast(cls, val):
#    try:
#        retval = cls(val)
#    except TypeError:
#        raise TypeError("{0} can not be casted to {1}".format(
#                val, cls))
#
#    return retval

class TypedAttr(object):
    """
    >>> class A(object):
    ...     a = TypedAttr('__a', str)
    ...
    >>> a1 = A()
    >>> a1.a = 'hi'
    """

    def __init__(self, name, cls):
        self.name = name
        self.cls = cls

    def __get__(self, obj, objtype):
        if (obj is None):
            raise AttributeError

        try:
            return obj.__dict__[self.name]
        except KeyError:
            raise AttributeError

    def __set__(self, obj, val):
        if not isinstance(val, self.cls):
            raise TypeError("'{0}' given but '{1}' expected".format(
                    val.__class__.__name__, self.cls.__name__))

        obj.__dict__[self.name] = val

#   def __delete__(self, obj):
#        if (obj.__dict__.has_key(self.value_name)):
#            del(obj.__dict__[self.value_name])


class DictPathDeco(collections.MutableMapping):
    """
    >>> d1 = dict(aaa = dict(bbb=1))
    >>> d2 = DictPathDeco(d1)
    >>> d2['aaa.bbb']
    1
    """

    def __init__(self, dct):
        self.dct = checked_type(collections.Mapping, dct)
        self.keys = dct.keys

    def __len__(self):
        return len(self.dct)

    def __iter__(self):
        return iter(self.dct)

    def __repr__(self):
        return repr(self.dct)

    def __getitem__(self, key):
        retval = self.dct

        for key in self.keypath(key):
            checked_type(collections.Mapping, retval)
            retval = retval[key]

        return retval

    def __getattr__(self, key):
        return getattr(self.dct, key)

    def __setitem__(self, key, value):
        dct = self.dct
        keys = self.keypath(key)

        for key in keys[:-1]:
            if not key in dct:
                dct[key] = {}

            dct = dct[key]

        dct[keys[-1]] = value

    def __delitem__(self, key):
        dct = self.dct
        keys = self.keypath(key)

        for key in keys[:-1]:
            dct = dct[key]

        del dct[keys[-1]]

    def has_key(self, key):
        dct = self.dct

        try:
            for key in self.keypath(key):
                dct = dct[key]
        except (KeyError, TypeError):
            return False

        return True

    def keypath(self, key):
        checked_type(str, key)
        return key.split('.')


# Provides same of Bunch
# class DictAttrDeco(collections.MutableMapping):
#     """dict keys are available as attributes
#     >>> d = dict(aaa = dict(bbb=1), ccc=2)
#     >>> deco = DictAttrDeco(d)
#     >>> deco.ccc
#     2
#     >>> deco.new = 4
#     >>> deco['new']
#     4
#     """
#
#     def __init__(self, dct):
#         self.__dict__['dct'] = checked_type(collections.Mapping, dct)
#
#     def __getitem__(self, key):
#         return self.dct[key]
#
#     def __setitem__(self, key, value):
#         self.dct[key] = value
#
#     def __delitem__(self, key):
#         del self.dct[key]
#
#     def __len__(self):
#         return len(self.dct)
#
#     def __iter__(self):
#         return iter(self.dct)
#
#     def __getattr__(self, key):
#         try:
#             return getattr(self.dct, key)
#         except AttributeError:
#             return self.dct[key]
#
#     def __setattr__(self, key, value):
#         self.dct[key] = value
#
#     def __repr__(self):
#         items = ["{0!r}: {1!r}".format(k, self.dct[k]) for k
#                  in sorted(self.dct.keys())]
#         return '{' + str.join(', ', items) + '}'
#
#     def copy(self):
#         return DictAttrDeco(self.dct.copy())


class Undefined(object):
    """Identify an absent object attribute"""

    def __init__(self, owner, attr):
        self.owner = owner
        self.attr = attr

    def __nonzero__(self):
        return False

    def __unicode__(self):
        return u"Undefined({0}, {1})".format(self.owner, self.attr)

    def __repr__(self):
        return "Undefined({0}, {1})".format(self.owner, self.attr)


def tag(name):
    """Set the given function attribute:

    >>> @tag('hidden')
    ... def func(args):
    ...     pass
    ...
    >>> func.hidden
    True
    """

    def wrap(f):
        setattr(f, name, True)
        return f
    return wrap


# Now in commodity.type_.module_to_dict
def load_module_as_dict(module):
    '''Return module global vars as a dict'''

    return dict([(k, v) for k, v in module.__dict__.items()
                 if not k.startswith('__') and not inspect.ismodule(v)])


class Call_when_changes:
    def __init__(self, callee, src):
        self.last_value = None
        self.src = src
        self.callee = callee

    def __call__(self):
        value = self.src()
        if value == self.last_value:
            return

        self.last_value = value
        self.callee(value)
