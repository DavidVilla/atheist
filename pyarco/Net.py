# -*- mode: python; coding: utf-8 -*-

"""net module provides functions for network programming

.. moduleauthor:: Arco Research Group

"""

import socket


def getFreePort(host=""):
    """Get a free port.

    :param host: The hostname.
    :type host: str.
    :returns:  int -- the free port.

    """
    s = socket.socket()
    s.bind((host, 0))
    port = s.getsockname()[1]
    s.close()
    return port
