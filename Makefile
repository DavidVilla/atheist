clean:
	find . -name *.pyc -delete
	find . -name *.pyo -delete
	find . -name *~ -delete
	$(MAKE) -C doc clean
	$(RM) atheist.1.gz  atheist.rst

vclean:
	$(RM) -r .svn debian
