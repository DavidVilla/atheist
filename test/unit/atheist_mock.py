# -*- mode:python; coding:utf-8; tab-width:4 -*-

import atheist

from mock import Mock


class TaskMock(object):
    def __init__(self):
        self.log = Mock()
        self.pre = atheist.condition.ConditionList('pre')
        self.post = atheist.condition.ConditionList('post')


class MockManager(atheist.iface.Manager):
    ts = []
    cfg = Mock()
    cfg.time_tag = ''
    cfg.screen_width = 80
    cfg.save_stdout = False
    cfg.save_stderr = False
    suite = Mock()
    suite.next = lambda: 0
