# -*- mode:python; coding:utf-8 -*-

import sys
import time
import threading
import unittest

from pyarco.Thread import ThreadPool, SimpleThreadPool

class TestPool(unittest.TestCase):
    def setUp(self):
        self.pool = ThreadPool(3)
        self.add_job()

    def add_job(self):
        for i in range(10):
            self.pool.add(time.sleep, [i/3])
        time.sleep(1)

    def test_increase(self):
        assert len(self.pool) == 3
        self.pool.resize(5)
        assert len(self.pool) == 5

    def test_decrease(self):
        assert len(self.pool) == 3
        self.pool.resize(2)
        assert len(self.pool) == 2

    def tearDown(self):
        self.pool.join()


class TestSimplePool(unittest.TestCase):

    def setUp(self):
        self.pool = SimpleThreadPool(3)

    def square(self, x):
        time.sleep(float(x)/4)
        return x*x


    def test_func_without_args(self):
        store = []
        self.pool.add(str, callback=store.append)
        self.pool.join()

        self.assertEquals(store[0], '')

    def test_no_callback(self):
        class Counter:
            def __init__(self):
                self.value = 0
                self.lock = threading.Lock()

            def inc(self):
                with self.lock:
                    self.value += 1

        counter = Counter()
        for i in range(3):
            self.pool.add(counter.inc)
        self.pool.join()

        self.assertEquals(counter.value, 3)

    def test_square_by_add(self):
        results = []

        for i in range(10):
            self.pool.add(self.square, (i,), results.append)
        self.pool.join()

        self.assertEquals(
            sorted(results),
            [0, 1, 4, 9, 16, 25, 36, 49, 64, 81])

    def test_square_by_map(self):
        results = self.pool.map(self.square, range(9,-1,-1))

        self.assertEquals(
            results,
            [81, 64, 49, 36, 25, 16, 9, 4, 1, 0])




#UnitTestCase(TestPool)
UnitTestCase(TestSimplePool)
