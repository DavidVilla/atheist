# -*- mode:python; coding:utf-8 -*-

import sys
import unittest

from pyarco.Pattern import ObjectObservableAsync


class Observer:
    def __init__(self):
        self.val = False

    def callback(self, value=None):
        self.val = value


class TestObserverAsyncSingleTopic(unittest.TestCase):

    def setUp(self):
        self.observable = ObjectObservableAsync({'topicTest':'callback'})
        self.observers = [Observer(), Observer(), Observer()]

    def test_subscribe_pos(self):
        try:
            for obs in self.observers:
                self.observable.attach(obs, 'topicTest')
            self.assertTrue(True)
        except ObjectObservableAsync.NotSuchTopic:
            self.assertTrue(False)

    def test_subscribe_neg(self):
        try:
            self.observable.attach(self.observers[0],
                                   'unexistTopic')
            self.assertTrue(False)

        except ObjectObservableAsync.NotSuchTopic:
            self.assertTrue(True)

    def test_unsubscribe(self):
        try:
            for obs in self.observers:
                self.observable.attach(obs, 'topicTest')
                self.observable.detach(obs, 'topicTest')
            self.assertTrue(True)
        except ObjectObservableAsync.NotSuchTopic:
            self.assertTrue(False)

    def test_unsubscribe_neg(self):
        try:
            self.observable.detach(self.observers[0],
                                        'unexistTopic')
            self.assertTrue(False)
        except ObjectObservableAsync.NotSuchTopic:
            self.assertTrue(True)

    def test_notify(self):
        for obs in self.observers:
            self.observable.attach(obs, 'topicTest')

        self.observable.notify('topicTest', True)

        from time import sleep
        sleep(0.5)

        for obs in self.observers:
            if not obs.val:
                self.assertTrue(False)

        self.assertTrue(True)



UnitTestCase(TestObserverAsyncSingleTopic)

