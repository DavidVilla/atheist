// MyTestSuite.h
#include <cxxtest/TestSuite.h>
class MyTestSuite : public CxxTest::TestSuite
{
public:
   void testAddition( void )
   {
      TS_ASSERT( 1 + 1 > 1 );
   }

   void testDiff( void )
   {
      TS_ASSERT( 1 - 1 == 0 );
   }

   void testMult(void)
   {
     TS_ASSERT((10*2) == 20);
   }

   void testDiv(void)
   {
     TS_ASSERT((10/2) == 5);
   }

};
