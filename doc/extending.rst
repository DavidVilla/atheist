
Extending atheist
==================

Atheist is easily expendable by means of plugins. It is possible to create
plugins for:

* Tasks
* Conditions
* Runners


Creating a Task plugin
----------------------

You may subclass Task to create new task types. The next are the Task methods
you may (or must) override in your subclass.

.. class:: Task([args,] **kargs)

   In the constructor, you can process specific args or keywords for you
   subclass. You can add/remove/modify standard keywords and finally you
   **must** call to Task.__init__().

   It is not mandatory to provide this method, but usually you will want
   specify arguments to your task by this way.

   It is important not to execute nothing expensive or that can fail. Preferably
   make attribute assignments only.

   .. attribute:: acro

      A 4-length string to abbreviate task class name.

   .. attribute:: allows

      A sequence of valid Task keywords names (as strings) for this task.

   .. attribute:: concurrent

      A boolean value indicating this task may be executed concurrently with
      others (default:``True``).

   .. method:: configure

   	  **[optional]**

	  Use that method to configure your task, usually by means of config
   	  files. It the config information is not valid nor complete, you may raise
   	  ``atheist.ConfigureError()`` to abort task.

   .. method:: pre_run

   	  **[optional]**

	  This method is invoked just before task execution. You can use that to create
   	  automatic conditions, execution time measurement, etc. Superclass
	  ``pre_run()`` **must be called** if that is provided.

   .. method:: is_running

   	  **[mandatory, but probably inherited]**

   	  This is required for detachable task only. If your class inherits from
   	  ``task.Subprocess`` mixin (as ``Test``) you do not need to override this
   	  method.

      Return a boolean indicating if task is already running, probably in a
   	  child process or thread.

   .. method:: terminate

      **[mandatory, but probably inherited]**

      This method must warranty the termination of a detachable task as soon as
      possible and free its resources. As ``is_running``, it is provided by the
      ``task.Subprocess`` mixin and you do not need to provide it.

   .. method:: run

      **[mandatory]**

      The operations that run the actual task. It must return a valid
      :ref:`task_results`.

   .. method:: repr_args

      **[optional]**

      Some brief information for textual representation of the *instance*.
      It may be useful values of relevant task arguments.



Creating a Condition plugin
---------------------------

.. class:: Condition(args)

   Do not perform any action in the constructor, store args only. **Do not
   forget** to call to the ``Condition`` constructor, that requires names of
   relevant instance attributes.

   .. method:: pre_task_run(task, condlist)

      **[optional]**

      This is invoked **just before** the related task execution:

      * task: the Task where this condition is.
      * condlist: the list of conditions where the condition is (pre/post)

   .. method:: run

      **[mandatory]**

      It actually checks the condition. It must return: OK, FAIL o ERROR.

   .. method:: repr_args

      **[optional]**

      Some brief information for textual representation of the *instance*.
      It may be useful values of relevant condition arguments.



.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
