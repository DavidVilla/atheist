.. Atheist documentation master file, created by
   sphinx-quickstart on Tue Aug  4 15:55:15 2009.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

=======================
Atheist's documentation
=======================

Atheist is a testing framework. Its goal is to integrate in a *single place* the
execution and result visualization of all project tests (any kind). Moreover,
atheist provides a small declarative language (a Python based DSL) to specify
system tests.

.. warning::

   This project is discontinued but bug-maintained for a while.

   Most of the system test DSL features with very similar design is provided by `prego
   <https://bitbucket.org/arco_group/prego>`_. Our advice is you migrate to prego.


.. toctree::
   :maxdepth: 2

   intro.rst
   install.rst
   tuto.rst
   ref.rst
   extending.rst
   faq.rst

   fdl.rst

..   examples.rst
..   other.rst



Indices and tables
==================

* :ref:`genindex`
* :ref:`search`

.. * :ref:`modindex


Copyright (C) 2009,2010,2011 David Villa Alises

Permission is granted to copy, distribute and/or modify this document
under the terms of the GNU Free Documentation License, Version 1.3
or any later version published by the Free Software Foundation;
with no Invariant Sections, no Front-Cover Texts, and no Back-Cover Texts.
A copy of the license is included in the section entitled "GNU
Free Documentation License".


.. Local variables:
..   coding: utf-8
..   ispell-local-dictionary: "american"
.. End:
