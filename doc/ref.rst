
.. _reference:

Reference and syntax
********************

Detailed information about all build-in atheist components.


.. include:: invoking.rst.inc
.. include:: task_types.rst.inc
.. include:: keywords.rst.inc
.. include:: conditions.rst.inc



Function utilities
==================

The next functions can be called from .test files.

.. function:: get_task(name)

   Returns the task whose ``tid`` attribute is *name*.
   ToDo: [include a sample here]

.. function:: load(filename)

   Makes possible to reuse atheist or python code in other
   files. ``load()`` returns a module-like object that may be used to
   access to functions, classes and variables defined in the "loaded"
   module. All atheist classes are available within the loaded modules::

    common = load("files/common.py")

    Test("./server %s" % common.server_params())

.. function:: temp_name()

   It returns a unique name for create a temporary file.


Variable substitutions
======================

Test files may include some substitutable variable. This is useful
to locate test-relevant related files. You must write the
symbol '$' preceding each one of the next words:

**basedir**
  It is the name of the directory where atheist was executed. Usually
  this is your project root directory.

**fname**
  The path to the testfile without its extension (.test). Useful to build
  paths to related files.

**fullbasedir**
  The same as ``basedir`` but indicated as a absolute path.

**fulltestdir**
  The absolute path version of ``testdir``.

**pid**
  The atheist process PID.

**testname**
  The name of the testfile, without extension nor directory path.

**testdir**
  The directory containing the current ``.test`` file.

**tmp**
  The atheist temporary directory: /tmp/$USER-$PWD

For example, for the following ``/home/john/devel/vars.test`` file::

     Test("echo $basedir $testdir $fname $testname")

When you run atheist, you get::

     john@host~$ atheist -i2 tests/vars.test
     [ OK ] TaskCase: ./devel/vars.test
     [ OK ] `- Test-1  -( 0: 0)  echo . ./devel ./devel/vars vars
     [  ALL OK!  ] - 0.37s - 1 test - 1 task


.. _task_results:

Task results
============

The execution of any task returns a value, which can be:

* **FAIL**: The task ran normally but user requirements or conditions were not met. The test **failed**.
* **OK**: The task ran successfully and all required conditions and/or return values were correct.
* **NOEXEC**: The task was skipped or it was not executed.
* **ERROR**: The implementation of the task is wrong and the task execution itself failed.
* **UNKNOWN**: The task was executed but its result is not known.
* **TODO**: The task implementation is unstable and it may produce false failures.
* **soft fail**: The task failed but it is irrelevant (depend on task kind).
* **misconfig**: The task requires specific configuration that is wrong or missing.


.. _timeout:


Timeouts and task termination
=============================

Task classes inheriting from ``Subprocess`` (``Test``, ``TestBG``, ``Command``,
``CommandBG``) accept the ``timeout`` keyword. It indicates the amount of
seconds that the process is allowed to live. If the time expires, the task is
considered failed without other considerations (this is a "safety net"). The
timeout value is 5 seconds by default.

If you want a program running in background (like a server) and other commands
interacting with it, probably you prefer avoid to specify a number of
seconds. This behavior may be achieved using `TestBG`` or ``CommandBG``. These
have no timeout (``timeout=0``) and runs in background
(``detach=True``). Note ``CommandBG`` is a non-checked, that is, its failing
has no consequences.

You need to take two important considerations:

* Atheist terminates all detached tasks when all non-detached tasks have
  finish. The termination is done sending the specified ``signal`` for each
  task. If the task does not finish after 5 tries, atheist begin to send it
  SIGKILL until it finally ends.
* Due to the previous point, the last task of every ``.test`` should be a
  non-detached task. Otherwise, all tasks are immediately terminated.

It may be a special situation: You want to run a command during some time and
then send it an expected to gracefully terminate it. If you try this with the
``timeout`` keyword, your command will receive the specified signal, but the
test will fail due to timeout expiration. In this case, you must use the
``TaskTermination`` task. By this way, you expresses your intention, that is,
the signal is not due to a timeout. You explicitly ask for the task
termination.

An example::

  server = TestBG(desc='this server ran for 3 seconds',
                  cmd='ncat -l -p 9000',
                  expected=-signal.SIGTERM)

  TaskTerminator(server, delay=3)





Reports and reporters
=====================

The report is the way to show task results to the user. Different reports can generate
different kind of reports.

JabberReporter

		[**plugin:** jabber-reporter]


SMTP_Reporter

		[**plugin:** smtp-reporter]


HTML_Reporter

		[**plugin:** html_reporter]


XUnit_Reporter

		[**plugin:** xunit_reporter]


Notifications
-------------

Notifications are just a kind of reporter that send report to remote sites.

That able to use Atheist to monitor the proper working of any application. It
sends you the corresponding test report when something is wrong. The easiest way
is to run a testcase with ``cron`` specifying one or more ``--notify-*``
command-line argument. Currently, two notificators are implemented:

**Email**

  The destination is a email account using the SMTP protocol. Atheist does not
  require an SMTP server or smarthost. You just need to configure an email
  account that will be used by Atheist to send mail to any destination. That
  information must be written in the ``~/.atheist/config`` configuration
  file as a "default" subsection within the "smtp" section. This is an example::

    [smtp]
      [[default]]
      host = smtp.server.org
      port = 587
      user = atheist.notify@server.org
      pasw = somesecret
      ssl = True


  Atheist may send mails directly to the mail server (if the server accepts
  that) just with::

    [smtp]
      [[default]]
      host = smtp.server.org
      port = 587


**Jabber**

  You must indicate a jabber account that will be used by Atheist to send
  notification messages to any other jabber account. The destination account
  must accept this contact previously. The following is an example for the
  configuration in the ``~/.atheist/config`` file. The information must be in a
  "default" subsection within the "jabber" section::

    [jabber]
      [[default]]
      user = atheist.notify@server.org
      pasw = anothersecret


To ask for a notification you just need to specify a test file and
one or more destinations. For example::

    $ atheist --notify-jabber John.Doe@jabber.info test/some_test.test

It is possible to give several ``--notify-*`` arguments in the same
command-line to send notifications to different destinations.


Runners
=======

Runner plugins can modify the conventional process to exec tasks and collect
results.


INotifyRunner

		[**plugin:** inotify-runner]

		FIXME


ManualRepeatRunner

		[**plugin:** manual-repeater-runner]


StepRunner

		[**plugin:** step-runner]

		FIXME


UntilFailRunner

		[**plugin:** until_fail_runner]

		FIXME


.. Local variables:
..   coding: utf-8
..   ispell-local-dictionary: "american"
.. End:
