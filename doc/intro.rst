Introduction
************

Atheist is a simple framework for running test cases.  Its goal is to integrate
in a *single place* the execution and result visualization of all project tests
(any kind). Moreover, atheist provides a small declarative language (a Python
based `DSL <http://en.wikipedia.org/wiki/Domain-specific_language>`_) to specify
`system tests <http://en.wikipedia.org/wiki/System_test>`_.

Main current features:

* `Black-box testing
  <http://en.wikipedia.org/wiki/Black-box_testing>`_: system and
  acceptance tests. It may execute any kind of external shell program.
* `White-box testing <http://en.wikipedia.org/wiki/White-box_testing>`_ by means
  of `unitest <http://docs.python.org/library/unittest.html>`_ for Python and
  `cxxtest <http://cxxtest.tigris.org/>`_ for C/C++.
* `Python doctests  <http://en.wikipedia.org/wiki/Doctest>`_.
* Basic web testing (webpage content asserts) with authentication support.
* Plugable tasks (new kinds of "tests").
* Plugable conditions.


ToDo:

* Per-project plugins.
* Limit/perf testing.
* Remote testing.
* Test deployment.
* Remote management.
* Distributed testing.


Rationale
=========

Atheist aims to provide a testing framework to deal with any kind of test
required in a typical software project. Today, unit tests are available only for
a few programming languages: Python, C and C++ but it is possible to extend to
other languages by means of plugins. We could say that atheist is a testing
front-end.

Atheist defines a `DSL <http://en.wikipedia.org/wiki/Domain-specific_language>`_
that allows to specify system tests in a easy and powerful way. It is based in
the principle that any successful program returns 0, but if something is wrong, it
returns an *error code*. May software projects use this to create ad-hoc
scripts (typically shell, perl, python,...) for system testing. Atheist proposes
a *standardized language* to write and manage these scripts.



Tasks
=====

The task is the minimal running unit. Each task instance defines an individual
execution (e.g. a shell command). If the task success is *checked*, the task is
a *test*, but there are many situations where we need non-checked tasks. Atheist
comes with may tasks types (see :ref:`task_types`).

The constructor of any task accepts many keyword parameters (in the ``key=val`` form)
that may change the task expected behavior in several ways (see :ref:`keywords`).

The most common task class is ``Test``. The Test object is responsible for
executing the command and checking its termination value. A very basic example::

   Test('true')


TestCases (``.test`` files)
===========================

Task instances need to be defined in text source files (with ``.test``
extension). Although these files are written in a subset of the Python language,
they may be seen as pseudo-declarative specifications. You tell Atheist what
you want to test and even the order, but the decision about running them and
when do it is taken by Atheist; some of them may be never run...

The file **does not define sequential** imperative sentences. For example,
if you write this in a ``.test`` file::

   Test('ls /')
   print "hello"

the ``print`` statement is executed when the test file is **LOADED** but the
``ls`` command will run later, when the test is **EXECUTED**. You must take in
mind that the atheist .test file defines a set of tests and their
dependencies. It is not a conventional python program.


Pre- and post- conditions
=========================

The atheist philosophy is based in the specification of *conditions*. A
condition is a independent predicate that may be evaluated as ``true`` or
``false``. Any test may have an arbitrary set of pre-conditions, that must be
satisfied before running the task itself. Then, if the task success, its
post-conditions are also evaluated. The post-conditions are equivalent to
assertions or expectations. All that is very related with the `design by
contract <http://en.wikipedia.org/wiki/Design_by_contract>`_.

The test execution happens in this way (in a kind of pseudo-code):

* if any pre-condition fails:

  * return FAIL
* if task fails:

  * return FAIL
* if any post-condition fails:

  * return FAIL
* return OK

Any predicate about task execution environment would be managed as a
condition. Atheist comes with many conditions (see :ref:`conditions`) and any
other may be easily supported writing a new condition as a plugin.

See :ref:`conditions_sample` for a basic condition example.



.. _changes:

Release notes
=============

.. include:: ../CHANGES.rst




.. Local variables:
..   coding: utf-8
..   ispell-local-dictionary: "american"
.. End:
