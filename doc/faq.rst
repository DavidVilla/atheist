Frequently asked questions (FAQ)
================================

.. _faq_shell:

Why do I get strange behavior when the Task command line has quotation?
-----------------------------------------------------------------------
You need to use a shell to execute your command. Use ``shell=True`` when
you define the **Task**.


Why do not the environment variables expand?
--------------------------------------------

Environment variable expansion must made by a shell. Use ``shell=True`` when
you define the **Task**.


Then, why do not shell=True is the default value?
------------------------------------------------

Shell is expensive if you want to run many tests. Use only when is
needed.


Why I get "<[FAIL]FileExists ''>"?
----------------------------------

Probably you are checking existence of the stdout or stderr of a
Task. You must put ``save_stdout=true`` o ``stdout`` in the Task
if you need those files.


Why I have Python errors in my correct test files?
--------------------------------------------------

atheist executes the .test files with the same Python version that it
runs. It you use new Python features in your .test files but you run
atheist with a old version the problem appears. To solve that always
execute atheist with the newer Python version that you have.

.. Local variables:
..   ispell-local-dictionary: "american"
..   TeX-master: "main.tex"
.. End:
