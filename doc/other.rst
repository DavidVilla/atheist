
.. _templates:

Templates
---------

The template is a set of predefined values for Test keywords. You may use the
same configuration for many tasks avoiding to repeat them. This is an example::


    t1 = Template(timeout=6, expected=-9)
    Test('foo', templates=[t1])
    Test('bar', templates=[t1])


Both tests will be automatically killed after 6 seconds and the
expected return value is -9. This means that these processes receive the
SIGKILL(9) signal. You may specify several templates as a list.




hooks: setup and teardown
-------------------------

You may write tasks to execute before and after **each** test file. To do
so, just put this tasks in files called ``_setup.test`` and
``_teardown.test``.


Clean-up
--------

When your task creates files you may track them for automatic
cleaning. Just add their filenames to the task ``gen`` attribute. Here's an
example::

    t = Test('touch foo')
    t.gen += 'foo'

You may specify one o more filenames (as a string list).

If you want the generated files not to be automatically removed for
manual inspection of the results, you must specify the ``--dirty`` option
(see below). To clean-up these files later, specify the ``-C`` option.




Related tools
=============


* `Twisted trial`_

* `Testing Framework`_

.. _`Twisted trial`: http://twistedmatrix.com/trac/wiki/TwistedTrial
.. _`Testing Framework`: http://www.c2.com/cgi/wiki?TestingFramework




.. Local variables:
..   coding: utf-8
..   ispell-local-dictionary: "american"
.. End:
