Installation
************

The preferred install way is the `debian package
<http://packages.debian.org/unstable/atheist>`_ (also `ubuntu
<http://packages.ubuntu.com/oneiric/atheist>`_)::

  sudo aptitude install atheist atheist-plugins


The ``atheist`` package includes the next as plugins:

* Alias (task)
* CompositeCondition (condition)
* DocTest (task)
* FileContainsRE(condition)
* Imap (task)
* ImapSearchQuery (task)
* ManualRepeatRunner (runner)
* OpenPort (condition)
* Or (condition)
* Reporter (runner)
* SMTP (task)
* StepRunner (runner)
* TaskFinished (condition)
* TaskRunning (condition)
* TaskTerminator (task)
* UnitTestCase (task)
* UntilFailRunner (runner)



Some other plugins are packaged separately:

* ``atheist-plugins``. It is a meta-package to install all the next packages.

* ``atheist-debian`` (plugin debianpkg)

  * DebPkgInstalled (condition)
  * DebPkgBuild (task)
  * DebPkgInstall (task)

* ``atheist-cxxtest`` (plugin cxxtest)

  * CxxTest (task)

* ``atheist-extra-notifiers`` (plugins smtp-reporter and jabber-reporter)

  * STMP_Reporter (task)
  * JabberReporter (task)

* ``atheist-inotify-runner`` (plugin inotify-runner)

  * INotifyRunner (runner)

* ``atheist-webtest`` (plugin webtesting)

  * WebTest (task)



Thanks to `Cleto Martin
<http://qa.debian.org/developer.php?login=cleto.martin%40gmail.com>`_ as
maintainer of Debian packages.


.. Local variables:
..   coding: utf-8
..   ispell-local-dictionary: "american"
.. End:
