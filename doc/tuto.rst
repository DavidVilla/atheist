Tutorial and Overview
*********************

This page shows the basics of Atheist and lets you know if this is you are
looking for. Detailed documentation may be found in the section :ref:`reference`.


Example 1: "The inevitable 'Hello world'"
=========================================

Suppose we want to write the typical *hello world* in any language, e.g. Ruby (file
``hello.rb``)... but we write the test first, of course (file
`tuto/hello-ruby.test <https://arco.esi.uclm.es/svn/public/prj/atheist/samples/tuto/hello-ruby.test>`_)::

  Test(desc = 'testing the ruby hello',
       cmd = 'ruby $testdir/hello.rb',
       post = FileContains('hello world'))

run the test...::

  $ atheist hello.test
  [FAIL] TaskCase: ./doc/hello.test
  [FAIL] `- Test-1  -( 1: 0)  ruby hello.rb
  [    FAIL   ] - 0.36s - 0/1 test

...and fails, why?::

  $ atheist hello.test --stderr
  T1:err| ruby: No such file or directory -- hello.rb (LoadError)
  [FAIL] TaskCase: ./doc/hello.test
  [FAIL] `- Test-1  -( 1: 0)  ruby hello.rb
  [    FAIL   ] - 0.36s - 0/1 test

ok, as we know, ``hello.rb`` does not even exists (this is a "hello world"
example :-). The option ``--stderr (-e)`` prints the task stderr to console. We
should check the ruby file existence with a pre-condition::

  Test(desc = 'testing the ruby hello',
       pre = FileExists('$testdir/hello.rb'),  # <-- that's new
       cmd = 'ruby $testdir/hello.rb',
       post = FileContains('hello world'))

run again, and you can see as the FileExists precondition fails::

  $ atheist hello.test
  [FAIL] TaskCase: ./hello.test
  [FAIL] `- Test-1  -( -: 0)  ruby ./hello.rb | testing the ruby hello
  [FAIL]    `- pre:  FileExists './hello.rb'
  [    FAIL   ] - 0.13s - 0/1 test


Now we must write some in ``hello.rb``, but we make a mistake::

  #!/usr/bin/ruby
  print("hello wplrd\n")

run the test again::

  $ atheist --plain hello.test
  T1:out| hello wprld
  [FAIL] TaskCase: ./hello.test
  [FAIL] `- Test-1  -( 0: 0)  ruby ./hello.rb | testing the ruby hello
  [ OK ]    `- pre:  FileExists './hello.rb'
  [FAIL]    `- post: FileContains '/tmp/atheist-david/3853/T1_out' (1) content:'['hello world']'
  [    FAIL   ] - 0.35s - 0/1 test

the progran ran, but the post-condition expecting "hello world" in the output
fails. The option ``--stdout (-o)`` prints the task stdout to console. Ok, now, we fix the ruby program::

  #!/usr/bin/ruby
  print("hello world\n")

...run the test again::

  $ atheist hello.test
  [ OK ] TaskCase: ./hello.test
  [  ALL OK!  ] - 0.36s - 1 test - 1 task

and all ok finally!


.. _conditions_sample:

Example 2: "Is that doc correct?"
=================================

As an example using more conditions, we may check successful compilation of a LaTeX
document. Test file is
`tuto/latex.test <https://arco.esi.uclm.es/svn/public/prj/atheist/samples/tuto/latex.test>`_::

  Test(desc = 'checking a LaTeX document',
       pre = FileExists('$testdir/main.tex'),
       cmd = 'pdflatex $testdir/main.tex',
       gen = ['main.pdf', 'main.log', 'main.aux'],
       post = Not(FileContains('Citation', 'main.log')))


1. It uses the ``cwd`` keyword to indicate that compilation must be done in the same
   directory where the ``.test`` file is.
2. A ``FileExists`` pre-condition to ensure the source file is there.
3. The ``gen`` keyword indicates files generated by the task (that must be
   removed at end). Furthermore, this implies that these files must not exist
   before run the task and must exist after.
4. Finally, a post-condition ensure the file ``mail.log`` does not content the
   word ``Citation``.  That would imply the ``.tex`` has unresolved bibliography
   references (the ``\cite{}``` LaTeX command).

Ok, lets run that::

  $ atheist latex.test -i5
  [ OK ] TaskCase: ./latex.test
  [ OK ] `- Test-1  -( 0: 0)  pdflatex main.tex | checking a LaTeX document
  [ OK ]    `- pre:  FileExists './main.tex'
  [ OK ]    `- post: Not (FileContains 'main.log' (1) content:'['Citation']'
  [  ALL OK!  ] - 0.46s - 1 test - 1 task



.. note::

   Atheist is not a build system. In fact, testing products are usually
   deleted. However, build process may be checked to assure its correctness, as
   in the previous example.


Example 3: "Is Google reachable?"
=================================

Here we are going make a bottom-up approach typical in network issue
debugging. Test file is `tuto/is_google_reachable.test <https://arco.esi.uclm.es/svn/public/prj/atheist/samples/tuto/is_google_reachable.test>`_:

First, the ``eth0`` NIC is UP::

   Test('ip link | grep eth0 | grep "state UP"', shell=True,
        desc="Is interface link up?")

We have a default gateway and is reachable...::

  Test("ping -c2 $(/sbin/route -n | grep ^0.0.0.0 | awk '{print $2}')", shell=True,
       desc="Is the local router reachable?")

And, all the DNS servers reachable too? Note we create a Test per ``nameserver``::

  for line in file('/etc/resolv.conf'):
      if line.startswith('nameserver'):
          server = line.split()[1]
          Test('ping -c 2 {0}'.format(server),
               desc="Is DNS server {0} reachable?".format(server))

Can the host resolve the Google domain name?::

  Test('host www.google.com',
       desc="Resolve google website")

Is Google host reachable?::

  Test('ping -c 1 www.google.com',
       desc="Is google reachable")

And finally, can ``index.html`` be downloaded successfully?::

  WebTest('http://www.google.com/webhp?hl=en',
          post = FileContains('value="I&#39;m Feeling Lucky"'))

Note we force English version to get the test run everywhere.

Let's run::

  $ atheist samples/tuto/is_google_reachable.test -i3
  [ OK ] TaskCase: ./samples/tuto/is_google_reachable.test
  [ OK ] +- Test-1  -( 0: 0)  ip link | grep eth0 | grep "state UP" | Is interface link up?
  [ OK ] +- Test-2  -( 0: 0)  ping -c2 $(/sbin/route -n | grep ^0.0.0.0 | awk '{print $2}') | Is the local router reachable?
  [ OK ] +- Test-3  -( 0: 0)  ping -c 2 192.168.2.1 | Is DNS server 192.168.2.1 reachable?
  [ OK ] +- Test-4  -( 0: 0)  host www.google.com | Resolve google website
  [ OK ] +- Test-5  -( 0: 0)  ping -c 1 www.google.com | Is google reachable
  [ OK ] `- Web -6  -( 0: 0)  http://www.google.com/webhp?hl=en
  [  ALL OK!  ] - 3.86s - 6 tests - 6 tasks


.. Local variables:
..   coding: utf-8
..   ispell-local-dictionary: "american"
.. End:
