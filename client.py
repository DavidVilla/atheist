#!/usr/bin/env python
# -*- coding: utf-8 -*-

import sys
import time
import gobject
import logging

import Ice

Ice.loadSlice('-I%s atheist.ice' % Ice.getSliceDir())
import Atheist

import atheist.log

log = logging.getLogger()
log.setLevel(logging.DEBUG)
log.addHandler(atheist.log.create_basic_handler())


class StatusObserverI(Atheist.StatusObserver):
    def updateTasks(self, tasks, current=None):
        log.info("statusOb Task: %s" % tasks)

    def updateTaskCases(self, cases, current=None):
        log.info("statusOb TaskCase: %s" % cases)

    def updateManager(self, value, current=None):
        log.info("statusOb Manager: %s" % value)


class OutObserverI(Atheist.OutObserver):
    def update(self, taskID, val, current=None):
        print taskID, val


class LogObserverI(Atheist.LogObserver):
    def update(self, taskID, entry, current=None):
        print taskID, entry



class client(Ice.Application):

    def run(self, argv):
        self.shutdownOnInterrupt()

        ic = self.communicator()

        adapter = ic.createObjectAdapterWithEndpoints("Client", "default")
        base = ic.stringToProxy(ic.getProperties().getProperty("Manager.Proxy"))
        print base

        self.mng = Atheist.ManagerPrx.checkedCast(base)

        if not self.mng:
            raise RuntimeError("Invalid proxy")

        self.mng.attachStatusOb(
            Atheist.StatusObserverPrx.uncheckedCast(
                adapter.addWithUUID(StatusObserverI())))

        self.mng.attachOutOb(
            Atheist.OutObserverPrx.uncheckedCast(
                adapter.addWithUUID(OutObserverI())).ice_batchOneway(), [])

        self.mng.attachLogOb(
            Atheist.LogObserverPrx.uncheckedCast(
                adapter.addWithUUID(LogObserverI())).ice_batchOneway(), [])

        adapter.activate()
        self.callbackOnInterrupt()

        gobject.threads_init()
        gobject.timeout_add(3, self.event)
        self.loop = gobject.MainLoop()
        self.loop.run()

        self.communicator().shutdown()
        return 0

    def interruptCallback(self, args):
        gobject.idle_add(self.loop.quit)

    def event(self):
        print 'Event!'

        cases = self.mng.getTaskCases()
        if cases:
            print len(cases), "task cases"
            print cases[0].getConfig()

        self.mng.ice_oneway().runAll()
        print "FIN ------------"
        gobject.timeout_add(20000, self.event)

        return False


if __name__ == "__main__":
    sys.exit(client().main(sys.argv))
