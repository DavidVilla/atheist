0.20120928
==========

* notify-ok default value to False
* JabberReporter: using loggingx.Account

1.20120501
==========

* 'user' key in config is used as default user for reports
* SMTP_Reporter: prefix subject with date


0.20110029
==========

* --notify-ok returns (it was lost by refactoring).


0.20110801
==========

* When atheist detects non-tty output, it automatically enables plain output and
  disable progressbar.

* python-fs: Task uses fs = OSFS('/') by default. FileExists, FileContains,
  etc. uses Task.fs. atheist/fs.py is a minimal OSFS implementation to avoid
  python-fs mandatory dependency.

* Several bugfixes adding automatic conditions. ConditionList.remove_dumps() modified.

* pyarco.Thread.ThreadFunc simplified

* Default reporter shows task index at left.

* MockFS plugin.

* New task method: is_finished()

* Default status-based implementations for Task.is_running() and
  Task.is_finished()

* run_task waits for detached tasks start.

* TaskFinished creates a instance per task.

* Test may be created without 'cmd' for pure-condition tasks. No auto
  TimeLessThan is created in this case.

* --cols=0 means no limit.

* console reporter shows decorated and composed conditions (--report-detail=5)

* --show-all removed: use --report-detail=5

* atypes.py: includes TypedList, Condition and ConditionList

* -v implies --cols=0

* Progressbar minimum width set to 20

* ConditionSplitter: used in task conditions

* CompositeTask moved to task.py

* tree.draw adds a new 'level' parameter

* CompositeCondition now on condition.py

* optparse to argparse migration

* manpage is now generated from a reST jinja template

* Many tests fixed.

* --config -> config-file

* New argument --config to override arguments

* some internal argument names changed to match with option names

* Plugin.get_options() renamed to Plugin.add_arguments()

* [vars] -> [interpolation]

* new [defaults] section in config file to specify default value for command line arguments

* DefaultManager renamed to Atheist

* util.relpath is now a class: RelativePathConverter

* exec_file() is now a class: FileExecutor

* Task._ts removed / Task._case added

* New argument --timeout-scale

* CompositeTask.children renamed as .tasks

* default_settings renamed as defaults

* new doctests in .utils

* more Tasks __doc__



Release 0.20110726
------------------

* Follow directory symlinks.


Release 0.20110722
------------------

* Interpolation in loaded files --with load() function--

* Reporter notifiers show argv args.

* New FileHasPermissions condition


Release 0.20110718
------------------

* New task TestBG.

* New task CommandBG, deprecates Daemon.

* New option --create for create and edit test files.

* default signal is now SIGTERM instead of SIGKILL

* New substitution key for "hostname"

* Fixed 'todo' keyword for debian plugin tasks.

Release 0.20110513
------------------

* SMTP_Reporter config bug fixed

* Tasks accepts a new keyword 'flags' to indicate boolean keywords as flags::

    Test('foo', flags=shell | save_stdout)


Release 0.20110513
------------------

* Auto-added :class:`TimeLessThan` condition for the `timeout` keyword.

* New :class:`JUnit` plugin.

* New HTML Reporter plugin. It provides ``--with-html`` and ``--html-dir`` options.

* New ``--save-logs`` command-line option.

* Auto-added conditions are shown only if the fails. Use ``--show-auto`` to show
  always all conditions.

* UnitTaskCase and DocTest are now TaskFactory

* Some improvements in documentation generation: conditions.


.. Local Variables:
..  mode: rst
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
