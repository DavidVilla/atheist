# -*- mode:python; coding:utf-8 -*-

import time
import optparse

from pyarco.UI import clear

import atheist
from atheist.conf import settings
from atheist import const


class StepReporter(atheist.reporter.ConsoleReporter):
    def render_task(self, task):
        return self.tree.draw([task], callback=self.build_task)


class StepCaseExecutor(atheist.CaseExecutor):

    def __init__(self, case, observer, reporter):
        atheist.CaseExecutor.__init__(self, case, observer)
        self.reporter = reporter

    def run_tasks(self):
        clear()
        print time.asctime()

        print self.case
        for task in self.case.tasks:
            if self.mng.aborted:
                break

            print task.describe().encode('utf8')
            raw_input("-- Run the task [ENTER]")

            result = atheist.run_task(task, end_callback=self.observer)
            print self.reporter.render_task(task)

            if result in [const.FAIL, const.ERROR] and task.check \
                    and not settings.keep_going:
                task.log.info("FAIL, skipping remaining tasks ('keep-going mode' disabled)")
                break

            raw_input('-- Next task [ENTER]')


class StepRunner(atheist.Runner, atheist.iface.Plugin):

    @classmethod
    def add_arguments(cls,  parser):
        parser.plugin_group.add_argument(
            '--step', action='store_true', default=False,
            help='Execute tests step by step controlled by user')

    @classmethod
    def config(cls, parser):
        if not settings.step:
            return

        settings.RunnerClass = cls
        settings.disable_bar = True

    def run_suite(self):
        reporter = StepReporter('step')

        self.setup()
        results = atheist.Suite()

        for case in self.suite:
            try:
                executor = StepCaseExecutor(case, observer=self.pb.inc, reporter=reporter)
                results.append(executor.run())

                if len(case.tasks) > 1:
                    clear()
                    print "Case report:"
                    print reporter.build_case(case)
                    raw_input('-- Next task [ENTER]')
                    clear()

            except EOFError:
                self.mng.abort()
                break

        clear()
        print "Final report:"
        return results
