# -*- mode:python; coding:utf-8 -*-

# IMAP reader plugin for atheist
#
# Copyright (C) 2010 Oscar Aceña
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


import imaplib
import email

import atheist
from atheist.conf import settings
from atheist.condition import Condition
import atheist.const as const


def checkResults(items):
    if all([x == 'OK' for x in items]):
        return const.OK

    return const.FAIL


class IMAP(atheist.Task, atheist.iface.Plugin):
    """Give access to a IMAP account"""

    acro = 'IMAP'
    allows = ['misconfig']

    def __init__(self, key='default', **kargs):
        self.key = 'imap.' + key
        self.connection = None
        atheist.Task.__init__(self, **kargs)
        self.auto_desc = "IMAP <not configured yet>"

    def configure(self):
        key = self.key

        if key not in settings:
            raise atheist.ConfigureError(\
                "Your config requires a section '%s'." % key)

        try:
            config = settings[key]
            self.host = config['host']
            self.port = config['port']
            self.user = config['user']
            self.pasw = config['pasw']
        except KeyError, e:
            raise atheist.ConfigureError(
                "Your config requires a key '%s.%s'" % (key, e.args[0]))

#        try:
#            self.port = checked_cast(int, self.port)
#        except TypeError, e:
#            raise atheist.ConfigureError(str(e))

        try:
            self.port = int(self.port)
        except ValueError, e:
            raise atheist.ConfigureError(
                "Your config key '%s.port' must be an integer instead of '%s'" %
                (key, self.port))

        self.auto_desc = "IMAP {0} -> {1}:{2}".format(
            self.user, self.host, self.port)

    def run(self):
        try:
            self.connection = imaplib.IMAP4_SSL(self.host, self.port)
            assert self.connection is not None

        except Exception, e:
            self.log.error("IMAP error: %s", e)
            return const.FAIL

        try:
            results = (
                self.connection.login(self.user, self.pasw)[0],
                self.connection.select()[0],
                )
        except imaplib.IMAP4.error, e:
            self.log.error("IMAP error: %s", e)
            return const.FAIL

        return checkResults(results)

    # http://tools.ietf.org/html/rfc2060.html#section-6.4.4
    def filter(self, query, **kargs):
        retval = ImapSearchQuery(self, query, **kargs)
        retval.auto_desc = "IMAP filter: {0}".format(query)
        return retval

    def repr_args(self):
        return self.key


class ImapSearchQuery(atheist.Task):
    """Perform a IMAP Query to filter a folder
    """

    acro = "MSet"

    def __init__(self, reader, query, **kargs):
        self.imap = reader
        self.query = query
        self.messages = []

        atheist.Task.__init__(self, **kargs)

        self.misconfig = self.imap.misconfig

    def configure(self):
        if not self.imap.connection:
            raise atheist.ConfigureError("ImapSearchQuery without IMAP!!")

    def run(self):
        try:
            result, msgIds = self.imap.connection.search(None, self.query)

            results = [result]
            for num in msgIds[0].split():
                result, msg = self.imap.connection.fetch(num, '(RFC822)')
                self.messages.append(email.message_from_string(msg[0][1]))
                results.append(result)

        except Exception, e:
            self.log.error("Error on search: %s" % e)
            return const.FAIL

        return checkResults(results)

    def findOnSubjects(self, criteria):
        return ImapSubjectContainsCondition(self, criteria)

    def repr_args(self):
        return self.query


class ImapSubjectContainsCondition(Condition):

    def __init__(self, mailbox, criteria):
        self._mailbox = mailbox
        self.criteria = criteria

        Condition.__init__(self)

    def run(self):
        for message in self._mailbox.messages:
            if self.criteria in message['Subject'].replace('\r\n\t', ' '):
                return const.OK
        return const.FAIL

    def __eq__(self, other):
        return (Condition.__eq__(self, other) and
                self.criteria == other.criteria and
                self._mailbox == other._mailbox)

    def repr_args(self):
        return "'{0}'".format(self.criteria)
