# -*- mode:python; coding:utf-8; tab-width:4 -*-

import socket
import smtplib

import atheist
from atheist.conf import settings
import atheist.const as const


class SMTP(atheist.Task, atheist.iface.Plugin):
    """FIXME
    """
    acro = 'SMTP'
    allows = ['misconfig']

    def __init__(self, key='default', **kargs):
        self.key = 'smtp.' + key
        atheist.Task.__init__(self, **kargs)
        self.conn = None

    def configure(self):
        key = self.key

        if not settings.has_key(key):
            raise atheist.ConfigureError(
                "Your config requires a section '%s'." % key)

        try:
            self.host = settings['{0}.host'.format(key)]
        except KeyError, e:
            raise atheist.ConfigureError(
                "Your config requires a key '%s.%s'" % (key, e.args[0]))

        try:
            self.port = int(settings.safe_get('{0}.port'.format(key), smtplib.SMTP_PORT))
        except ValueError, e:
            raise atheist.ConfiggureError(
                "Your config key '%s.port' must be an integer instead of '%s'" %
                (key, self.port))

        try:
            self.fromaddr = settings['{0}.user'.format(key)]
            self.credentials = (self.fromaddr,
                                settings['{0}.pasw'.format(key)])
        except KeyError:
            self.fromaddr = 'atheist@' + socket.gethostname()
            self.credentials = None


        self.ssl = settings.safe_get('{0}.ssl'.format(key), False)

        if self.credentials is None:
            sender = '<anom>'
        else:
            sender = self.credentials[0]

        self.auto_desc = "SMTP {0} -> {1}:{2}".format(sender, self.host, self.port)

    def run(self):
        self.log.debug("SMTP Connecting: %s:%s" % (self.host, self.port))
        try:
            self.conn = smtplib.SMTP(self.host, self.port)

            if self.ssl:
                self.conn.ehlo()
                self.conn.starttls()
                self.conn.ehlo()

            if self.credentials:
                self.conn.login(*self.credentials)

            return const.OK

        except (smtplib.SMTPAuthenticationError,
                smtplib.SMTPException,
                socket.error), e:
            self.log.error(e)
            return const.FAIL

    def send(self, toaddrs, subject, data, **kargs):
        return SendMail(self, toaddrs, subject, data, **kargs)

    def repr_args(self):
        return self.key


class SendMail(atheist.Task):
    acro = 'Mail'

    def __init__(self, smtp, toaddrs, subject, data, **kargs):
        self.smtp = smtp

        if isinstance(toaddrs, str):
            toaddrs = [toaddrs]

        self.toaddrs = toaddrs

        self.subject = subject
        self.data = data

        atheist.Task.__init__(self, **kargs)

        self.misconfig = self.smtp.misconfig
        self.auto_desc = "SMTP '{0}' -> {1}".format(subject, toaddrs)

    def configure(self):
        if not self.smtp.conn:
            raise atheist.ConfigureError("SendMail without SMTP task!!")

    def run(self):
        from email.utils import formatdate

        msg = "From: %s\r\nTo: %s\r\nSubject: %s\r\nDate: %s\r\n\r\n%s" % (
            self.smtp.fromaddr, str.join(',', self.toaddrs),
            self.subject, formatdate(), self.data)

        try:
            self.smtp.conn.sendmail(self.smtp.fromaddr, self.toaddrs,
                                    msg.encode('utf-8'))
            return const.OK

        except (smtplib.SMTPSenderRefused, smtplib.SMTPDataError), e:
            self.log.error(e)
            return const.FAIL

        finally:
            self.smtp.close()
