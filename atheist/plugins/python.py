# -*- mode: python; coding: utf-8 -*-

# unittest python plugin for atheist
#
# Copyright (C) 2010 Arco Group
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import sys
import types
import inspect
import unittest
import doctest

import atheist
import atheist.const as const

from pyarco.Type import checked_type


class UnitTask(atheist.Task):
    acro = 'Unit'
    allows = ['check', 'expected', 'must_fail']
    concurrent = False

    def __init__(self, case, **kargs):
        self.unitcase = checked_type(unittest.TestCase, case)

        atheist.Task.__init__(self, **kargs)
        self.wrap_outs = True

        self.auto_desc = "{0}.{1}".format(
            case.__class__.__name__,
            case._testMethodName.lstrip('test'))

        short_desc = case.shortDescription()
        if short_desc:
            self.auto_desc += ": " + short_desc.split("\n")[0].strip()

    def run(self):
        try:
            tr = unittest.TestResult()
            self.unitcase(tr)

            if tr.wasSuccessful():
                return const.OK

            if tr.errors:
                sys.stderr.write(tr.errors[0][1])
                return const.ERROR

            if tr.failures:
                sys.stderr.write(tr.failures[0][1])
                return const.FAIL

            return const.ERROR

        except Exception, e:
            self.log.error(e)
            return const.FAIL

    def repr_args(self):
        return self.unitcase._testMethodName.lstrip('test')


class UnitTestCase(atheist.TaskFactory, atheist.iface.Plugin):
    """ Wrapper for standard ``unitest.TestCase``. ``case`` must be a user
    written unittest.TestCase. Several may be specified. If it is called without
    args, all the TestCase's in the file are processed.

    An example::

      class TestSample(unittest.TestCase):
          def test_trivial():
              self.assertEqual(1, 1)

      UnitTestCase(TestSample)

    or without args to process all the TestCase classes in the module::

      UnitTestCase()
    """

    @classmethod
    def create(cls, *cases, **kargs):
        if not cases:
            children = cls.discover(**kargs)

        elif len(cases) > 1:
            children = [UnitTestCase(cls, **kargs) for cls in cases]

        else:
            cls = cases[0]

            if not issubclass(cls, unittest.TestCase):
                raise TypeError("Test cases should be derived from unitest.TestCase")

            suite = unittest.TestLoader().loadTestsFromTestCase(cls)
            children = [UnitTask(x, **kargs) for x in suite]

    @classmethod
    def discover(cls, **kargs):
        frame = inspect.getouterframes(inspect.currentframe())[3][0]
        cases = [x for x in frame.f_locals.values()
                 if inspect.isclass(x) and issubclass(x, unittest.TestCase)]

        return [UnitTestCase(case, **kargs) for case in sorted(cases)]


#class TestCaseDiscover(atheist.Task, atheist.Plugin):
#    def __init__(self):
#        frame = inspect.getouterframes(inspect.currentframe())[2][0]
#
#        cases = [x for x in frame.f_locals.values()
#                 if inspect.isclass(x) and issubclass(x, unittest.TestCase)]
#
#        for case in cases:
#            UnitTestCase(case)


class DocTest(atheist.TaskFactory, atheist.iface.Plugin):
    """Wrapper to add standard `doctest
    <http://docs.python.org/library/doctest.html>`_ to atheist testcases.

    Examples::

      import sys
      DocTest(sys)

      DocTest('os.path')

      DocTest('package', cwd='$testdir/files')
    """

    allows = ['cwd', 'must_fail']

    @classmethod
    def create(cls, module, **kargs):

        atheist.KeyWords.validate(cls, kargs)

        checked_type((str, types.ModuleType), module)

        if 'cwd' in kargs:
            sys.path.append(kargs['cwd'])

        if isinstance(module, str):
            module = __import__(module, fromlist=module.split('.')[-1])

        for case in doctest.DocTestSuite(module):
            t = UnitTask(case)
            t.auto_desc = case.shortDescription()
