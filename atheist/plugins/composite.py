# -*- mode: python; coding: utf-8 -*-
""" Composite tasks and conditions """

from atheist.iface import Plugin
from atheist.condition import CompositeCondition


class Or(CompositeCondition, Plugin):
    """Check at least one of the given conditions is satisfied."""

    def __init__(self, *conds):
        CompositeCondition.__init__(self, any, *conds)

    def repr_args(self):
        return "(" + str.join(', ', ["{0}({1})".format(x.name, x.repr_args())
                                     for x in self.children])
