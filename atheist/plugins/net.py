import os
import socket

import atheist
from atheist.log import Log
from atheist.condition import Condition


class OpenPort(Condition, atheist.iface.Plugin):
    """
    Check the port number *port* is open, i.e., a process is listening to it.
    Default *host* is ``localhost`` and default *proto* is TCP.
    """

    def __init__(self, port, host='localhost', proto='tcp'):
        assert proto in ['tcp', 'udp']
        assert isinstance(port, int) and 0 < port < 65535

        self.proto = proto
        self.port = port
        self.host = host

        Condition.__init__(self, 'port', 'host', 'proto')

        if self.proto == 'udp' and host != 'localhost':
            raise Exception('OpenPort does not support remote UDP ports')

    def run(self):
        if self.host == 'localhost':
            return self.localport()

        return self.remoteport()

    def localport(self):
        return os.system('fuser -n %s %s > /dev/null 2> /dev/null' % \
                             (self.proto, self.port)) == 0

    def remoteport(self):
        #return os.system(self.cmd = 'nmap -p%s %s | grep "%s/%s open" > /dev/null' % \
        #                     (self.port, self.host, self.port, self.proto)) == 0

        s = socket.socket()
        s.settimeout(1)
        try:
            s.connect((self.host, self.port))
            s.shutdown(socket.SHUT_RDWR)
            s.close()
            return True
        except socket.error, e:
            Log.info("OpenPort: %s" % e)
            return False

    def repr_args(self):
        return "'%s %s/%s'" % (self.host, self.proto, self.port)
