# -*- mode:python; coding:utf-8 -*-

import os
import time
import logging
import shutil

import atheist
from atheist import const
from atheist.log import Log
from atheist.conf import settings
from atheist.utils import count, escape_html
from atheist import CompositeTask

from atheist.reporter import Reporter


plugindir = os.path.split(__file__)[0]


def get_pkg_file(fname):
    for d in [plugindir, '/usr/share/atheist']:
        candidate = os.path.join(d, fname)
        if os.path.exists(candidate):
            return candidate

    return None


class HTML_Reporter(Reporter, atheist.iface.Plugin):
    htmldir = 'report'

    def __init__(self, htmldir=None):
        Reporter.__init__(self, dst='html')
        self.htmldir = htmldir or HTML_Reporter.htmldir
        self.data = ''

        if not os.path.exists(self.htmldir):
            os.makedirs(self.htmldir)

    def render(self, suite, fd=None):
        if fd is None:
            shutil.copy(get_pkg_file('style.css'), self.htmldir)
            fd = file(os.path.join(self.htmldir, 'index.html'), 'w')

        template = file(get_pkg_file('html-report.tmpl')).read()

        suite.calculate()
        with fd:
            fd.write(template.format(
                    title    = "Atheist Report",
                    time     = time.asctime(),
                    user     = settings.user,
                    host     = settings.host,
                    basedir  = settings.base_dir,
                    cmdline  = settings.cmdline,
                    loglevel = logging.getLevelName(settings.loglevel),
                    stats    = self.build_stats(suite),
                    body     = self.data,
                    ))

    def add_case(self, case):
        self.data += self.build_case(case)

    def build_case(self, case):
        case_template = '''
<div class="case result-{value}">
  <div class="summary">
    <span class="result">{result}</span>
    Taskcase: <a target="_blank" class="casename" href="{fpath}" title="show testfile">{casename}</a>
  </div>
  <div class="view">
    {tasks}
  </div>
</div>
'''

        tasks = ''
        for task in case.tasks:
            tasks += self.build_task(task)

        retval = case_template.format(
            result   = self.pretty(case.result),
            value    = int(case.result),
            fpath    = self.file_to_link(case.fname),
            casename = settings.relpath(case.fname),
            elapsed  = case.elapsed,
            tasks    = tasks,
            )

        return retval

    def build_task(self, task):

        task_template = '''
<div class="task result-{value}">
  <div class="summary">
    <span class="result">{result}</span>
    <span class="testname atheist-{acro}">{acro}-{index:>03}</span>
    {mode}<span class="return-code">(<span title="given retcode">{retcode:>2}</span>:
       <span title="expected retcode">{expected:2}</span>)</span>
    <span class="description">{desc}</span>
    {keywords}
  </div>

  <div class="view">
    {files}
    {gen}
    {conditions}
    {subtasks}
  </div>
</div>
'''

#title="{task_cls}"

        if not hasattr(task, 'retcode'):
            retcode = 'na'
        elif task.retcode is None:
            retcode = '-'
        else:
            retcode = int(task.retcode)

        subtasks_render = ''
        if isinstance(task, CompositeTask):
            for t in task.tasks:
                subtasks_render += self.build_task(t)

        conditions_render = ''
        for condlist in [task.pre, task._runnin, task.post]:
            for c in condlist:
                conditions_render += self.build_conditions(task, c, condlist.name)

        retval = task_template.format(
            result   = self.pretty(task.result),
            value    = int(task.result),
            task_cls = task.__class__.__name__,
            acro     = task.acro,
            index    = str(task.indx),
            mode     = task.get_mode(),
            retcode  = retcode,
            expected = task.expected,
            desc     = escape_html(task.auto_desc),
            keywords = self.build_keywords(task),
            gen      = self.build_gen(task.gen),
            files    = self.build_files(task),
            subtasks = subtasks_render,
            conditions = conditions_render,
            )

        return retval

    def build_keywords(self, task):

        def add_row(name, value):
            return '''<tr><td class="name">{0}: </td><td>{1}</td></tr>'''.format(
                name, value)

        retval = '<tr><th>{0}</th><th>{1}</th></tr>'.format(
            task.name, task.__class__.__name__)

        for name, record in atheist.KeyWords.items():
            if name in atheist.KeyWords.multivalued() + ['flags']:
                continue

            value = getattr(task, name)
            if value != record.default:
                retval += add_row(name, value)

        gen = task.gen[:]
        if task.stdout in gen:
            gen.remove(task.stdout)
        if task.stderr in gen:
            gen.remove(task.stderr)

        retval += add_row('gen',
                          str.join('', ['%s<br/>' % x for x in gen]))

        return '<table class="keywords result-{result}">{data}</table>'.format(
            result = int(task.result),
            data = retval)

    def build_gen(self, files):
        return '<div class="generated">gen: {0}</div>'.format(str(files))

    def build_files(self, task):
        def create_link(name, fpath):
            size = get_size(os.path.join(self.htmldir, fpath))
            if not size:
                return '<span class="shadow">{0}</span>'.format(name)

            return '<a target="_blank" href="{fpath}">{name}</a> ({size}B)'.format(
                **locals())

        def get_size(fpath):
            try:
                return os.stat(fpath).st_size
            except OSError:
                return None

        def build_reason(task):
            if not task.result in [const.ERROR, const.FAIL]:
                return ''

#            message = task.persistent_log.getvalue()
            message = settings.fs.getcontents(task.filelog_name)
            if not message:
                return ''

            return '''\
| <span class="reason">
  <span class="summary">[ error ]</span>
  <div class="view">
   {0}
  </div>
</span>
'''.format(message)

        files = [('out', task.stdout),
                 ('err', task.stderr),
                 ('log', task.filelog_name)]

        links = []
        for name, fpath in files:
            links.append((name, self.file_to_link(fpath)))

        return '''
<div class="files">
[ {0} | {1} | {2} {3}]
</div>'''.format(
                create_link(*links[0]),
                create_link(*links[1]),
                create_link(*links[2]),
                build_reason(task),
                )

    def build_conditions(self, task, condition, kind):
        condition_template = '''
<div class="condition {auto} result-{value}">
  <span class="result">{result}</span> {kind} {name} {info}
</div>
'''
        retval = condition_template.format(
            result = self.pretty(condition.value),
            value  = int(condition.value),
            auto   = 'auto-condition' if condition.auto and condition.value in [const.OK, const.NOEXEC] else ' ',
            kind   = escape_html('{0:<5}'.format(kind + ':')),
            name   = condition.name,
            info   = escape_html(condition.repr_args()),
            )

        return retval

    def build_stats(self, suite):
        delta = time.time() - settings.init_time
        if delta > 60:
            t = time.strftime("%H:%M:%S", time.gmtime(delta))
        else:
            t = "%.2fs" % delta

        template = '''
<div class="stats result-{value}">
  {result} - {time} - {ok}/{total_tests} - {total_tasks}
</div>
'''
        result = const.FINAL_OK if suite.all_ok() else const.FINAL_FAIL

        return template.format(
            value       = result,
            result      = self.pretty(result),
            time        = t,
            ok          = suite.ok,
            total_tests = count(suite.total, 'test'),
            total_tasks = count(suite.num_tasks, 'task'),
            )

    def file_to_link(self, src):
        template = '''\
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
  </head>
  <body>
    <pre>{0}</pre>
  </body>
</html>
'''

        dst = os.path.split(src)[1]  + '.html'
        f_dst = os.path.join(self.htmldir, dst)
        try:
            content = file(src).read().strip()
            if content:
                content = template.format(content)

            with file(f_dst, 'w') as fd:
                fd.write(content)
        except IOError, e:
            Log.error(e)
            return 'does not exists ' + dst

        return dst

    def pretty(self, value):
        return escape_html(atheist.pretty(value, plain=True))

    @classmethod
    def add_arguments(cls, parser):
        parser.plugin_group.add_argument(
            '--with-html', action='store_true', default=False,
            help="Enable plugin HTML")
        parser.plugin_group.add_argument(
            '--html-dir', type=str, default=cls.htmldir, metavar='FILE',
            help="Path to directory to store HTML report in. Default "
            "is '%s' in the working directory" % cls.htmldir)

    @classmethod
    def config(cls, parser):
        if not settings.with_html:
            return

        if not settings.verbose:
            settings.console_log = False

        settings.reporters.append(HTML_Reporter(settings.html_dir))
        settings.clean_first = True
        settings.loglevel = logging.DEBUG
        settings.disable_bar = True
        settings.plain = True
        settings.save_stdout = True
        settings.save_stderr = True
        settings.save_logs = True
