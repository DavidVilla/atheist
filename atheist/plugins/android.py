# -*- mode: python; coding: utf-8 -*-

from pyarco.Type import checked_type

import atheist
from atheist.condition import Condition

class DroidDeviceReady(Condition, atheist.iface.Plugin):
    '''This condition evaluates the output of the command "adb devices". The
    command output is like the following example:

    $ adb devices
    List of devices attached
    device-name    status
    device2-name   status
    '''

    def __init__(self, devname='',  allowMultiple=False):
        '''If devname specified, ignore the allowMultiple arg.  Else,
        if allowMultiple is False, look for a single device
        connected. If allowMultiple es True, only test if there is
        some device connected, ignoring their device names
        '''
        self.__devname = checked_type(str, devname)
        self.__allowMultiple = checked_type(bool, allowMultiple)

        Condition.__init__(self)

    def run(self):
        import commands

        # skip the first and the two last elements of the list.
        # the first line is a description message of the command
        raw_devices = commands.getoutput('adb devices').strip().split('\n')[1:]
        devices = [ x.split('\t')[0] for x in raw_devices ]

        if self.__devname != '':
            return self.__devname in devices

        if self.__allowMultiple:
            return len(devices) > 0

        else:
            return len(devices) == 1

    def repr_args(self):
        if self.__devname != '':
            return "'%s' is connected" % self.__devname

        else:
            return "Some device is connected"


class DroidTest(atheist.task.Subprocess, atheist.iface.Plugin):
    'Class to do tests to Android developments'

    acro = 'Andr'
    allows = ['check', 'save_stdout', 'stdout', 'device']

    def __init__(self, *filters, **kargs):
        '''The DroidTest constructor get the following arguments:

        filters: a dictionary or a list with tuples on the form (Tag, verbosity
        level).  device: a string with the device serial number to use. If not
        specified, try to use a single connected devices (it will fail if it's
        more than one device connected \tThe verbosity level must be one of the
        Android defined:  see adb \tlogcat --help for info
        '''

        filter_spec = ''

        for f in filters:
            filter_spec += str(f) + ' '

        cmd = 'adb '

        if kargs.has_key('device'):
            cmd += '-s %s ' % kargs.pop('device')

        cmd += 'logcat -d %s *:S' % filter_spec

        atheist.task.Subprocess.__init__(self, cmd=cmd, **kargs)
        self.shell = True

        self.auto_desc = 'DroidTest'
