# -*- mode:python; coding:utf-8; fill-column:110 -*-

from loggingx import JabberHandler, Account

import atheist
from atheist.log import Log
from atheist.conf import settings

from atheist.reporter import FailReporter


class JabberReporter(FailReporter, atheist.iface.Plugin):
    def __init__(self, destination):
        key = 'jabber.default'
        if not key in settings:
            raise atheist.ConfigureError("Your config requires a '%s' section." % key)

        try:
            config = settings[key]
            account = Account(config['user'], config['pasw'])
            handler = JabberHandler(account, destination, Log)
        except AttributeError:
            raise atheist.ConfigureError(\
                "Your '%s' config requires 'user' and 'pass' keys." % key)

        FailReporter.__init__(self, 'jabber://' + destination, handler, plain = True)

    @classmethod
    def add_arguments(cls, parser):
        FailReporter.add_arguments(parser)
        parser.plugin_group.add_argument(
            '--notify-jabber', metavar='ACCOUNT',
            dest='jabber_accounts', action='append', default=[],
            help='notify failed tasks to the given jabber account')

    @classmethod
    def config(cls, parser):
        for account in settings.jabber_accounts:
            settings.reporters.append(JabberReporter(account))
