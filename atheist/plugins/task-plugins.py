# -*- coding:utf-8; tab-width:4; mode:python -*-

from atheist import Task
from atheist.iface import Plugin
from atheist.condition import ConditionSplitter
from atheist.atypes import TypedList
from atheist.condition import Poll, Not
import atheist.const as const


class TaskRunning(ConditionSplitter, Plugin):
    """Check that all the given tasks are running."""
    ItemClass = Task

    def check(self, task):
        return task.is_running()


class TaskFinished(ConditionSplitter, Plugin):
    """Check that all the given tasks are running."""
    ItemClass = Task

    def check(self, task):
        return task.is_finished()


class TaskTerminator(Task, Plugin):
    """It's a special test to kill and **ensure** the termination of other
    tasks.

    The next example runs a netcat server for 10 seconds and then kills it::

     nc = CommandBG("ncat -l 2000")
     TaskTerminator(nc, delay=10)
    """

    acro = 'Term'

    def __init__(self, *tasks, **kargs):
        if isinstance(tasks, Task):
            tasks = [tasks]

        self.tasks = TypedList(Task)
        for t in tasks:
            self.tasks += t

        Task.__init__(self, **kargs)

        # FIXME: aún no está fijado el indx
        self.auto_desc = "Terminates %s" % \
            ', '.join([x.name for x in self.tasks])

        for t in self.tasks:
            self.post += Poll(Not(TaskRunning(t)))

    def run(self):
        for t in self.tasks:
            self.log.debug("terminates %s sending signal %s" % \
                               (t.name, t.signal))
            t.terminate()

        return const.OK

    def repr_args(self):
        return str([x.name for x in self.tasks])
