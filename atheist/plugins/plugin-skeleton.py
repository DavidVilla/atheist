# -*- mode: python; coding: utf-8 -*-

import atheist
from atheist.condition import Condition


class SampleTask(atheist.Task, atheist.iface.Plugin):
    ":skip:"
    acro = 'Sample'

    def __init__(self, **kargs):
        # here you can add/remove/modify kargs
        atheist.Task.__init__(self, **kargs)
        # your code
        # Do not check out staff or execute nothing here. only attribute assignments

    def configure(self):
        ''' [optional]
        - Read/process specific config info (usually on files)
        - It may raise ConfigureError() when essential data is missing or wrong
        '''

    def pre_run(self):
        ''' [optional]
        - You can add automatic pre and post conditions here
       '''

    def is_running(self):
        ''' [mandatory] but possibly inherited
        - Only for non-blocking tasks
        '''
        return False

    def run(self):
        ''' [mandatory]
        - The operations that run the actual task
        '''
        return atheist.const.OK

    def terminate(self):
        ''' [mandatory] but possibly inherited
        Only for non-blocking tasks
        '''

    def repr_args(self):
        ''' [optional]
        Some brief info for the textual representation of the *instance*.
        Only a few words.
        '''
        return 'sample'


class SampleCondition(Condition, atheist.iface.Plugin):
    """:skip:"""

    def __init__(self, values):
        Condition.__init__(self)
        self.values = values
        # Store values only. Don't process nothing here!

    def pre_task_run(self, task, condlist):
        ''' [optional]
        This is invoked just BEFORE the related task execution
        - task: where the condition is added.
        - condlist: the list of conditions where the condition is (pre/post)
        - pos: position in that list.
        '''
        return []

    def run(self):
        ''' [mandatory]
        It actually checks the condition.
        It must return a boolean with the condition value.
        '''
        return True

    def repr_args(self):
        ''' [optional]
        Some brief info for the textual representation of the *instance*.
        Only a few words.
        '''
        return str(self.values)
