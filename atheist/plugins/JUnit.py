# -*- mode: python; coding: utf-8 -*-

# junit plugin for atheist
#
# Copyright (C) 2011 Miguel Ángel García
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import tempfile
import os

import atheist
from atheist.task import Subprocess
from atheist import CompositeTask


def append_auto_condition(container, condition):
    condition.auto = True
    container += condition


class JUnit(CompositeTask, atheist.iface.Plugin):
    acro = 'JUnit'

    class Compiler(Subprocess):

        def __init__(self, main_file, class_file, classpath):
            self.main_file = main_file

            cpstr = ''
            if classpath:
                cpstr = '-cp ' + ':'.join(classpath)
            cmd = 'javac {1} {0}'.format(main_file, cpstr)

            Subprocess.__init__(self, cmd=cmd)

        def repr_args(self):
            return self.main_file

    class Runner(Subprocess):

        def __init__(self, main_file, classpath):
            self.main_file = main_file

            cpstr = ''
            if classpath:
                cpstr = ':'.join(classpath) + ":"
            cmd = 'java -cp {1}{0} org.junit.runner.JUnitCore'.format(main_file, cpstr)

            Subprocess.__init__(self, cmd=cmd)

        def repr_args(self):
            return self.main_file

    def __init__(self, testfile, objs={}, compiling_flags='', runner_env={},
                 mock_library=None, runner_must_fail=False,
                 classpath=['/usr/share/java/junit4.jar'], **kargs):
        self.objs = objs
        self.testfile = testfile
        self.compiling_flags = '{0}'.format(compiling_flags).strip()
        self.mock_library = mock_library
        self.runner_env = runner_env
        self.generated_file = None
        self.runner_must_fail = runner_must_fail
        self.classpath = classpath
        self.temp_files = []

        tasks = self._create_tasks()

        CompositeTask.__init__(self, all, *tasks, **kargs)

    def close(self):
        for fd, filename in self.temp_files:
            os.remove(filename)
        super(JUnit, self).close()

    def _create_tasks(self):
        result = []

        class_file = self.testfile

        if self.testfile.endswith('.java'):
            temp = tempfile.mkstemp(suffix=".class", dir=atheist.const.ATHEIST_TMP)
            self.temp_files.append(temp)
            class_file = temp[1]
            compiler = self.Compiler(self.testfile, class_file, self.classpath)
            result.append(compiler)

        runner = self.Runner(class_file, self.classpath)

        result.append(runner)

        return result

    def repr_args(self):
        return self.testfile
