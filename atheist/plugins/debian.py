# -*- mode: python; coding: utf-8 -*-

# Copyright (C) 2009-2011 Cleto Martín <cleto.martin@gmail.com>

import sys, os
import commands

from pyarco.Type import accept

import atheist
from atheist.log import Log
from atheist.condition import Condition
from atheist.task import Subprocess
from atheist.utils import base_abspath, run_cmd

'''
[**plugin:** {0}]
'''.format(__file__)


class DebPkgInstalled(Condition, atheist.iface.Plugin):
    """
    Check the given debian package is installed. Optionally a minimum version
    may be specified.
    """

    @accept(package_name=str, min_version=str)
    def __init__(self, package_name, min_version=None):
        self.package = package_name
        self.version = min_version
        Condition.__init__(self)

    def run(self):
        rcode, info = run_cmd("dpkg -l %s | grep ^ii" % self.package)
        retval = (rcode == 0)

        if retval and self.version:
            installed = info.split()[2].strip()
            retval &= installed >= self.version

            Log.debug("%s: pkg:%s inst:%s req:%s" %
                       (self.__class__.__name__, self.package,
                        installed, self.version))
        return retval

    def __eq__(self, other):
        return Condition.__eq__(self, other) \
            and self.package == other.package \
            and self.version == other.version

    def repr_args(self):
        return u"'%s'" % self.package


class DebPkgBuild(Subprocess, atheist.iface.Plugin):
    # FIXME: Assure an available pdebuild environment
    """Test a Debian package building from 'debian' directory by using pbuilder
    """

    def __init__(self, debian_dir, timeout=3600, **kargs):
        assert isinstance(debian_dir, str)
        self.buildir = '/tmp/'
        Subprocess.__init__(
            self,
            cmd = "pdebuild --buildresult %s" % self.buildir,
            timeout=timeout, **kargs)

        self.cwd = debian_dir
        self.pre += DebPkgInstalled("pbuilder")

        with file(base_abspath('debian/changelog', self.cwd)) as fd:
            self.version  = fd.readline().split()[1].strip('()')

        self.__pkgs = self.get_package_files()

        # FIXME: use a configure method
        if not self.__pkgs:
            Log.error("No packages declared at '%s/debian/control' file" % self.cwd)
            sys.exit(1)

        for p in self.__pkgs:
            self.gen += p

    def get_package_files(self):
        retval = []
        fcontrol = file(base_abspath('debian/control', self.cwd))
        for l in fcontrol.read().split('\n\n'):
            try:
                name, arch = l.splitlines()[0:2]
                if "Package:" not in name:
                    continue

                name = name.split(':')[1].strip()
                arch = arch.split(':')[1].strip()

                if arch == "any":
                    arch = commands.get_output('dpkg-architecture -qDEB_BUILD_ARCH')

                pkg_name = os.path.join(
                    self.buildir,
                    '{name}_{version}_{arch}.deb'.format(
                        name = name,
                        version = self.version,
                        arch=arch))

                retval.append(pkg_name)

            except IndexError:
                continue

        fcontrol.close()
        return retval

    @property
    def packages(self):
        return self.__pkgs

    def repr_args(self):
        return os.path.abspath(self.cwd + "/debian")


class DebPkgInstall(Subprocess, atheist.iface.Plugin):
    """Test a Debian package installation using piuparts"""

    def __init__(self, deb_files,
                 timeout = 3600,
                 mirrors = ["http://ftp.debian.de/debian"],
                 basetgz = None,
                 verify_signs = True,
                 warn_broken_symlinks = False,
                 **kargs):

        assert isinstance(deb_files, list)
        assert all([isinstance(x, str) for x in deb_files])

        self.debs = deb_files

        cmd = "sudo piuparts "

        # FIXME: use a configure method
        if not self.debs:
            Log.error("No packages to install")
            sys.exit(1)

        for d in self.debs:
            cmd += '%s ' % d
        for m in mirrors:
            cmd += '-m %s ' % m

        if basetgz:
            assert isinstance(basetgz, str)
            cmd += '-b "%s" ' % basetgz

        if not verify_signs:
            cmd += '--do-not-verify-signatures 1 '

        if warn_broken_symlinks:
            cmd += '-W '

        Subprocess.__init__(self, cmd=cmd, timeout=timeout, **kargs)

        self.pre += DebPkgInstalled("piuparts")
        for d in self.debs:
            self.pre += atheist.FileExists(d)

    def repr_args(self):
        return str(self.debs)
