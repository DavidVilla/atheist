# -*- coding:utf-8; tab-width:4; mode:python -*-
import re

import atheist
from atheist.condition import FileContains

class FileContainsRE(FileContains, atheist.iface.Plugin):
    """Same of ``FileContains()`` but using a regular expression."""

    def run(self):
        try:
            return FileContains.run(self)
        except re.error, e:
            atheist.Log.warning(e)
            return atheist.const.ERROR


    @classmethod
    def check(cls, expected, content):
        pattern = re.compile(expected)
        return len(pattern.findall(content))
