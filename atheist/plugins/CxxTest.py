# -*- mode: python; coding: utf-8; fill-column: 95 -*-

# cxxtest plugin for atheist
#
# Copyright (C) 2010 Cleto Martín, David Villa
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA

import os

import atheist
from atheist import iface, condition
from atheist.task import Subprocess, TestFunc
from atheist import CompositeTask


class RunningError(Exception):
    pass


def append_auto_condition(container, condition):
    condition.auto = True
    container += condition


class GoogleMocks:
    def __init__(self, fname):
        self._fname = fname
        self.compiling_flags = '-lgmock -lgtest -lpthread'

    def run(self):
        code_file = open(self._fname, 'rw+')
        code = code_file.readlines()
        code_file.seek(0)
        code_file.writelines(self._add_code(code))
        code_file.close()
        return 0

    def _add_code(self, code):
        main_pattern = 'int main() {\n'
        mark = code.index(main_pattern)

        new_code = ['#include <gmock/gmock.h>\n',
                    'int main(int argc, char **argv) {\n',
                    '::testing::GTEST_FLAG(throw_on_failure) = true;\n',
                    '::testing::InitGoogleMock(&argc, argv);\n',
                    ]

        retval = code[0:mark - 1] + new_code + code[mark + 1:]
        return retval


class MockLibrary(TestFunc):

    libraries = {'google-mocks': GoogleMocks}

    def __init__(self, library, fname):
        if library not in self.libraries.keys():
            raise RuntimeError("Mock library '%s' not supported" % library)

        self.the_library = self.libraries[library](fname)

        TestFunc.__init__(self, self.the_library.run)

    def get_compiling_flags(self):
        return self.the_library.compiling_flags


class Generator(Subprocess):
    def __init__(self, testfile, **kargs):
        self.testfile = os.path.abspath(testfile)
        self.outputfile = self._gen_output_filename(self.testfile)
        cmd = 'cxxtestgen --error-printer -o %s %s' % (self.outputfile,
                                                       self.testfile)
        Subprocess.__init__(self, cmd=cmd, shell=True, **kargs)
        self.gen += self.outputfile

    def _gen_output_filename(self, fname):
        # /home/user/test.cpp -> ATHEIST_TMP/test_runner
        outfile, extension = os.path.splitext(os.path.basename(fname))
        retval = os.path.join(atheist.const.ATHEIST_TMP,
                              outfile + '_runner' + extension)
        return retval

    def repr_args(self):
        return self.testfile


class CxxCompiler(Subprocess):
    def __init__(self, main_file, objs, cxxflags, **kargs):
        self.main_file = main_file
        self.objs = self._get_objs(objs)
        self.cxxflags = cxxflags
        self.outputfile = os.path.splitext(self.main_file)[0]

        cmd = 'g++ %s %s %s -o %s' % (self.main_file, self.cxxflags,
                                      str.join(' ', self.objs), self.outputfile)
        Subprocess.__init__(self, cmd=cmd, **kargs)

        for o in self.objs:
            self.pre += condition.FileExists(o)
        self.gen += self.outputfile

    def _get_objs(self, objs):
        retval = []
        for dirname, files in objs.items():
            retval.extend([os.path.join(dirname, f) for f in files])
        return retval

    def repr_args(self):
        return self.main_file


class CxxTest(CompositeTask, iface.Plugin):
    """Prepare and run C/C++ unittests using CxxTest library"""
    acro = 'Cxx'

    def __init__(self, testfile, objs={}, compiling_flags='', runner_env={},
                 mock_library=None, runner_must_fail=False, **kargs):

        self.objs = objs
        self.testfile = testfile
        self.compiling_flags = '{0} -lcxxtest'.format(compiling_flags).strip()
        self.mock_library = mock_library
        self.runner_env = runner_env
        self.generated_file = None
        self.runner_must_fail = runner_must_fail

        tasks = self._create_tasks()

        CompositeTask.__init__(self, all, *tasks, **kargs)

    def _create_tasks(self):
        generator = Generator(self.testfile)
        self.generated_file = generator.outputfile
        append_auto_condition(generator.pre, condition.FileExists(self.testfile))

        if self.mock_library:
            mock = self.MockLibrary(self.mock_library, generator.outputfile)
            append_auto_condition(mock.pre, condition.FileExists(generator.outputfile))

            self.compiling_flags += ' ' + mock.get_compiling_flags()

        compiler = CxxCompiler(generator.outputfile,
                                    self.objs, self.compiling_flags)
        append_auto_condition(compiler.pre, condition.FileExists(generator.outputfile))

        runner = Subprocess(compiler.outputfile, env=self.runner_env,
                                    must_fail=self.runner_must_fail)
        append_auto_condition(runner.pre, condition.FileExists(compiler.outputfile))

        tasks = [generator, compiler, runner]
        if self.mock_library:
            tasks.insert(1, mock)

        return tasks

    def repr_args(self):
        return self.testfile
