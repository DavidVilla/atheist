# -*- mode:python; coding:utf-8; tab-width:4 -*-

#import optparse

import atheist
from atheist.log import Log
from atheist.conf import settings


class AliasValueError(Exception):
    pass


class Alias(atheist.iface.Plugin):

    @classmethod
    def add_arguments(cls, parser):

        def alias_is_right(value, valid):
            for opt in [x for x in value.split() if x.startswith('--')]:

                if not opt in valid:
                    Log.error("[alias] '%s' is not a valid option" % opt)
                    return False

            return True

        if 'alias' not in settings:
            return []

        valid_options = parser.get_all_prefixes()
        aliases = parser.add_argument_group(title="Aliases")

        for key, value in settings.alias.items():
            if not alias_is_right(value, valid_options):
                continue

            aliases.add_argument(
                '--%s' % key, action='store_true',
                help="User alias for '%s'" % value)

    @classmethod
    def config(cls, parser):

        def configure_alias(key, value):
            if not hasattr(settings, key) or not getattr(settings, key):
                return

            Log.info("Applying alias '%s': %s", key, value)
            arguments = parser.parse_args(value.split())

            alias_opts = [x for x in value.split() if x.startswith('--')]

            for name in alias_opts:
                attr_name = parser.get_option(name).dest
                value = getattr(arguments, attr_name)
                setattr(settings, attr_name, value)

        if not hasattr(settings, 'alias'):
            return

        for key, value in settings.alias.items():
            configure_alias(key, value)
