# -*- mode:python; coding:utf-8; tab-width:4 -*-

import sys
import os
import inspect
from operator import attrgetter

from jinja2 import Environment, FileSystemLoader

from pyarco.Type import split_if, Record

import atheist
from atheist.log import Log
from atheist.conf import settings
from atheist import condition


class DocGenerator(atheist.Runner, atheist.iface.Plugin):

    @classmethod
    def add_arguments(cls, parser):
        parser.plugin_group.add_argument(
            '--gen-docs', action='store_true', default=False,
            help="Generate docs from jinja templates")

    @classmethod
    def config(cls, parser):

        if not settings.gen_docs:
            return

        settings.RunnerClass = cls
        cls.parser = parser

    def run(self):
        hooks = [
            ('atheist.rst.tmpl',            'parser',     lambda:self.parser),
            ('doc/invoking.rst.inc.tmpl',   'parser',     lambda:self.parser),
            ('doc/task_types.rst.inc.tmpl', 'tasks',      self.tasks),
            ('doc/conditions.rst.inc.tmpl', 'conditions', self.conditions),
            ('doc/keywords.rst.inc.tmpl',   'keywords',   self.keywords)]

        base = os.path.split(sys.argv[0])[0]
        env = Environment(loader = FileSystemLoader(['/', settings.curdir, base]))

        for name, symbol, hook in hooks:
            template = env.get_template(name)
            outname = name[:-5]
            Log.info("Generating %s", outname)

            with file(os.path.join(base, outname), 'w') as fd:
                fd.write(template.render(**{symbol: hook()}))

        sys.exit(0)

    def tasks(self):
        retval = self.filter_classes(self.mng.api.get_Task_classes())
        retval.remove(atheist.task.Test)
        return self.tag_classes(retval, exclude=['__init__', 'task'])

    def conditions(self):
        retval = split_if(self.filter_classes(self.mng.api.get_Condition_classes()),
                          lambda x: issubclass(x, condition.ConditionDecorator))[1]

        return self.tag_classes(retval, exclude=['condition'])

    def keywords(self):

        def get_type(record):
            if isinstance(record.type_, tuple):
                return str.join(', ', [x.__name__ for x in record.type_])

            return record.type_.__name__

        def get_default(record):
            if record.default == '':
                return ' '

            if record.type_ is dict and record.default:
                return '{...}'

            return record.default

        retval = []
        for name, record in atheist.KeyWords.items():
            value = Record(str_type = get_type(record),
                           default = get_default(record),
                           desc = record.desc)
            retval.append((name, value))

        return retval

    def filter_classes(self, seq):
        retval = [x for x in seq
                  if not x.__doc__ or not x.__doc__.startswith(":skip:")]

        return sorted(retval, key=attrgetter('__name__'))

    def tag_classes(self, seq, exclude=[]):
        '''Tag a sequence of classes to add __init__ arg spec, inline doc and
        file it is defined.
        '''
        for c in seq:
            try:
                argspec = inspect.getargspec(c.__init__)
                c.args = inspect.formatargspec(argspec.args[1:], argspec.varargs,
                                               argspec.keywords, argspec.defaults)
            except TypeError:
                c.args = ''

            if c.__doc__ is None:
                c.doc = ''
            else:
                c.doc = c.__doc__
                if not c.doc.startswith('\n'):
                    c.doc = '\n    ' + c.doc.lstrip()

            fname = os.path.splitext(os.path.split(inspect.getfile(c))[1])[0]
            if fname not in exclude:
                c.plugin = fname

        return seq
