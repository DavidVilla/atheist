# -*- mode:python; coding:utf-8 -*-

import socket
import logging

import atheist
from atheist import const
from atheist.log import Log
from atheist.conf import settings
from atheist.utils import escape_xml
from atheist.reporter import Reporter


class XUnitReporter(Reporter, atheist.iface.Plugin):
    xunitfile = 'atheist.xml'

    def __init__(self, xunitfile=None):
        Reporter.__init__(self, dst='xunit')
        if xunitfile is None:
            self.xunitfile = XUnitReporter.xunitfile

        self.with_hostname = True
        self.with_time = True
        self.data = ''

    def render(self, suite, fd=None):
        if fd is None:
            fd = file(self.xunitfile, 'w')

        head = '<?xml version="1.0" encoding="{encoding}"?>'
        head += '<testsuite name="atheist"'
        if self.with_hostname:
            head += ' hostname="{hostname}"'
        head += ' tests="{total}" errors="{errors}" failures="{failures}"'
        head += ' skip="{skipped}">'

        suite.calculate()
        with fd:
            fd.write(head.format(
                    encoding='UTF-8',
                    hostname=socket.gethostname(),
                    **suite.__dict__))
            fd.write(self.data)
#            for case in suite:
#                fd.write(self._render_case(case))
            fd.write('</testsuite>')

    def add_case(self, case):
        self.data += self._render_case(case)

    def _render_case(self, case):

        def render_reason(task):
            retval = ''
            err_names = {const.ERROR: 'error', const.FAIL: 'failure'}

            if task.result in [const.ERROR, const.FAIL]:
                retval += '<{tag} type="{tag}" >{message}</{tag}>'.format(
                    tag = err_names[task.result],
                    message = settings.fs.getcontents(task.filelog_name))

            return retval

        def file_content(fname):
            retval = ''
            try:
                retval = file(fname).read()
            except IOError:
                Log.error("Missing file: %s", fname)

            return escape_xml(retval) if retval else ''

        def render_outs(task):

            return '''\
<system-out>{stdout}</system-out>\
<system-err>{stderr}</system-err>'''.format(
                stdout = file_content(task.stdout),
                stderr = file_content(task.stderr))

        retval = ''
        for task in case.tasks:
            head = '<testcase classname="{casename}" name="{testname}"'
            if self.with_time:
                head += ' time="{elapsed}" '
            head += ' >'

            xml_case = head.format(
                casename=case.name.lstrip('./'),
                testname=task.name,
                elapsed=case.elapsed)

            xml_case += render_reason(task)
            xml_case += render_outs(task)
            xml_case += '</testcase>'

            retval += xml_case

        return retval

    @classmethod
    def add_arguments(cls, parser):
        parser.plugin_group.add_argument(
            '--with-xunit', action='store_true', default=False,
            help='Enable plugin Xunit: This plugin provides test results '
            'in the standard XUnit XML format')
        parser.plugin_group.add_argument(
                '--xunit-file', type=str, default=cls.xunitfile, metavar='FILE',
                help='Path to xml file to store the xunit report in. Default '
                     'is %s in the working directory' % cls.xunitfile)

    @classmethod
    def config(cls, parser):
        if not settings.with_xunit:
            return

        settings.reporters.append(XUnitReporter(settings.xunit_file))
        settings.clean_first = True
        settings.loglevel = logging.DEBUG
        settings.disable_bar = True
        settings.plain = True
        settings.save_stdout = True
        settings.save_stderr = True
        settings.save_logs = True
