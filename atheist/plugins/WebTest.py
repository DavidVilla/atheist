# -*- mode: python; coding: utf-8 -*-

import urllib

#import pycurl

import atheist
from atheist.log import Log


def get_arg(dic, key, default=None):
    if not dic.has_key(key):
        return default

    retval = dic[key]
    del dic[key]
    return retval

# curl command uses OpenSSL that works in many more cases that curl
# module (that uses GNU TSL). For this reason, we prefere the command.
#
#class WebTest(atheist.Task):
#    acro = 'web'
#    allows = ['check', 'delay', 'desc', 'must_fail', 'save_stdout', 'stdout']
#
#    def __init__(self, url, **kargs):
#        self.url = url
#        self.http_post =  get_arg(kargs, 'http_post')
#        self.cookie =     get_arg(kargs, 'cookie')
#        self.ssl_verify = get_arg(kargs, 'ssl_verify', True)
#        self.ssl_cert =   get_arg(kargs, 'ssl_cert')
#
#        atheist.Task.__init__(self, **kargs)
#
#        if self.ssl_cert:
#            self.pre += atheist.FileExists(self.ssl_cert)
#
#        if not self.desc:
#            self.desc = self.url
#
#
#    def run(self):
#        c = pycurl.Curl()
#        c.setopt(c.VERBOSE, 1)
#
#        c.setopt(pycurl.URL, self.url)
#        c.setopt(pycurl.WRITEFUNCTION, self.outwrap.write)
#
#        if self.cookie:
#            c.setopt(pycurl.COOKIEFILE, self.cookie)
#        if self.http_post:
#            c.setopt(pycurl.POSTFIELDS, urllib.urlencode(self.http_post))
#        if not self.ssl_verify:
#            c.setopt(pycurl.SSL_VERIFYPEER, False)
#        if self.ssl_cert:
#            c.setopt(pycurl.SSLCERT, self.ssl_cert)
#
#        c.setopt(c.SSL_VERIFYHOST, False)
#
#
#        try:
#            c.perform()
#            self.result = atheist.OK
#
#        except pycurl.error, e:
#            atheist.Log.error("%s: %s" % (self.url, e))
#            self.result = atheist.FAIL
#
#        finally:
#            c.close()


class WebTest(atheist.task.Subprocess, atheist.iface.Plugin):
    """Check an `url` is correct and accessible.

    **Authentication with cookie:**

    If you want to get a page from a restricted site you may need a cookie
    file::

      WebTest('example.org', cookie='cookie.txt')

    To get the cookie you may use a ``curl`` command similar to this::

      $ curl -c cookie.txt -d "name=John.Doe&pass=secret&form_id=user_login" http://example.orrg/login

    Specific keywords:
    - cookie: Cookie file.
    - http_post: Form content dictionary.
    - insecure: Ignore SSL certificates.
    """
    acro = 'Web '
    allows = ['check', 'must_fail', 'save_stdout', 'stdout', 'timeout']

    def __init__(self, url, **kargs):
        self.url = url
        self.cookie =    get_arg(kargs, 'cookie')
        self.http_post = get_arg(kargs, 'http_post')
        self.insecure =  get_arg(kargs, 'insecure')

        # cmd = 'curl -Ss '
        cmd = 'curl -f --location '

        if self.cookie:
            cmd += '--cookie %s ' % self.cookie

        if self.http_post:
            cmd += '--data "%s" ' % urllib.urlencode(self.http_post)

        if self.insecure:
            cmd += '-k '

        cmd += self.url

        atheist.task.Subprocess.__init__(self, cmd=cmd, **kargs)
        self.shell = True

        if self.cookie:
            self.pre += atheist.condition.FileExists(self.cookie)


        self.auto_desc = self.url.decode('utf-8')
