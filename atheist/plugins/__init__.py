# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import imp
import inspect

import atheist
from atheist.utils import *

log = logging.getLogger('atheist.plugin')
log.propagate = True
log.addHandler(atheist.log.NullHandler())


class Loader(object):
    def __init__(self, dname):
        self.plugins = []
        for m in self.load_modules(dname):
            klases = [x[1] for x in inspect.getmembers(m, inspect.isclass)]
            if not klases:
                continue

            plugins = [x for x in klases if \
                           issubclass(x, atheist.iface.Plugin) \
                           and x.is_enabled()]

            if not plugins:
                continue

            log.debug("PluginManager: {0} {1}".format(
                    os.path.relpath(m.__file__, dname),
                    [x.__name__ for x in plugins]))
            self.plugins.extend(plugins)

    @staticmethod
    def load_modules(dirname):
        banned = []
        files = []
        for x in os.listdir(dirname):
            if x in banned or x[0] in ".#":
                continue

            if os.path.isdir(os.path.join(dirname, x)) or x.endswith('.py'):
                files.append(os.path.splitext(x)[0])

        files.sort(key=str.lower)

        modules = []
        for f in files:
            try:
                modules.append(Loader.load_module(f, dirname))
            except ImportError, e:
                Log.error("Load error for plugin '%s': %s", f, e)

        return modules

    @staticmethod
    def load_module(filename, dirname):
        fp, fpath, desc = imp.find_module(filename, [dirname])
#        log.debug("Module: %s %s" % (fpath, desc))
        try:
            return imp.load_module(filename, fp, fpath, desc)
        finally:
            if fp:
                fp.close()

    def __iter__(self):
        return iter(self.plugins)
