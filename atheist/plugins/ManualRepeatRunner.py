# -*- mode:python; coding:utf-8 -*-

import time
from multiprocessing import Process

import atheist
from atheist.log import Log
from atheist.conf import settings

from pyarco.UI import clear


class ManualRepeatRunner(atheist.Runner, atheist.iface.Plugin):

    @classmethod
    def add_arguments(cls, parser):
        parser.plugin_group.add_argument(
            '--again', action='store_true', default=False,
            help='Repeat tests when user press ENTER')

    @classmethod
    def config(cls, parser):
        if not settings.again:
            return

        settings.RunnerClass = cls

    def subprocess(self):
        def simple_runner():
            try:
                atheist.Runner(self.mng).run()
            except atheist.ExecutionError, e:
                Log.error(e)

        p = Process(target=simple_runner)
        p.start()
        p.join()

    def run(self):
        while 1:
            clear()
            print time.asctime()

            self.subprocess()

            if self.mng.aborted:
                break

            try:
                raw_input("\n-- Press ENTER to run tests again... ")
            except EOFError:
                self.mng.abort()
                break

        return 0
