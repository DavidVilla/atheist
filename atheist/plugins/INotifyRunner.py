# -*- mode:python; coding:utf-8 -*-

import sys
import time
from multiprocessing import Process

import pyinotify as inotify

import atheist
from atheist.log import Log
from atheist.conf import settings


class INotifyRunner(atheist.Runner, atheist.iface.Plugin):
    '''Re-run when a monitor file changes'''

    path = None
    watch_tests = False

    def __init__(self, mng):
        atheist.Runner.__init__(self, mng)
        self.tinit = time.time()

    @classmethod
    def add_arguments(cls, parser):
        parser.plugin_group.add_argument(
            "--watch-tests", action='store_true', default=False,
            help='Automatically re-run tests when they change')
        parser.plugin_group.add_argument(
                "--watch-file", metavar='FILE', default=None,
                help='Automatically re-run tests when the given file or directory changes')

    @classmethod
    def config(cls, parser):
        if settings.watch_tests:
            cls.watch_tests = True
            settings.RunnerClass = cls

        if settings.watch_file:
            cls.path = settings.watch_file
            settings.RunnerClass = cls

    def subprocess(self):
        def simple_runner():
            atheist.Runner(self.mng).run()

        p = Process(target=simple_runner)
        p.start()
        time.sleep(1)
        p.join()

    def run(self):
        class EventHandler(inotify.ProcessEvent):
            def __init__(self, runner, watch_mng, paths, flags):
                self.runner = runner
                self.wm = watch_mng
                self.paths = paths
                self.flags = flags
                self.wd = {}

                self.on()

            def on(self):
                self.wd = self.wm.add_watch(self.paths, self.flags, rec=True, do_glob=True)

            def off(self):
                self.wm.rm_watch(self.wd.values())
                self.wd.clear()

            def process_default(self, event):
                if event.mask & self.flags == 0:
                    return

#                self.off()
                Log.info("%s: %s", event.maskname, settings.relpath(event.pathname))
                self.runner.subprocess()
#                self.on()

        def stop():
            Log.info('stopping notifier')
            self.notifier.stop()
            self.notifier.stop()

        self.setup()
        self.subprocess()

        paths = []
        if self.watch_tests:
            print "Waiting for changes in test files...",
            sys.stdout.flush()
            paths.extend([case.fname for case in self.suite])

        if self.path:
            print "Waiting for changes in '%s'" % self.path
            paths.append(self.path)

        wm = inotify.WatchManager()
        handler = EventHandler(self, wm, paths, inotify.IN_MODIFY)
        self.notifier = inotify.Notifier(wm, handler, read_freq=1, timeout=1)

        self.mng.abort_observers.append(stop)
        try:
            self.notifier.loop()
            return
        except KeyError:
            pass
