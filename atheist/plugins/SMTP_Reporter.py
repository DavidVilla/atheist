# -*- mode:python; coding:utf-8 -*-
import sys
import socket
import datetime

from loggingx import SMTP_SSLHandler

import atheist
from atheist.log import Log
from atheist.conf import settings

from atheist.reporter import FailReporter


class MySMTPHandler(SMTP_SSLHandler):
    def getSubject(self, record):
        return "{0:%Y.%m.%d} - {1}".format(
            datetime.datetime.today(),
            record.msg.split('\n')[0])


class SMTP_Reporter(FailReporter, atheist.iface.Plugin):
    width = 120

    def __init__(self, toaddrs):
        try:
            account = settings['smtp.default']
        except KeyError:
            raise atheist.ConfigureError("Your config requires a 'smtp.default' section.")

        host = account['host']
        port = int(account.get('port', 25))

        try:
            fromaddr = account['user']
            credentials = (fromaddr, account['pasw'])
        except KeyError:
            fromaddr = 'atheist@' + socket.gethostname()
            credentials = None

        ssl = account.get('ssl', False)

        Log.debug('SMTP_Reporter.mailhost: {0}:{1} ssl:{2}'.format(host, port, ssl))
        Log.debug('SMTP_Reporter.user: {0}'.format(credentials))

        try:
            handler = MySMTPHandler((host, port), fromaddr, toaddrs,
                                    subject='Atheist report',
                                    credentials=credentials,
                                    ssl=ssl)
        except AttributeError, e:
            Log.error("You must give a smtp account in your config file. Key: %s" % e.args)
            sys.exit(1)
        except socket.error, e:
            Log.error("{0}: {1}", host, e)
            return

        FailReporter.__init__(self, 'smtp://' + toaddrs, handler, plain = True)

    @classmethod
    def add_arguments(cls, parser):
        FailReporter.add_arguments(parser)
        parser.plugin_group.add_argument(
            '--notify-smtp',  metavar='ACCOUNT',
            dest='mail_accounts', action='append', default=[],
            help='notify failed tasks to the given email address')

    @classmethod
    def config(cls, parser):
        for account in settings.mail_accounts:
            settings.reporters.append(SMTP_Reporter(account))
