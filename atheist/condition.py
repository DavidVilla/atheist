#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

import os
import sys
import stat
import time
import types
import traceback

#FIXME una función equivalente con subprocess para todas la ejecuciones síncronas de comandos
import commands

from pyarco.Type import checked_type, attributes, accept, Undefined

from .log import Log
from .utils import process_exists, print_exc
import atheist.const as const
from .iface import Public
from .atypes import Condition, ConditionList
from .conf import settings

try:
    from fs.error import ResourceError
except ImportError:
    from .fs import ResourceError


class AtheistVersion(Condition, Public):
    """Check that the installed version of **atheist** is equal or newer than
    the given number. This is useful to assure recent or experimental features.
    """

    def __init__(self, version):
        self.version = self.str_to_float(version)
        Condition.__init__(self, 'version')

    def run(self):
        Log.debug("%s: inst:%s req:%s" %
                   (self.__class__.__name__, const.VERSION, self.version))
        return self.str_to_float(const.VERSION) >= self.version

    @staticmethod
    def str_to_float(string):
        retval = str.join('.', string.split('.')[:2])
        try:
            retval = float(retval)
        except ValueError, e:
            Log.error(e)
            sys.exit(1)

        return retval


class Callback(Condition, Public):
    """Call the specified function with the given arguments. This is a fragile
    check and must be avoided as much as possible.
    """

    @attributes('func', 'args')
    def __init__(self, func, args=()):
        assert callable(func)
        checked_type(types.TupleType, args)
        Condition.__init__(self, 'func', 'args')

    def run(self):
        try:
            retval = self.func(*self.args)
            if retval is None:
                Log.warning("%s() returns None" % self.func.__name__)
            return bool(retval)

        except Exception, e:
            Log.error("%s: %s" % (self.__class__.__name__, e))
            print_exc(traceback.format_exc(limit=1), Log)

        return const.FAIL

    def repr_args(self):
        return u"%s: '%s'" % (self.func.__name__, self.args)


class EnvVarDefined(Condition, Public):
    """
    Check the environment variable *varname* exists. If *varvalue* is given,
    the variable must have that value.
    """

    @accept(varname=str)
    @attributes('varname', 'varvalue')
    def __init__(self, varname, varvalue=None):
        Condition.__init__(self, 'varname', 'varvalue')

    def run(self):
        retval = self.varname in os.environ.keys()
        if retval and self.varvalue:
            retval = (os.environ[self.varname] == self.varvalue)
        return retval

    def repr_args(self):
        return u"'%s'" % self.varname


class FileExists(Condition, Public):
    """Check file *path* exists."""

    @accept(path=str)
    def __init__(self, path):
        self.path = os.path.abspath(path)
        Condition.__init__(self, 'path')

    def run(self):
        return self._task.fs.exists(self.path)

    def repr_args(self):
        return u"'{0}'".format(settings.relpath(self.path))

    @classmethod
    def create(cls, fname, task, condlist):
        """helper to add this condition as pre of others"""
        retval = FileExists(fname)

        if retval in condlist:
            return []

        retval.auto = True
        retval.pre_task_run(task, condlist)
        return [retval]

    @classmethod
    def create_negate(cls, fname, task, condlist):
        retval = Not(FileExists(fname))

        if retval in condlist:
            return []

        retval.auto = True
        retval.pre_task_run(task, condlist)
        return [retval]


class DirExists(FileExists, Public):
    """Check directory *path* exists."""

    def run(self):
        return FileExists.run(self) and self._task.fs.isdir(self.path)


class FileContains(Condition, Public):
    """Check file *fname* exists and contains *expected*, which may be a
    string or a string list.

    If *whole* is ``False`` (default) *expected* must be a string and the whole
    content of file must be equal to *expected*.

    Otherwise, the file must contain **at least** *times* occurrences of
    *expected*. The default value for *fname* is the stdout of the corresponding
    task, and it implies ``save_stdout=True``. This implies also the automatic
    creation of a ``FileExists`` condition for the file.

    The file content may be stripped with *strip* argument. No stripping by
    default.
    """

    @accept(strip=str, whole=bool, times=int)
    def __init__(self, expected, fname=None,  strip='', whole=False, times=1):
        if fname is None:
            fname = Undefined(owner=None, attr='stdout')

        assert isinstance(fname, Undefined) or fname,\
            "'fname' must be None or non-empty string"

        self.fname = fname
        self.strip = checked_type(str, strip)
        self.whole = checked_type(bool, whole)
        self.times = checked_type(int, times)

        if whole:
            assert isinstance(expected, str), "In whole-mode, expected must be a string"

        self.expected = self.check_contains(expected)
        Condition.__init__(self, 'fname', 'expected', 'times', 'whole', 'strip')

    def pre_task_run(self, task, condlist):
        task.log.debug(u'pre-run %s' % self)
        retval = Condition.pre_task_run(self, task, condlist)

        if isinstance(self.fname, Undefined):
            owner = self.fname.owner or task
            out = self.fname.attr
            owner.enable_outs(**{out:True})
            self.fname = getattr(owner, out)
            return retval

        retval.extend(FileExists.create(self.fname, task, condlist))

        return retval

    def check_contains(self, expected):
        if isinstance(expected, str):
            return [expected]
        elif isinstance(expected, list):
            assert all([isinstance(x, str) for x in expected]),\
                "FileContains args must be strings"
            return expected

        raise TypeError(expected)

    def run(self):
        if self.fname is None:
            Log.critical("Condition is not initialized!")
            return const.ERROR

        self.fname = os.path.abspath(self.fname)
        if not self._task.fs.exists(self.fname):
            self._task.log.error("'%s' does not exist" % settings.relpath(self.fname))
            return const.ERROR

        try:
            fcontent = self._task.fs.getcontents(self.fname)

        except ResourceError, e:
            self._task.log.error(e)
            return const.ERROR

        if self.strip:
            fcontent = fcontent.strip(self.strip)

        if self.whole:
            return fcontent == self.expected[0]

        #FIXME: check expected order
        return all(self.check(x, fcontent) >= self.times for x in self.expected)

    @classmethod
    def check(cls, expected, content):
        return content.count(expected)

    def repr_args(self):
        times = '' if self.times == 1 else '%s:' % self.times
        return u"'{0}' {1}{2}".format(self.fname, times, self.expected, self._task)


class StdOutContains(FileContains):
    """Check the task stdout contains *expected* data. Arguments have the same
    meaning as :py:obj:`FileContains` condition.

    Shortcut for ``FileContains(content, fname=<task>.stdout)``.
    """
    def __init__(self, expected, strip='', whole=False, times=1):
        FileContains.__init__(self, expected=expected,
                              fname=Undefined(owner=None, attr='stdout'),
                              strip=strip, whole=whole, times=times)


class StdErrContains(FileContains):
    """Check the task stderr contains *expected* data. Arguments have the same
    meaning as :py:obj:`FileContains` condition.

    Shortcut for ``FileContains(content, fname=<task>.stderr)``.
    """
    def __init__(self, expected, strip='', whole=False, times=1):
        FileContains.__init__(self, expected=expected,
                              fname=Undefined(owner=None, attr='stderr'),
                              strip=strip, whole=whole, times=times)


class FileEquals(Condition, Public):
    """
    Check content of *fname1* and *fname2* are identical. The default value
    for *fname2* is stdout of the current task, and implies
    ``save_stdout=True``.
    """

    def __init__(self, fname1, fname2):
        self.fname1 = checked_type(str, fname1)
        self.fname2 = checked_type((str, Undefined), fname2)

        if isinstance(fname2, str):
            assert fname2, "'fname2' must be a non-empty string"

        Condition.__init__(self, 'fname1', 'fname2')

    def pre_task_run(self, task, condlist):
        retval = Condition.pre_task_run(self, task, condlist)
        if isinstance(self.fname2, Undefined):
            owner = self.fname2.owner
            out = self.fname2.attr

            owner.enable_outs(stdout = True)
            self.fname2 = getattr(owner, out)

        retval.extend(FileExists.create(self.fname1, task, condlist))
        retval.extend(FileExists.create(self.fname2, task, condlist))
        return retval

    def run(self):
        try:
            return self._task.fs.getcontents(os.path.abspath(self.fname1)) == \
                self._task.fs.getcontents(os.path.abspath(self.fname2))

        except ResourceError, e:
            self._task.log.error(e)
            return const.ERROR

    def repr_args(self):
        return u"'%s' == '%s'" % (self.fname1, self.fname2)


class FileIsNewerThan(Condition, Public):
    """Check file *fname1* modification date is more recent that *fname2*."""

    def __init__(self, fname1, fname2):
        self.fname1 = checked_type(str, fname1)
        self.fname2 = checked_type(str, fname2)
        Condition.__init__(self, 'fname1', 'fname2')

    def run(self):
        try:
            time1 = self._task.fs.getinfo(os.path.abspath(self.fname1))['modified_time']
            time2 = self._task.fs.getinfo(os.path.abspath(self.fname2))['modified_time']
        except ResourceError, e:
            Log.error(e)
            return const.ERROR

        return int(time1 > time2)

    def repr_args(self):
        return u"'%s' is newer than '%s'" % (self.fname1, self.fname2)


class FileHasPermissions(Condition, Public):
    """Check file/directory permissions as a bitwise or"""

    def __init__(self, mask, fname):
        self.mask = mask
        self.fname = fname
        Condition.__init__(self, 'mask', 'fname')

    def pre_task_run(self, task, condlist):
        task.log.debug(u'pre-run %s' % self)
        retval = Condition.pre_task_run(self, task, condlist)
        retval.extend(FileExists.create(self.fname, task, condlist))
        return retval

    def run(self):
        try:
            permissions = stat.S_IMODE(self._task.fs.getinfo(os.path.abspath(self.fname))['st_mode'])
        except ResourceError, e:
            Log.error(e)
            return const.ERROR

        return (permissions & self.mask) == self.mask

    def repr_args(self):
        return u"'{0}' has permissions {1:o}".format(self.fname, self.mask)


class ProcessRunning(Condition, Public):
    """
    Check the given process *ps* belongs to a running process. Argument *ps*
    may be both PID (int) or program name (str).
    """

    def __init__(self, ps):
        Condition.__init__(self, 'pid')

        self.info = str(ps)
        if isinstance(ps, int):
            self.pid = ps
            return

        if isinstance(ps, str):
            pids = commands.getoutput('pidof %s' % ps).strip()
            if not pids:
                self.pid = None
                return

            self.pid = int(pids.split()[0])
            return

        raise AssertionError("ps must be int (the PID) or str (the program name)")

    def run(self):
        if not self.pid:
            return const.FAIL

        return process_exists(self.pid)

    def repr_args(self):
        return u"'%s'" % self.info


# class ConditionSplitterMixin(Condition):
#     def __init__(self, *items):
#         for i in items:
#             assert isinstance(i, self.ItemClass)
#
#         self.items = list(reversed(items))
#         self.item = self.items[0]
#         Condition.__init__(self, 'item')
#
#     def pre_task_run(self, task, condlist):
#         retval = Condition.pre_task_run(self, task, condlist)
#         for i in self.items[1:]:
#             c = self.__class__(i)
#             c.pre_task_run(task, condlist)
#             retval.append(c)
#
#         return retval


class TimeLessThan(Condition, Public):
    """Check the task execution time is less than the specified (in seconds) """

    def __init__(self, timeout):
        self.max = checked_type(int, timeout)
        Condition.__init__(self, 'max')
        self.tini = self.log = None
        self.elapsed  = 0

    def pre_task_run(self, task, condlist):
        self.log = task.log
        self.tini = time.time()
        return Condition.pre_task_run(self, task, condlist)

    def run(self):
        assert self.tini is not None, "Internal error"
        self.elapsed = time.time() - self.tini
        return self.elapsed <= self.max

    def repr_args(self):
        return "'{0} > {1:.1f}'".format(self.max, self.elapsed)


#FIXME: Clonación de Condition decoradas
class ConditionDecorator(Condition, Public):

    @accept(conds=Condition)
    def __init__(self, *conds):
        Condition.__init__(self)
        self.children = ConditionList('children', *conds)

    def clone(self):
        retval = Condition.clone(self)
        retval.children = self.children.clone()
        return retval

    def pre_task_run(self, task, condlist):
        retval = []
        for c in self.children:
            retval.extend(c.pre_task_run(task, condlist))
        retval.extend(Condition.pre_task_run(self, task, condlist))
        return retval

    def run(self):
        raise NotImplementedError

    def __eq__(self, other):
        if not Condition.__eq__(self, other):
            return False

        return sorted(self.children) == sorted(other.children)

    @property
    def name(self):
        return "%s (%s" % (self.__class__.__name__,
                          str.join(' ', [x.name for x in self.children]))

    def repr_args(self):
        return unicode(str.join(' ', [x.repr_args() for x in self.children]))


class Not(ConditionDecorator, Public):
    def __init__(self, condition):
        ConditionDecorator.__init__(self, condition)

    def run(self):
        val = self.children[0].evaluate()
        if isinstance(val, bool):
            return not(val)

        return const.ERROR


class Poll(ConditionDecorator, Public):
    def __init__(self, condition, interval=1, timeout=5):
        ConditionDecorator.__init__(self, condition)
        self.interval = interval
        self.timeout = timeout

    def run(self):
        enlapsed = 0
        while 1:
            Log.debug("Polling condition: %s" % self.children[0])
            retval = self.children[0].evaluate()
            if retval != const.FAIL:
                return retval

            if self.timeout is not None and enlapsed > self.timeout:
                break

            time.sleep(self.interval)
            enlapsed += self.interval

        return const.FAIL


class CompositeCondition(ConditionDecorator, Public):
    """Apply an arbitrary operator to compose results for given condition. It is
   the same idea of `compositetask`_ but applied to conditions. Sample::

     CompositeCondition(all, cond1, cond2)"""

    def __init__(self, oper, *conds):
        assert callable(oper)
        self.oper = oper
        ConditionDecorator.__init__(self, *conds)

    def run(self):
        return self.oper([c.evaluate() for c in self.children])

    @property
    def name(self):
        return self.__class__.__name__

    def repr_args(self):
        oper = "[{0}]".format(self.oper.__name__) if len(self.children) > 1 else ''
        return "{0} {1}".format(
            oper,
            str.join(', ', ["{0}({1})".format(x.name, x.repr_args())
                            for x in self.children]))


class ConditionSplitter(CompositeCondition):
    '''Mixin to build a set of conditions (grouped in a composite) from a single
    multivalued condition. Example:

    TaskRunning(t1, t2, t2)
    ==>
    CompositeCondition with:
       TaskRunning(t1)
       TaskRunning(t2)
       TaskRunning(t3)
    '''

    class Item(Condition):
        def __init__(self, checker, item):
            assert isinstance(item, checker.ItemClass)
            self.checker = checker
            self.item = item
            Condition.__init__(self, 'item')

        def run(self):
            return self.checker.check(self.item)

        @property
        def name(self):
            return self.checker.__class__.__name__

        def repr_args(self):
            # FIXME: assumes that item has a 'name' attribute!!
            return self.item.name

    def __init__(self, *items):
        CompositeCondition.__init__(self, all, *[self.Item(self, i) for i in items])

    def __eq__(self, other):
        return CompositeCondition.__eq__(self, other) and self.check is other.check

    def repr_args(self):
        return "[{0}]".format(
            str.join(', ', [x.repr_args() for x in self.children]))
