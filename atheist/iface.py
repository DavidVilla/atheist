#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-


class Manager(object):
    pass


class Public:
    "Base class for classes available for the client tasks"


class Plugin:
    "Base class for plugins"

    @classmethod
    def add_arguments(cls, parser):
        pass

    @classmethod
    def config(cls, parser):
        pass

    @classmethod
    def is_enabled(cls):
        return True
