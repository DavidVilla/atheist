
import os
import time
import logging

import atheist.const as const

pid = os.getpid()
curdir = os.getcwd()
init_time = time.time()
interpolation = {}

# logging
loglevel = logging.WARNING
console_log = True
timetag_fmt = ''

# arguments
task_args = ''
base_dir = curdir
clean_first = False
clean_only = False
config = []
config_file = const.ATHEIST_CFG
create = None
ignore = ''
keep_going = False
random = None
save_stdout = False
save_stderr = False
skip_hooks = False
workers = 1
case_time = False
cols = 80
describe = False
disable_bar = False
dirty = False
stderr = False
fails_only = False
gen_template = False
list_only = False
out_on_fail = False
plain = False
quiet = False
report_detail = 1
save_logs = False
stdout = False
time_tag = False
timeout_scale = 1
verbose = 0
