# -*- mode: python; coding: utf-8 -*-

import sys
import os
import subprocess
import logging
import abc
import xml.sax.saxutils as saxutils
from functools import partial
import StringIO

from pyarco.Conio import *
from atheist import const
from atheist.log import Log
import atheist.gvar


def count(n, unit):
    """
    >>> count(3, 'tick')
    '3 ticks'
    >>> count(1, 'dog')
    '1 dog'
    """
    return "%s %s%s" % (n, unit, 's' if n > 1 else '')


#FIXME una función equivalente con subprocess para todas la ejecuciones síncronas de comandos
def process_exists(pid):
    return os.system("ps ef %s > /dev/null" % pid) == 0


def run_cmd(cmd):
    devnull = open(os.devnull, 'w')
    ps = subprocess.Popen(cmd, shell=True,
                          stdout=subprocess.PIPE, stderr=devnull)
    output = ps.communicate()[0]
    devnull.close()
    ps.stdout.close()
    return ps.returncode, output


def remove_dups(val):
    """
    >>> remove_dups([1,2,3,1,2])
    [1, 2, 3]
    >>> remove_dups([1,1,2,1,2,1])
    [1, 2]
    """
    retval = []
    for x in val:
        if x not in retval:
            retval.append(x)

    return retval


class RelativePathConverter:
    """
    >>> rpc = RelativePathConverter('/usr/share')
    >>> rpc()
    '.'
    >>> rpc('/usr/share/doc')
    './doc'
    >>> rpc.enable = False
    >>> rpc('/usr/share/doc')
    '/usr/share/doc'
    >>> rpc('/var/log')
    '/var/log'
    """

    def __init__(self, root, enable=True):
        self.root = os.path.abspath(root)
        self.enable = enable

    def __call__(self, path=None):
        if path is None:
            path = self.root

        if not self.enable:
            return path

        try:
            retval = os.path.relpath(path, self.root)
        except ValueError:
            return path

        if retval[0] not in [os.sep, os.curdir]:
            retval = os.curdir + os.sep + retval

        if len(retval) > len(path):
            return path

        return retval


def base_abspath(path, base=None):
    """return an absolute path.

    >>> base_abspath('/usr/share')
    '/usr/share'
    >>> base_abspath('/usr/share', 'usr')
    '/usr/share'

    >>> os.chdir('/usr')
    >>> base_abspath('share')
    '/usr/share'
    """
    if os.path.isabs(path):
        return path

    if base is None:
        if isinstance(path, unicode):
            base = os.getcwdu()
        else:
            base = os.getcwd()

    return os.path.normpath(os.path.join(base, path))


def print_exc(exc, logger=logging):
    for line in exc.split('\n'):
        if line.strip():
            logger.error('| ' + line)


class TreeRender:
    '''generic tree renderer'''

    __metaclass__ = abc.ABCMeta

    @classmethod
    def draw(cls, sequence, func, connector='', level=0, args=[]):
        retval = ''
        lon = len(sequence)
        for i, item in enumerate(sequence):
            brother = cls.norm_brother
            parent  = cls.norm_parent

            if lon == i + 1:
                brother = cls.last_brother
                parent  = cls.last_parent

            retval += func(item,
                           connector + parent,
                           connector + brother,
                           level + 1,
                           *args)

        return retval


class UTF_TreeRender(TreeRender):
    norm_brother = u'├─'
    last_brother = u'└─'
    norm_parent = u'│  '
    last_parent = u'   '


class ASCII_TreeRender(TreeRender):
    norm_brother = '+-'
    last_brother = '`-'
    norm_parent = '|  '
    last_parent = '   '


def high(val):
    return cout(HIGH, str(val), NORM)


def remove_escape(args):
    return [x for x in args if x[0] != ESC]


def cout(*args):
    if atheist.gvar.plain:
        args = remove_escape(args)

    return "%s" % str.join('', args)


def cout_config(plain=None):
    if plain is not None:
        atheist.gvar.plain = plain
        return

    term = os.getenv('TERM')
    cterm = os.getenv('COLORTERM')
    atheist.gvar.plain = not (cterm != '' or 'color' in term or 'xterm' == term)
    Log.debug("UI: Color out set to: %s" % atheist.gvar.plain)


def pretty(value, plain=False):
    COLOR = {\
        const.FAIL:       (['[', LIGHT_RED],            [NORM, ']']),
        const.OK:         (['[', GREEN],                [NORM, ']']),
        const.NOEXEC:     (['[', GREY],                 [NORM, ']']),
        const.ERROR:      (['[', BOLD, LIGHT_RED],      [NORM, ']']),
        const.UNKNOWN:    (['[', GREY],                 [NORM, ']']),
        const.TODO:       (['[', PURPLE],               [NORM, ']']),
        const.SOFT_FAIL:  (['[', RED],                  [NORM, ']']),
        const.MISCONF:    (['[', PURPLE],               [NORM, ']']),
        const.FINAL_FAIL: ([BOLD, LIGHT_RED_BG, '['],   [']', NORM]),
        const.FINAL_OK:   ([BOLD, LIGHT_GREEN_BG, '['], [']', NORM]),
        }

    assert isinstance(value, int)
    if plain:
        return "[%s]" % const.STR_STATUS[value]

    return cout(*(COLOR[value][0] + [const.STR_STATUS[value]] + COLOR[value][1]))


def escape_xml(data, encoding="utf-8", entities={}):
    entities.update(
        {'"': '&quot;',
         "'": '&apos;',
         })

    if isinstance(data, unicode):
        data = data.encode(encoding)

    return saxutils.escape(str(data), entities)


def escape_html(data, encoding="utf-8"):
    return escape_xml(data, encoding, {" ": '&nbsp;'})


class Isolator():
    """From nose isolation plugin:
    http://packages.python.org/nose/plugins/isolate.html"""

    def __enter__(self):
        self.before_mods = sys.modules.copy()

    def __exit__(self, *args):
        print "removing modules"
        for m in sys.modules.keys():
            if m not in self.before_mods:
                del sys.modules[m]

        sys.modules.update(self.before_mods)


def assure_unicode(value):
    if isinstance(value, unicode):
        return value

    try:
        return unicode(value)
    except UnicodeDecodeError:
        pass

    return unicode(value, 'utf-8', 'replace')


class FileWrapper:
    def __init__(self, console_out, file_handler, tag, only_on_fail=False):
        self.encoding = None
        self.prefix = '%s| ' % tag
        self.put_tag = True

        def file_write(text, fd):
            fd.write(assure_unicode(text))
            fd.flush()

        def file_write_tag(text, fd=sys.__stdout__):
            if len(text) == 0:
                return

            if self.put_tag:
                text = self.prefix + text
                self.put_tag = False

            out = text[:-1].replace('\n', '\n' + self.prefix) + text[-1]
            if text[-1] == '\n':
                self.put_tag = True

            fd.write(assure_unicode(out).encode('utf-8'))
            fd.flush()
        #--

        self.out_funcs = []
        self.last_task_out = None

        if console_out:
            self.out_funcs.append(file_write_tag)

        elif only_on_fail:
            self.last_task_out = StringIO.StringIO()
            self.out_funcs.append(partial(file_write_tag, fd=self.last_task_out))

        if file_handler:
            self.out_funcs.append(partial(file_write, fd=file_handler))

    def write(self, data):
        for func in self.out_funcs:
            func(data)

    def flush(self):
        pass

    def isatty(self):
        return False

    def print_fail_out(self):
        if not self.last_task_out:
            return  # it was not asked

        print self.last_task_out.getvalue(),
        sys.__stdout__.flush()


def str2bool(text):
    return str(text).lower() in ['true', 'yes', 'y', 'on', '1']
