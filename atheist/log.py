# -*- coding:utf-8; tab-width:4; mode:python -*-

import logging
logging.basicConfig(level=100)  # avoid logging messages from other loggers


# FIXME: this exists in new versions of standard library
class NullHandler(logging.Handler):
    def emit(self, record):
        pass

logging.getLogger().addHandler(NullHandler())


class CapitalLoggingFormatter(logging.Formatter):
    '''
    define variable "levelcapital" for message formating. You can do things like:
    [EE] foo bar
    '''

    def format(self, record):
        record.levelcapital = record.levelname[0] * 2
        return logging.Formatter.format(self, record)


def create_basic_handler():
    formatter = CapitalLoggingFormatter('[%(levelcapital)s] %(message)s')
    console = logging.StreamHandler()
    console.setFormatter(formatter)
    return console


def get_logger(name):
    retval = logging.getLogger(name)
    if not retval.handlers:
        retval.addHandler(create_basic_handler())
        retval.propagate = 0
        retval.setLevel(logging.WARNING)
    return retval


Log = get_logger('atheist')
