# -*- mode: python; coding: utf-8 -*-

import sys
import os
import string
import types
import select
import time
import subprocess as subp
import signal
import shlex
import errno

from pyarco.Type import accept, merge_uniq

from atheist import Task, run_task, AbortExc
from atheist.iface import Public
from atheist.log import Log
import atheist.const as const
from .atypes import TypedList
from atheist.condition import TimeLessThan
from atheist.utils import pretty


class Subprocess(Task):

    allows = Task.allows + ['detach', 'env', 'expected', 'path', 'parent',
                            'timeout', 'shell', 'signal']

    @accept(cmd=(str, types.NoneType))
    def __init__(self, cmd=None, **kargs):
        self.cmd = '' if cmd is None else cmd

        Task.__init__(self, **kargs)
        self.auto_desc = cmd if cmd else '<none>'

        self.env = self.environ_update()
        self.ps = None
        self.retcode = None

    def environ_update(self):
        environ = os.environ.copy()
        for k, v in self.env.items():
            environ[k] = string.Template(v).safe_substitute(environ)

        return environ

    def pre_run(self):
        if self.cmd and self.timeout != 0:
            t = TimeLessThan(self.timeout)
            t.auto = True
            self._runnin += t

        super(Subprocess, self).pre_run()

    def run(self):
        if not self.cmd:
            return const.OK

        if self.path:
            os.environ['PATH'] += ':' + self.path

        if self.shell:
            cmd = ['/bin/bash', '-c', "%s" % self.cmd]
        else:
            # FIXME: test that
            if set('><|;&').intersection(set(self.cmd)):
                self.log.warning("The command '%s' has special characters." % self.cmd)
                self.log.warning("Perhaps it requires 'shell=True'")

            cmd = shlex.split(self.cmd)

        try:
            self.ps = subp.Popen(
                cmd,
                close_fds  = True,
                stdin      = subp.PIPE,
                stdout     = subp.PIPE,
                stderr     = subp.PIPE,
                shell      = False,
                bufsize    = 0,
                cwd        = self.cwd,
                env        = self.env,
                preexec_fn = os.setsid)

        except OSError, e:
            self.log.error("%s: '%s'" % (e.strerror, self.cmd))
            return const.ERROR

        self.log.debug("starts (pid: %s)" % self.ps.pid)

        for c in self._runnin:
            c.pre_task_run(self, self._runnin)

        try:
            runin_conds = self._communicate()
        except select.error:
            self.mng.abort()
            return const.ERROR

        self.outwrap.write(self.ps.stdout.read())
        self.errwrap.write(self.ps.stderr.read())
        self.retcode = self.ps.returncode

        if runin_conds == const.FAIL:
            return const.FAIL

        if self.shell and self.retcode == 127:
            self.log.error("No such file or directory: '%s'" % self.cmd)
            return const.ERROR

        retval = (self.retcode == self.expected)
        self.log.debug("%s finish with %s" % (pretty(retval), self.retcode))

        if self.must_fail:
            retval = (self.retcode != 0)

        return retval

    def _communicate(self):
        retval = const.OK
        read_fds = [self.ps.stdout, self.ps.stderr]
        read_ready = select.select(read_fds, [], [], 0.05)[0]

        while self.ps.poll() is None or read_fds:
            for fd in read_ready:
                # http://mail.python.org/pipermail/python-bugs-list/2001-March/004491.html
                time.sleep(0.01)
                data = os.read(fd.fileno(), 2048).decode('utf-8', 'replace')

                if not data:
                    read_fds.remove(fd)
                elif fd is self.ps.stdout:
                    self.outwrap.write(data)
                elif fd is self.ps.stderr:
                    self.errwrap.write(data)

            read_ready = select.select(read_fds, [], [], 0.2)[0]

            if not self.eval_conditions(self._runnin, False):
                retval = const.FAIL
                self.kill()

        return retval

    def is_running(self):
        return self.ps is not None and self.ps.poll() is None

    def is_finished(self):
        return self.ps is not None and self.ps.poll() is not None

    def repr_args(self):
        return "'%s'" % self.cmd.decode('utf-8', 'replace')

    def describe(self):
        return Task.describe(self, ['cmd'])

    def clone(self):
        return Task.clone(self, self.cmd)

    def terminate(self):
        self.kill(self.signal)

    def kill(self, n=None, assure=True):
        if n is None:
            n = self.signal

        try:
            self.log.debug("(%s) receives signal %s", self.ps.pid, n)
            os.killpg(self.ps.pid, n)
        except OSError, e:
            if e.errno == errno.ESRCH:  # No such process
                return

            Log.debug("%s (%s)" % (e, self.ps.pid))
            return
        except AttributeError:
            self.log.warning("did not even start")
            return

        if not assure:
            return

        tini = time.time()
        while 1:
            if self.is_finished():
                break

            time.sleep(0.5)

            if (time.time() - tini) > 5:
                self.log.warning('is still alive after 5 seconds!')
                self.kill(signal.SIGKILL, assure=False)

            if (time.time() - tini) > 3:
                self.kill(n, assure=False)

    def close(self):
        if not self.ps:
            return

        self.ps.stdin.close()
        self.ps.stdout.close()
        self.ps.stderr.close()


class Test(Subprocess, Public):
    """
    Test(command, [key-vals])

    It is the very basic (and common) atheist primitive for system testing. It
    executes the given command and checks its return value. If that value differs
    from expected, the test fails.
    """
    acro = 'Test'


class TestBG(Test):
    """Shortcut for detached tests. It has the next predefined keywords:

    * ``detach = True``
    * ``timeout = 0`` (runs "forerver")

    .. versionadded:: 0.20110718
    """

    acro = "TestBG"
    forbid = ['detach', 'timeout']

    def __init__(self, cmd, **kargs):
        Test.__init__(self, cmd=cmd, **kargs)
        self.timeout = 0
        self.detach = True


class Command(Subprocess, Public):
    """It is a non-checked ``Test``. Command is exactly the same that a Test
    with a ``check=False`` keyword. That means its fail has not any consequence.

    The Commands (or any other non-checked Tasks) are not considered in results
    counting.
    """
    acro = 'Cmnd'

    def __init__(self, cmd, **kargs):
        Subprocess.__init__(self, cmd=cmd, **kargs)
        self.check = False


class CommandBG(Command):
    """Shortcut for detached commands. It has the next predefined keywords:

    * ``detach = True``
    * ``expected = -15`` (SIGTERM)
    * ``timeout = 0`` (runs "forerver")
    * ``check = False``

    .. versionadded:: 0.20110718
    """

    acro = 'CmndBG'
    forbid = ['detach', 'timeout']

    def __init__(self, cmd, **kargs):
        Command.__init__(self, cmd, **kargs)
        self.check = False
        self.detach = True
        self.expected = -15
        self.timeout = 0


class Daemon(Command):
    """
    .. deprecated::  0.20110718
       Use :func:`CommandBG` instead.
    """

    acro = 'Daem'
    forbid = ['detach', 'timeout']

    def __init__(self, cmd, **kargs):
        Command.__init__(self, cmd, **kargs)
        self.timeout = 0
        self.detach = True
        self.check = False


class TestFunc(Task, Public):
    """It checks the return value of an arbitrary Python function or method. A
    return value of 0 means success, otherwise is a error code. However, for unit
    testing, prefer `UnitTestCase`_ instead of TestFunc.
    """
    acro = 'Func'
    allows = ['expected', 'must_fail']
    concurrent = False

    def __init__(self, func, args=(), **kargs):
        assert callable(func)
        Task.__init__(self, **kargs)

        self.func = func
        self.args = args

        self.auto_desc = u"{0}({1})".format(
            func.__name__,
            str.join('', [str(x) for x in self.args]))

    def run(self):
        sys.stdout = self.outwrap
        sys.stderr = self.errwrap

        try:
            self.retcode = self.func(*self.args)

            if not isinstance(self.retcode, int):
                Log.error("Function '%s' should return an integer" % self.func.__name__)

            if self.retcode is None:
                return const.ERROR

            retval = self.retcode == self.expected

        except Exception, e:
            self.log.error(e)
            return const.ERROR

        finally:
            sys.stdout = sys.__stdout__
            sys.stderr = sys.__stderr__

        if self.must_fail and retval == const.FAIL:
            return const.OK

        return retval
