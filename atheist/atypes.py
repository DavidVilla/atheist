# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import copy


from pyarco.Type import checked_type, accept

from atheist.log import Log
from .const import NOEXEC
from .utils import pretty, remove_dups


class TypedList(list):
    def __init__(self, cls, *values):
        self._cls = cls
        for i in values:
            self += i

    def append(self, val):
        Log.error("Use operator += instead")

    def __iadd__(self, val):
        if isinstance(val, list) or isinstance(val, tuple):
            for i in val:
                self += i

            return self

        list.append(self, checked_type(self._cls, val))
        return self

    def copy(self):
        retval = TypedList(self._cls)
        retval.extend(self)
        return retval

    def prepend(self, val):
        self.insert(0, val)

    @classmethod
    def isA(cls, val, klass):
        "checks 'val' is a TypedList(cls)"
        assert isinstance(val, list), val
        for i in val:
            assert isinstance(i, klass)


class Condition(object):
    def __init__(self, *attrs):
        self._value = NOEXEC
        self._task = None
        self.auto = False

        self.identity_attrs = attrs[:]

    @property
    def value(self):
        return self._value

    def pre_task_run(self, task, condlist):
        """ Executed just BEFORE task execution """

        if self._task is not None and task is not self._task:
            Log.error("Conditions can not be shared: %s in %s, use clone()" % (self, self._task))
            sys.exit(1)

        self._value = NOEXEC
        self._task = task
        return []

    def run(self):
        raise NotImplemented

    def evaluate(self):
        self._value = self.run()
        assert self._value is not None
        return self._value

    def clone(self):
        retval = copy.copy(self)
        retval._value = NOEXEC
        retval._task = None
        return retval

    def __eq__(self, other):
        if self.__class__ is not other.__class__:
            return False

        retval = all(getattr(self, x) == getattr(other, x) for x in self.identity_attrs)
        return retval

    def __repr__(self):
        return str(self)

    def __str__(self):
        return unicode(self).encode('utf-8', 'replace')

    def __unicode__(self):
        return u"<%s %s %s>" % (pretty(self._value), self.name, self.repr_args())

    @property
    def name(self):
        return self.__class__.__name__

    def repr_args(self):
        return u''

    def check_sanity(self):
        assert self._task


class ConditionList(TypedList):
    @accept(name=str, values=Condition)
    def __init__(self, name='', *values):
        TypedList.__init__(self, Condition)
        for i in values:
            self += i

        self.name = name

    def __iadd__(self, val):
        if val in self:
            Log.debug("Trying to add a duplicate condition, ignored: %s", val)
            return self

        checked_type(Condition, val)
        return TypedList.__iadd__(self, val.clone())

    def __add__(self, other):
        retval = ConditionList(self.name)
        for i in self:
            retval += i

        for i in other:
            retval += i

        return retval

    def clone(self):
        return ConditionList(self.name, *self)

    def remove_dups(self):
        retval = ConditionList(self.name)
        for i in remove_dups(self):
            list.append(retval, i)
        return retval

    def replace(self, old, news):
        retval = ConditionList(name=self.name)
        for c in self:
            if c == old:
                retval.extend(news)
            else:
                retval += c

        return retval

    def __str__(self):
        retval = '<ConditionList \n['
        for x in self:
            retval += '%s:%s\n ' % (id(x), x)
        retval += ']>'
        return retval
