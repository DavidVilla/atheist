# -*- coding:utf-8; tab-width:4; mode:python -*-

import sys
import time
import logging
import optparse
from functools import partial
from operator import attrgetter

from pyarco.UI import ellipsis
import pyarco.Conio as Conio

from .log import Log
from .conf import settings
from utils import ASCII_TreeRender, UTF_TreeRender
from utils import  high, pretty, count, cout
import atheist.const as const

from atheist import CompositeTask
from .condition import ConditionDecorator


class Reporter:
    width = 80

    def __init__(self, dst, handler=None, plain=False):
        self.dst = dst
        assert isinstance(dst, str)
        self.logger = logging.getLogger(dst)
        self.logger.propagate = 0
        self.logger.setLevel(logging.INFO)
        self.logger.addHandler(handler or logging.StreamHandler())

        self.plain = settings.plain or plain

        if self.plain:
            self.tree = ASCII_TreeRender
            self.pretty = partial(pretty, plain=True)
            self.high = lambda x: x
        else:
            self.tree = UTF_TreeRender
            self.pretty = pretty
            self.high = high

    def add_case(self, case):
        "case by case basis reporting (usefull for non persistent files)"

    def do_render(self, suite):
        Log.debug("%s: sending report to '%s'" % (self.__class__.__name__, self.dst))
        report = self.render(suite)
        if report:
            self.logger.info(report.encode('UTF-8'))

#        print '--\n', report

    def render(self, suite):
        if settings.report_detail == 0:
            return ''

        retval = u''
        for case in sorted(suite, key=attrgetter('fname')):
            if settings.fails_only and case.result == const.OK:
                continue

            retval += self.build_case(case)

        retval += self.build_stats(suite)
        return retval

#    def join(self, lines):
#        return str.join('\n', lines)

    def build_case(self, case):
        retval = ''
        elapsed = "{0:6.2f}".format(case.elapsed)
        if case.elapsed > 1:
            elapsed = cout(Conio.RED, elapsed, Conio.NORM)

        fname = settings.relpath(case.fname)

        TESTCASE_FORMAT = '{result} TaskCase: {path:<50} {time}\n'
        retval += TESTCASE_FORMAT.format(
            id =     id(case),
            result = self.pretty(case.result),
            path   = fname[2:] if fname.startswith('./') else fname,
            time   = elapsed if settings.case_time else '',
#            para   = 'P' if case.concurrent else '',
            )

        if case.result in [const.OK, const.NOEXEC] and settings.report_detail < 2:
            return retval

        retval += self.tree.draw(case.tasks, func=self.build_task)
        return retval

    def build_task(self, task, conn, tree, level):
        retval = ''
        #FIXME: retcode y cmd son específicos de Subprocess y deberían especializarse

        if not hasattr(task, 'retcode'):
            retcode = 'na'
        elif task.retcode is None:
            retcode = '-'
        else:
            retcode = int(task.retcode)

        width = self.width - 31
        desc = ''
        desc_width = 0
        if task.desc:
            desc_width = width * 0.6
            try:
                desc = u"| {0}".format(task.desc.decode('utf8', 'replace'))
            except UnicodeDecodeError, e:
                desc = unicode(e)

        TASK_FORMAT = u"{result} {index:>3} {tree} {type:<9}{mode}({retcode:>2}:{expected:2})  {auto_desc} {desc}\n"
        retval += TASK_FORMAT.format(
            result   = self.pretty(task.result),
            tree     = tree,
            type     = task.acro[:6],
            index    = task.indx,
            mode     = task.get_mode(),
            retcode  = retcode,
            expected = task.expected,
            auto_desc = ellipsis(task.auto_desc, width - desc_width),
            desc      = ellipsis(desc, desc_width),
            )

        if (settings.report_detail > 2 or task.result != const.OK) and \
                isinstance(task, CompositeTask):
            retval += self.tree.draw(task.tasks, self.build_task, conn, level)

        if task.conditions_ok and (settings.report_detail < 4):
            return retval

        conditions = self.prepare_conditions(task)
        retval += self.tree.draw(conditions, self.build_condition, conn, level)
        return retval

    def prepare_conditions(self, task):
        retval = []
        for condlist in [task.pre, task._runnin, task.post]:
            for c in condlist:
                if settings.report_detail < 5 and \
                        (c.value == const.NOEXEC or (c.value == const.OK and c.auto)):
                    continue

                c.group_name = condlist.name
                retval.append(c)

        return retval

    # FIXME: el arbol se genera mal para "test/task-classes.test -veo"
    def build_condition(self, condition, conn, tree, level):
        retval = ''
        CONDITION_FORMAT = u"{value} {tree:>{indent}}{auto}{kind:<5} {name} {info}\n"
        retval += CONDITION_FORMAT.format(
                value  = self.pretty(condition.value),
                tree   = tree,
                indent = 4 + len(tree),
                auto   = '+' if condition.auto else ' ',
                kind   = '%s:' % condition.group_name,
                name   = condition.name,
                info   = ellipsis(condition.repr_args(),
                                  self.width - 21 - len(condition.name)),
                )

        if settings.report_detail < 5:
            return retval

        if isinstance(condition, ConditionDecorator) and len(condition.children) > 1:
            retval += self.tree.draw(condition.children, self.build_decorated_condition,
                                     conn, level)
        return retval

    def build_decorated_condition(self, condition, conn, tree, level):
        COMPOSED_CONDITION_FORMAT = u"{tree:>{indent}}{auto}{name} {value} {info}\n"
        retval = COMPOSED_CONDITION_FORMAT.format(
                value  = self.pretty(condition.value, plain=True),
                tree   = tree,
                indent = 11 + len(tree),
                auto   = '+' if condition.auto else ' ',
                name   = condition.name,
                info   = ellipsis(condition.repr_args(),
                                  self.width - 21 - len(condition.name)),
                )

        if isinstance(condition, ConditionDecorator):
            retval += self.tree.draw(condition.children, self.build_decorated_condition,
                                     conn, level)

        return retval

    def build_stats(self, suite):
        delta = time.time() - settings.init_time
        if delta > 60:
            t = time.strftime("%H:%M:%S", time.gmtime(delta))
        else:
            t = "%.2fs" % delta

        if suite.all_ok():
            return "{0} - {1} - {2} - {3} \n".format(
                self.pretty(const.FINAL_OK),
                t,
                count(suite.total, 'test'),
                count(suite.num_tasks, 'task'))

        return "{0} - {1} - {2}/{3}\n".format(
            self.pretty(const.FINAL_FAIL),
            t,
            self.high(suite.ok),
            count(suite.total, 'test'))


class FailReporter(Reporter):
    '''A reporter to use only on fails'''

    @classmethod
    def add_arguments(cls, parser):
        if parser.has_option('--notify-ok'):
            return

        parser.plugin_group.add_argument(
            '--notify-ok', action='store_true', default=False,
            help='Send notifications even when all is right')

    def do_render(self, suite):
        if not suite.all_ok() or settings.notify_ok:
            Reporter.do_render(self, suite)

    def render(self, suite):
        retval = "Atheist: %s %s@%s:/%s %s\n" % (
            self.pretty(suite.all_ok()),
            settings.user,
            settings.host,
            settings.base_dir,
            settings.args,
            )

        if not suite.all_ok():
            retval += "SOME TASKS FAILED!\n"

        retval += '''
user: %s
host: %s
date: %s
argv: %s
base: %s
\n''' % (settings.user,
         settings.host,
         time.asctime(),
         str.join(' ', sys.argv),
         settings.base_dir)

        retval += Reporter.render(self, suite)
        return retval


class ConsoleReporter(Reporter):
    def __init__(self, dst='console'):
        Reporter.__init__(self, dst)
        self.width = settings.cols
