# -*- mode: python; coding: utf-8 -*-

# atheist
#
# Copyright (C) 2009,2010,2011 David Villa Alises
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


__all__ = []

import os
import sys
import string

import glob
import logging
import time
import signal
import traceback
import codecs
from itertools import chain
from inspect import getmembers
import threading
import types
import random

from pyarco.Type import Record, merge_uniq, checked_type, Undefined
from pyarco.Thread import ThreadFunc, SimpleThreadPool
from pyarco.UI import ellipsis, ProgressBar
#from pyarco.Conio import *

from .log import Log, CapitalLoggingFormatter
from .conf import settings
from .iface import Public
from .atypes import TypedList, ConditionList
from .utils import print_exc, high, remove_dups, FileWrapper, process_exists, assure_unicode, pretty
import atheist.condition as condition
import atheist.const as const


class ConfigureError(Exception):
    pass


def assure_tmp_dir():
    if not os.path.exists(const.ATHEIST_TMP):
        os.makedirs(const.ATHEIST_TMP)
        return

#    for fname in os.listdir(atheist.const.ATHEIST_TMP):
#        os.remove(os.path.join(atheist.const.ATHEIST_TMP, fname))


def assure_list(value, cls=None):
    if isinstance(value, list):
        if cls is not None:
            for x in value:
                checked_type(cls, x)
        return value

    return [value]


def file_template():
    # FIXME: Construir a partir de KeyWords'
    return '''\
#!/usr/bin/atheist
# -*- mode:python; coding:utf-8 -*-

# Test(desc = 'test template',
#      pre  =   ,
#      cmd  = '',
#      post =   ,

#      cwd         = '.',
#      delay       = 0,
#      detach      = False,
#      env         = {},
#      expected    = 0,
#      must_fail   = False,
#      path        = '.',
#      template    = [],
#      timeout     = 5,
#      save_stderr = False,
#      save_stdout = False,
#      signal      = signal.SIGTERM)
'''


def create_new_test():
    filename = settings.create

    if not filename.endswith('.test'):
        filename += '.test'

    if os.path.exists(filename):
        Log.error("Test file '{0}' already exists".format(filename))
        return

    with file(filename, 'w') as fd:
        fd.write(file_template())

    Log.warning("Template for '{0}' created.".format(filename))

    if 'EDITOR' not in os.environ:
        Log.error('EDITOR shell variable is not defined')
        return

    os.system('{0} {1}'.format(os.environ['EDITOR'], filename))


class task_mode:
    MAIN, \
    SETUP, TEARDOWN, \
    DIR_SETUP, DIR_TEARDOWN = ['-', 's', 't', 'S', 'T']


class ExecutionError(Exception):
    def __str__(self):
        return '<{0}>'.format(self.__class__.__name__)


class AbortExc(ExecutionError):
    pass


class FileExecutor:
    pyunit_loaded = False

    @classmethod
    def error(cls, fname):
        print_exc(traceback.format_exc(), Log)
        Log.error("Errors in the testcase definition '%s'" % fname)
        raise ExecutionError()

    @classmethod
    def unittest_wrapper(cls, name):
        from atheist.plugins.python import UnitTestCase

        module = __import__(name)
        module.main = UnitTestCase
        return module

    @classmethod
    def prepare_pyunit(cls):
        if cls.pyunit_loaded:
            return

        sys.modules['unittest'] = cls.unittest_wrapper('unittest')

        try:
            sys.modules['unittest2'] = cls.unittest_wrapper('unittest2')
        except ImportError:
            Log.debug('unittest2 not available')

        cls.pyunit_loaded = True

    @classmethod
    def run(cls, fname, _global=None, _local=None, template={}):
        try:
            with file(fname) as fd:
                script = string.Template(fd.read()).safe_substitute(template)

            if 'unittest' in script:
                cls.prepare_pyunit()

            code = compile(script, fname, 'exec')
            exec(code) in _global, _local  # EXEC TEST FILE

        except Exception:
            cls.error(fname)


# FIXME: representation should be delegated to a class that may be specialized
def task_view(task):
    return u"%s: %s(%s)" % \
        (high(task.name), task.__class__.__name__, ellipsis(task.repr_args()))


class FactoryMeta(type):

    def __call__(cls, *args, **kargs):
        cls.create(*args, **kargs)


class TaskFactory(object):
    allows = []
    forbid = []

    __metaclass__ = FactoryMeta

    @classmethod
    def create(cls, mng, *args, **kargs):
        raise NotImplementedError


class Task(Record):

    acro = 'Task'
    allows = ['cwd', 'dirty', 'flags', 'must_fail', 'save_stdout', 'save_stderr', 'stderr', 'stdout']
    forbid = []
    concurrent = True

    _mng = None
    _case = None

    __creation_counter = 0

    @classmethod
    def get_counter(cls):
        Task.__creation_counter += 1
        return Task.__creation_counter

    def __init__(self, **kargs):
        self.mng = Task._mng
        self._indx = Task.get_counter()
        current_ids = [x.tid for x in Task._case.tasks]    # Only this testcase

        assert 'tid' not in kargs.keys() or kargs['tid'] not in current_ids, \
            "Duplicate task id: '%s'" % kargs['tid']

        if 'parent' in kargs:
            self._case = kargs['parent']

        self._case.tasks.append(self)
        self.kargs = kargs

        self.__dict__.update(KeyWords.defaults())

        self.stdout = Undefined(self, 'stdout')
        self.stderr = Undefined(self, 'stderr')

        if 'template' in kargs:
            for i in checked_type(list, kargs['template']):
                self.__dict__.update(KeyWords.validate(self, i))

        self.__dict__.update(KeyWords.validate(self, kargs))

        # keyword post-processing
        self.timeout = int(self.timeout * settings.timeout_scale)

        if 'flags' in kargs:
            self.__dict__.update(KeyWords.validate(self, KeyWords.flags_to_dict(kargs['flags'])))

        self.auto_desc = ''
        self.fname = ''

        self.gen = TypedList(str, *assure_list(self.gen))

        self.pre = ConditionList('pre', *assure_list(self.pre))
        self.post = ConditionList('post', *assure_list(self.post))

        self._runnin = ConditionList('run')

        self._mode = None  # normal, setup, teardown
        self.result = const.NOEXEC
        self.conditions_ok = True

        self.thread = None
        self.time_start = None
        self.time_end = None

        self.stderr_fd = None
        self.stdout_fd = None
        self.last_task_out = None

        self.fs = settings.fs

        self.save_stdout |= bool(self.stdout) or settings.save_stdout
        self.save_stderr |= bool(self.stderr) or settings.save_stderr

        self.enable_outs(stdout=self.save_stdout,
                         stderr=self.save_stderr)

        self.wrap_outs = False

        self.setup_logger()

    def setup_logger(self):
        self.log = logging.getLogger(self.name)
        if self.log.handlers:
            return  # logger is already setup

        loglevel = settings.loglevel if self.loglevel is None else self.loglevel

        self.log.setLevel(loglevel)
        self.log.propagate = 0

        if settings.console_log:
            term = logging.StreamHandler()
            term.setFormatter(CapitalLoggingFormatter(
                    settings.timetag_fmt + '[%(levelcapital)s] %(name)s: %(message)s'))
            term.setLevel(loglevel)
            self.log.addHandler(term)

        if settings.save_logs:
            self.filelog_name = os.path.join(const.ATHEIST_TMP, '%s_log' % (self.name))
            self.filelog = logging.FileHandler(self.filelog_name)
            self.filelog.setLevel(settings.loglevel)
            self.log.addHandler(self.filelog)
            self._case.generated.append(self.filelog_name)

    def get_mode(self):
        return self._mode

    def set_mode(self, mode):
        self._mode = mode

    def enable_outs(self, stdout=False, stderr=False):

        def enable_out(task, name):
            assert name in ['stdout', 'stderr'], "Internal Error"

            out = getattr(task, name)
            if not out:
                out = os.path.join(const.ATHEIST_TMP, '%s_%s' % (task.name, name[-3:]))
                setattr(task, name, out)

            setattr(task, "save_%s" % name, True)
            task.gen += out

        if stdout:
            enable_out(self, 'stdout')
        if stderr:
            enable_out(self, 'stderr')

    def eval_conditions(self, condition_set, do_log=True):
        for c in condition_set:
            value = c.evaluate()
            if do_log:
                self.log.debug(u"{0:<5} {1}".format(condition_set.name + ':', c))

            if value == const.OK:
                continue

            if not do_log:
                self.log.debug(u"{0:<5} {1}".format(condition_set.name + ':', c))

            self.conditions_ok = False
            self.result = const.FAIL
            if not settings.keep_going:
                break

        return self.conditions_ok

    # FIXME: Debería dejar la Task en el estado inicial, necesario para --until-failure
    def do_pre_run(self):
        self.log.debug('pre-run')
        self.sanity_check()
        self.configure()
        self.pre_run()
        self.eval_conditions(self.pre)

    def sanity_check(self):
        if self.dirty and not self.gen:
            self.log.warning("'dirty' without 'gen' has no sense.")

        for name in ['gen', 'pre', '_runnin', 'post']:
            value = getattr(self, name)
            if not isinstance(value, TypedList):
                self.log.critical("Task attribute assignation is forbidden:\n"
                                  "%s: %s = %s", self.fname, name, value)
                sys.exit(1)

    def configure(self):
        '''Configure the task, reading config file or similar'''
        pass

    def pre_run(self):
        def extract_aditional_conditions(condlist):
            retval = ConditionList(condlist.name)
            for c in condlist:
                conditions = c.pre_task_run(self, condlist)
                assert conditions is not None, c
                retval.extend(conditions + [c])
            return retval

        self.pre = extract_aditional_conditions(self.pre)
        self.pre = self.pre.remove_dups()

        self.post = extract_aditional_conditions(self.post)
        self.post = self.post.remove_dups()

        self.gen = [os.path.abspath(x) for x in self.gen]
        self.gen = remove_dups(self.gen)

        for i in self.gen:
            self.post[0:0] = condition.FileExists.create(i, self, self.post)
            self.pre[0:0]  = condition.FileExists.create_negate(i, self, self.pre)

        for condlist in [self.pre, self.post]:
            for cond in condlist:
                cond.check_sanity()

    @property
    def indx(self):
        return self._indx

    @property
    def name(self):
        return self.acro[0] + str(self.indx)

    def describe(self, pre_keys=[]):
        self.pre_run()

        dic = self.__dict__.copy()
        dic['fname'] = settings.relpath(dic['fname'])

        keys = pre_keys[:] + ['fname']
        if self.result is not const.NOEXEC:
            keys.append('result')

        # add standard attrs with no-default value
        for k, v in KeyWords.items():
            if k in KeyWords.multivalued() + ['flags']:
                continue

            if getattr(self, k) != v.default:
                keys.append(k)

        attrs = []
        for k in keys:
            value = dic[k]
            if isinstance(value, Undefined):
                continue

            value = assure_unicode(value)
            value = u"'%s'" % value.replace('\n', '\n' + ' ' * 16)
            attrs.append(u"{0}: {1}".format(k.rjust(14), value))

        # multivalued attributes
        for a in KeyWords.multivalued():
            for i in getattr(self, a):
                attrs.append(u"{0:>14}: {1}".format(a, i))

        retval = u"%3s:%s\n%s\n" % (self.indx, self.__class__.__name__,
                                    str.join('\n', attrs))

#        return retval.encode('utf-8')
        return retval

    def __repr__(self):
        return str(self)

    def __str__(self):
        return unicode(self).encode('utf-8', 'replace')

    def __unicode__(self):
        return u"%s: <%s(%s)>" % \
            (self.name, self.__class__.__name__, self.repr_args())

    def repr_args(self):
        return u''

    def clone(self, *args):
        kargs = self.kargs
        kargs['desc'] = '(copy of %s) %s' % (self.name, kargs['desc'])

        retval = self.__class__(*args, **kargs)

        for c in self.pre:
            retval.pre += c
        for c in self.post:
            retval.post += c
        retval.gen = self.gen.copy()
        return retval

    def do_run(self):
        time.sleep(self.delay)

        if self.stdout:
            self.stdout_fd = codecs.getwriter('utf-8')(open(self.stdout, 'w', 0))

        if self.stderr:
            self.stderr_fd = codecs.getwriter('utf-8')(open(self.stderr, 'w', 0))

        self.outwrap = FileWrapper(settings.stdout,
                                   self.stdout_fd,
                                   tag='%s:out' % self.name,
                                   only_on_fail=settings.out_on_fail)

        self.errwrap = FileWrapper(settings.stderr,
                                   self.stderr_fd,
                                   tag='%s:err' % self.name,
                                   only_on_fail=settings.out_on_fail)

        if self.wrap_outs:
            sys.stdout = self.outwrap
            sys.stderr = self.errwrap

        self.tini = time.time()
        self.result = self.run()

        assert self.result is not None, "%s." % self.__class__.__name__

        sys.stdout = sys.__stdout__
        sys.stderr = sys.__stderr__

        if self.result == const.FAIL and not self.check:
            self.result = const.SOFT_FAIL

        if self.result not in [const.OK, const.SOFT_FAIL]:
            self.outwrap.print_fail_out()
            self.errwrap.print_fail_out()

        self.outwrap = None
        self.errwrap = None

        if self.stdout_fd:
            self.stdout_fd.close()

        if self.stderr_fd:
            self.stderr_fd.close()

        self.close()

    # This is status-based. Provide a better approach in the subclass if is possible.
    def is_running(self):
        return self.result == const.UNKNOWN

    # This is status-based. Provide a better approach in the subclass if is possible.
    def is_finished(self):
        return self.result in [const.FAIL, const.OK, const.ERROR,
                               const.TODO, const.SOFT_FAIL, const.MISCONF]

    def run(self):
        raise NotImplementedError(self)

    def terminate(self):
        raise NotImplementedError(self)

    def close(self):
        '''Close file descriptors: stdin, stdout, etc.'''


class CompositeTask(Task, Public):
    acro = 'Comp'
    allows = ['detach', 'parent']

    """ The CompositeTask let you specify a predicate to evalue a set of tasks:

    CompositeTask(all, t1, t2, t3) is OK if all the tasks are OK.

    #--

    def at_least_two(tasks):
        return [bool(x) for x in tasks].count(True) >= 2

    CompositeTask(at_least_two, t1, t2, t3) is OK when 2 or more tasks are OK.
    """

    def __init__(self, oper, *tasks, **kargs):
        self.oper = oper
        self.tasks = TypedList(Task, tasks)
        for t in self.tasks:
            assert t.detach == False, 'CompositeTask children must be not detached'

        Task.__init__(self, **kargs)

        for t in tasks:
            t.parent = self
            t._case.tasks.remove(t)

        self.auto_desc = u"{0} {1}".format(oper.__name__,
                                           [x.name for x in tasks])

    def set_mode(self, mode):
        self._mode = mode
        for t in self.tasks:
            t.set_mode(mode)

    def run(self):
        self.gen = merge_uniq(*[t.gen for t in self.tasks])

        for t in self.tasks:
            Log.info(t)
            run_task(t)

        retval = self.oper(t.result == const.OK for t in self.tasks if t.check)
        self.log.debug('%s finish' % pretty(retval))
        return retval

    def terminate(self):
        self.log.debug('Terminating child tasks')
        for t in self.tasks:
            t.terminate()

    def is_running(self):
        return any(x.is_running() for x in self.tasks)

    def repr_args(self):
        return str([x.name for x in self.tasks])


class KeyWords(object):

    check       = Record(type_=bool, default=True,
                         desc="If 'check' is ``False`` there is no consequences when the Task "
                         "fails and it is not considered in the final test stats")
    cwd         = Record(type_=str,  default=None,
                         desc="Directory for the task command execution")
    delay       = Record(type_=(int, float), default=0,
                         desc="Wait for 'delay' seconds before launching task actions")
    desc        = Record(type_=str,  default='',
                         desc="One-liner textual task description")
    detach      = Record(type_=bool, default=False,
                         desc="When ``detach`` is ``False`` the next task does not start until "
                         "the current one ends. When ``detach`` is ``True`` the task is backgrounded and "
                         "the next task starts immediately")
    dirty       = Record(type_=bool, default=False,
                         desc="Do not remove generated files by this task")
    env         = Record(type_=dict, default=os.environ.copy(),
                         desc=" A dictionary with shell environment variables")
    expected    = Record(type_=int,  default=0,
                         desc="Expected return code for the command. If it uses a shell, the value may "
                         "be negative if the process is killed by a signal")
    flags       = Record(type_=int,  default=0,
                         desc="A set of boolean keyword flags that you want to set")
    gen         = Record(type_=(str, list), default=[],
                         desc="A name (or list) of task generated files")
    loglevel    = Record(type_=int,  default=None,
                         desc="Override the loglevel for this task")
    misconfig   = Record(type_=bool, default=False,
                         desc="If the task raises a ConfigurationError during setup, it is considered "
                         "as a soft fail")
    must_fail   = Record(type_=bool, default=False,
                         desc="The task is expected to fail (negative test)")
    parent      = Record(type_=CompositeTask, default=None,
                         desc="A parent CompositeTask to aggregate this one")
    path        = Record(type_=str,  default='',
                         desc="A unix-like directory list to add to PATH env variable")
    post        = Record(type_=(condition.Condition, list), default=[],
                         desc="A post condition (or list of them)")
    pre         = Record(type_=(condition.Condition, list), default=[],
                         desc="A pre condition (or list of them)")
    save_stderr = Record(type_=bool, default=False,
                         desc="Save the process standard error output")
    save_stdout = Record(type_=bool, default=False,
                         desc="Save the process standard output")
    shell       = Record(type_=bool, default=False,
                         desc="Execute the task command inside a shell. It is required to understand "
                         "pipes, redirection and other shell stuff")
    signal      = Record(type_=int,  default=signal.SIGTERM,
                         desc="Send that signal to kill the task process")
    stderr      = Record(type_=str,  default=None,
                         desc="The filename to save task command standard error output")
    stdout      = Record(type_=str,  default=None,
                         desc="The filename to save task command standard output")
    template    = Record(type_=list, default=[],
                         desc="A list of TaskTemplate's. See :ref:`templates`")
    timeout     = Record(type_=int,  default=5,
                         desc="The maximum execution time (in seconds) for the process task. If timeout "
                         "expires, the task fails. `timeout=0` means the task may run forever")
    tid         = Record(type_=str,  default=None,
                         desc="A task user identifier")
    todo        = Record(type_=bool, default=False,
                         desc="It indicates that the task is not fully verified and it is possible that "
                         "it fails unduly. This has no effect when the task ends successfully")

    __defaults = {}

    @classmethod
    def items(cls):
        return iter(getmembers(cls, lambda x: isinstance(x, Record)))

    @classmethod
    def multivalued(cls):
        return ['pre', 'post', 'gen']

    @classmethod
    def flag_enum(cls):
        retval = {}
        for n, word in enumerate([x for x in cls.items() if x[1].default == False]):
            retval[word[0]] = 2 ** n

        return retval

    @classmethod
    def flags_to_dict(cls, flags):
        retval = {}
        for key, val in cls.flag_enum().items():
            if flags & val:
                retval[key] = True

        return retval

    @staticmethod
    def validate(cls, _dict):
        if not isinstance(cls, type):
            cls = cls.__class__

        allways_allowed = ['delay', 'desc', 'gen', 'loglevel', 'pre', 'post', 'template', 'tid', 'todo']
        redundant = set(cls.allows).intersection(allways_allowed)
        assert not redundant, \
            "{0} can not be in '{1}.allows' specification".format(list(redundant), cls.__name__)

        allowed = set(allways_allowed).union(cls.allows).difference(cls.forbid)

        for k, v in _dict.items():
            assert hasattr(KeyWords, k), "'%s' is not a valid keyword" % k
            assert k in allowed, "'%s' is not an allowed keyword for '%s'" % (k, cls.__name__)

            checked_type(getattr(KeyWords, k).type_, v)

    #        if v == task_attrs[k].default:
    #            Log.info("Default value for '%s' is already '%s'. Remove it." % (k, v))

        return _dict

    @classmethod
    def defaults(cls):
        if not cls.__defaults:
            for k, v in KeyWords.items():
                cls.__defaults[k] = v.default

        return cls.__defaults


class Template(dict, Public):
    def __init__(self, **kargs):
        super(Template, self).__init__(**kargs)


def run_task(task, recurse=False, end_callback=lambda: None):

    if task.detach and recurse == False:
        task.log.debug("detaching")
        task.thread = ThreadFunc(run_task, task,
                                 recurse=True, end_callback=end_callback)

        tini = time.time()
        try:
            time.sleep(0.1)

            while 1:
                if task.is_finished() or task.is_running():
                    break

                time.sleep(0.2)

                if time.time() - tini > 10:
                    Log.critical("'%s' does not start", task)
                    break

        except NotImplementedError:
            Log.critical("'%s' is_running does not exists", task)

        return None

    try:
        task.result = const.UNKNOWN

        if task.pre or task.post:
            task.result = const.OK  # preliminary result

        task.do_pre_run()

        if task.conditions_ok:
            task.do_run()

        if task.result == const.OK:
            task.eval_conditions(task.post)

    except ConfigureError, e:
        task.log.error(e)
        if task.misconfig:
            task.check = False
            task.result = const.MISCONF
        else:
            task.result = const.ERROR

    if task.result in [const.FAIL, const.ERROR] and task.todo:
        task.check = False
        task.result = const.TODO

    elif task.result == const.FAIL and not task.check:
        task.result = const.SOFT_FAIL

    end_callback()
    return task.result


class Interpolation(object):
    @classmethod
    def populate(self, case):
        retval = settings.interpolation.copy()  # [vars] section in config file
        retval.update({
            'basedir':     settings.relpath(),
            'fullbasedir': settings.relpath.root,
            'testdir':     settings.relpath(case.testdir),
            'fulltestdir': case.testdir,
            'dirname':     " ERROR: ['$dirname' is deprecated: use '$testdir' instead] ",
            'fname':       case.name,
            'testname':    os.path.basename(case.name),
            'atheist':     './athcmd.py --plain -v',
            'pid':         settings.pid,
            'tmp_base':    const.ATHEIST_TMP_BASE,
            'tmp':         const.ATHEIST_TMP,
            'hostname':    settings.host})
        return retval


class TaskCase(object):
    def __init__(self, mng, fname, template={}, mode=task_mode.MAIN):
        self.mng = mng
        self.fname = fname
        self.template = template
        self.generated = []
        self.mode = mode

        self.name = os.path.splitext(settings.relpath(fname))[0]
        self.testdir = os.path.dirname(fname)

        self.elapsed = 0

        self.template.update(Interpolation.populate(self))

    def load(self):
        self.tasks = []
        Task._case = self

        if not settings.skip_hooks and self.mode == task_mode.MAIN:
            setup = os.path.join(self.testdir, const.SETUP)
            if os.path.exists(setup):
                self.process_file(setup, task_mode.SETUP)

        self.process_file(self.fname, self.mode)

        if not settings.skip_hooks  and self.mode == task_mode.MAIN:
            teardown = os.path.join(self.testdir, const.TEARDOWN)
            if os.path.exists(teardown):
                self.process_file(teardown, task_mode.TEARDOWN)

        self.result = const.NOEXEC

        self.concurrent = all(x.concurrent for x in self.tasks)

    def __repr__(self):
        return "<TaskCase {fname}:{result}>".format(**self.__dict__)

    def process_file(self, fname, mode):
        Log.debug("loading %s" % settings.relpath(fname))

        before = self.tasks[:]
        env = self.mng.exec_env.copy()
        env['load'] = self.load_module
        env['__file__'] = os.path.abspath(fname)
#        env['PYTHONPATH'] = os.path.dirname(fname)

        FileExecutor.run(fname, env, None, self.template)

        for t in set(self.tasks) - set(before):
            t.fname = fname
            t.set_mode(mode)

    def calculate_result(self):
        checked = [x.result == const.OK for x in self.tasks if x.check]
        if checked:
            self.result = all(checked)
            return

        nochecked = [x.result == const.OK for x in self.tasks]
        if all(nochecked):
            self.result = const.OK
        else:
            self.result = const.SOFT_FAIL

        if any(x.result == const.MISCONF for x in self.tasks):
            self.result = const.MISCONF

        if any(x.result == const.TODO for x in self.tasks):
            self.result = const.TODO

    def get_generated(self):
        retval = self.generated

        for t in self.tasks:
            if t.result == const.NOEXEC:
                continue

            if not settings.dirty and t.dirty:
                t.log.warning("dirty-task, not removing gen: %s" % t.gen)
            else:
                retval.extend(t.gen)

        return retval

    def load_module(self, fname):
        env = self.mng.exec_env.copy()
        before_keys = env.keys()

        FileExecutor.run(fname, env, None, self.template)

        # better imp.new_module?
        retval = types.ModuleType(fname)
        for key, val in env.items():
            if key not in before_keys:
                setattr(retval, key, val)

        return retval


class CaseResult:
    def __init__(self, identifier):
        self.fname = identifier
        self.result = const.NOEXEC
        self.elapsed = 0
        self.tasks = {}

    def __repr__(self):
        return '<CaseResult {0}: {1}>'.format(
            self.fname,
            str.join(',', [x.name for x in self.tasks]),
            )

#class TaskResult(Record): pass


class CaseExecutor:
    def __init__(self, case, observer=None):
        self.case = case
        self.observer = observer
        self.mng = case.mng

    # template-method
    def run(self):
        tini = time.time()
        Log.info("Test case {0:-<{1}}".format(
                settings.relpath(self.case.fname) + ' ',
                max(80, settings.cols - 15)))

        if not self.case.tasks:
            return

        self.run_tasks()
        self.wait_detached()
        self.kill_daemons()
        self.case.calculate_result()
        settings.reporters.report_case(self.case)
        self.mng.clean_async(
            os.path.join(const.ATHEIST_TMP, os.path.basename(self.case.name)),
            self.case.get_generated())

        self.case.elapsed = time.time() - tini


#    def async_run(self):
#        retval = CaseResult(
#            identifier = self.case.fname)
#
#        self.run()
#
#        for task in self.case.tasks:
#            retval.tasks[task.name] = TaskResult(
#                result = task.result
#                )
#
#        return retval

    def run_tasks(self):
        for t in self.case.tasks:
            if self.mng.aborted:
                break

            Log.info(task_view(t))
            result = run_task(t, end_callback=self.observer)

            if result in [const.FAIL, const.ERROR] and t.check \
                    and not settings.keep_going:
                t.log.info("FAIL, skipping remaining tasks (keep-going is off)")
                break

    def wait_detached(self):
        while 1:
            unfinished = [x for x in self.case.tasks if x.thread \
                              and x.thread.isAlive() \
                              and x.timeout != 0]

            if not unfinished:
                break

            for t in unfinished:
                t.log.debug("-- waiting to finish detached task")

                if self.mng.aborted:
                    t.terminate()

                t.thread.join(0.5)

    def kill_daemons(self):
        while 1:
            daemons = [x for x in self.case.tasks if x.thread \
                           and x.thread.isAlive() \
                           and x.timeout == 0]

            if not daemons or self.mng.aborted:
                break

            for t in daemons:
                t.log.debug("-- terminating daemon")

                t.terminate()
                t.thread.join(0.5)


def load_dirty_filelist(dirname):
    retval = []

    for fname in glob.glob(dirname + '/*.gen'):
        Log.debug("reading generated files from '%s'" % fname)

        try:
            with file(fname) as fd:
                retval.extend([x.strip('\n') for x in fd.readlines()])
        except IOError, e:
            Log.error(e)
            continue

#    print "generated-logs", dirname, retval
    return retval


def get_generated_from_finished_atheists():
    if not os.path.exists(const.ATHEIST_TMP_BASE):
        return []

    retval = []
    for fname in os.listdir(const.ATHEIST_TMP_BASE):
        fpath = os.path.join(const.ATHEIST_TMP_BASE, fname)
        if not os.path.isdir(fpath):
            continue

        # do not remove running atheist tmp dirs
        if fname.isdigit() and process_exists(int(fname)):
            continue

        retval.extend(load_dirty_filelist(fpath))

        for f in [fpath] + [os.path.join(fpath, x) for x in os.listdir(fpath)]:
            if f not in retval:
                retval.append(f)

    return retval


def save_dirty_filelist(fname, filelist):
    if not filelist:
        return

    filelist = remove_dups([fname] + filelist)

    gen_fd = open(fname, 'w')
    Log.debug("Storing generated files in %s:\n%s" % (fname, filelist))

    for fname in filelist:
        Log.warning("dirty-mode: not removing '%s'" % fname)
        gen_fd.write(fname + '\n')

    gen_fd.flush()
    gen_fd.close()


def remove_files_and_dirs(paths):

    def cmp(f1, f2):
        f1dir = os.path.isdir(f1)
        f2dir = os.path.isdir(f2)
        if f1dir == f2dir:
            return 0
        if f1dir:
            return 1

        return -1

    paths.sort(cmp=cmp)

    for f in paths:
        f = os.path.abspath(f)
        if not (f.startswith('/tmp') or f.startswith(settings.relpath.root)):
            Log.warning('Removing files out of /tmp or %s is forbbiden: %s',
                        settings.relpath.root, f)
            continue

        if os.path.isdir(f):
            Log.debug("- removing directory '%s'" % settings.relpath(f))
            try:
                os.rmdir(f)
            except OSError, e:
                Log.warning(e)

        elif os.path.isfile(f):
            Log.debug("- removing file '%s'" % settings.relpath(f))
            try:
                os.remove(f)
            except OSError, e:
                Log.warning(e)
        else:
            Log.info("- removing file '%s': does not exist!" % settings.relpath(f))


class Suite(list):
    def __init__(self):
#        self._task_index = 0

        self.total = 0
        self.ok = 0
        self.failures = 0
        self.errors = 0
        self.skipped = 0

#    def append(self, taskcase):
#        tests = [x for x in taskcase.tasks if x.check]
#        self.total += len(tests)
#        self.ok     += len([x for x in tests if x.result == OK])
#        self.failures   += self.total - self.ok  # puede que no hayan sido ejecutados
##        Log.info("*** %s fail: %s", taskcase, self.failures)
#        list.append(self, taskcase)

    def calculate(self):
        tests = [x for x in self.itertasks() if x.check]
#        print tests
        self.total = len(tests)
        self.ok     = len([x for x in tests if x.result == const.OK])
        self.errors = len([x for x in tests if x.result == const.ERROR])
        self.failures = self.total - self.ok  # puede que no hayan sido ejecutados
        self.skipped  = len([x for x in tests if x.result is [const.NOEXEC]])

#    def next(self):
#        self._task_index += 1
#        return self._task_index

    def itertasks(self):
        return chain(*[x.tasks for x in self])

    @property
    def num_tasks(self):
        return sum([len(s.tasks) for s in self])

    def all_ok(self):
        return self.ok == self.total

    def copy(self):
        retval = Suite()
        for case in self:
            retval.append(case)

        return retval


class Runner(object):
    def __init__(self, mng):
        Log.info("Using runner: %s", self)
        self.mng = mng
        self.setup_done = False
        self.suite = []

        if settings.random is not None:
            if self.cfg.random == 0:
                random.seed()  # random seed
            else:
                random.seed(settings.random)

    def setup(self):
        if self.setup_done:
            return

        self.setup_done = True

        self.mng.reload()
        self.suite = self.mng.suite

        if settings.random is not None:
            random.shuffle(self.suite)

        self.setup_progress_bar()

    def setup_progress_bar(self):
        self.pb = ProgressBar(
            self.mng.suite.num_tasks,
            width=max(47, settings.cols),
            label='Run ',
            disable = any([settings.disable_bar,
                           settings.verbose,
                           settings.quiet,
                           settings.stdout,
                           settings.stderr]))

        self.pb.listen_logger(Log, settings.loglevel)

    def run(self):
        self.run_suite()
        self.report()
        return self.suite.failures

    def run_suite(self):
        self.setup()
        self.pb.reset()
        for case in self.suite:
            executor = CaseExecutor(case, observer=self.pb.inc)
            executor.run()
            if not settings.keep_going and case.result in [const.FAIL, const.ERROR]:
                break

        self.pb.clean()
        self.suite.calculate()
        return self.suite

    def report(self, suite=None):
        if suite is None:
            suite = self.suite

        for r in settings.reporters:
            r.do_render(suite)

#    def clean(self):
#        generated = []
#        for case in self.suite:
#            generated.extend(case.get_generated())
#
#        generated.append(ATHEIST_TMP)
#
#        if not settings.dirty:
#            remove_files_and_dirs(generated)
#        else:
#            save_dirty_filelist(
#                os.path.join(ATHEIST_TMP, 'dirty.list'),
#                generated)
#


#    def async_run_suite(self):
#        self.setup()
#        results = {}
#        for case in self.suite:
#            executor = CaseExecutor(case, observer=self.pb.inc)
#            results[case.fname] = executor.run()
#        self.aync_apply_results(results)
#        self.suite.calculate()
#        return self.suite
#
#    def async_apply_results(self, results):
#        for case in self.suite:
#            result_case = results[case.fname]
#            case.result = result_case.result
#            for task in case:
#                result_task = resul_case[case.name]
#                task.result = result_task.result

    def __repr__(self):
        return "<{0}>".format(self.__class__.__name__)


class Counter:
    def __init__(self):
        self.n = 0
        self.lock = threading.Lock()

    def inc(self, *args):
        with self.lock:
            self.n += 1

    @property
    def value(self):
        return self.n


class ThreadPoolRunner(Runner):
    def run(self):

        def run_concurrent_suite(suite):
            done = Counter()

            pool = SimpleThreadPool(nworkers)

            for case in suite:
                executor = CaseExecutor(case, self.pb.inc)
                pool.add(executor.run, [], callback=done.inc)

            while not self.mng.aborted:
                time.sleep(0.5)
                if done.value == len(suite):
                    break

            pool.join()
#            pool.join(True, True)

        self.setup()
        nworkers = settings.workers

        if len(self.suite) == 1 or nworkers == 1:
            return Runner.run(self)

        if nworkers == 0:
            nworkers = min(100, 1 + len(self.suite) / 2)
            Log.info("Creating %s workers" % nworkers)

        concurrent_suite = [x for x in self.suite if x.concurrent]

        self.pb.reset()
        run_concurrent_suite(concurrent_suite)

        not_concurrent_suite = [x for x in self.suite if not x.concurrent]
        for case in not_concurrent_suite:
            executor = CaseExecutor(case, self.pb.inc)
            executor.run()
        self.pb.clean()

        self.suite.calculate()

        self.report()
        return self.suite.failures


class DescribeRunner(Runner):
    def run(self):
        self.setup()
        for task in self.suite.itertasks():
            print task.describe().encode('utf8')

        return 0


class ListOnlyRunner(Runner):
    def run(self):
        self.setup()
        for case in self.suite:
            print(case.fname)
            for task in case.tasks:
                print (u'   %s' % task).encode('utf8')

        return 0



# Local Variables:
# mode: flyspell-prog
# ispell-local-dictionary: "american"
# End:
