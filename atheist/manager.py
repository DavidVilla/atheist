# -*- mode: python; coding: utf-8 -*-

# atheist
#
# Copyright (C) 2009-2011 David Villa Alises
#
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA


#import codecs, locale
#sys.stdout = codecs.getwriter(locale.getpreferredencoding())(sys.stdout)
#sys.stderr = codecs.getwriter(locale.getpreferredencoding())(sys.stderr)


import os
import logging
import sys
import inspect
import fnmatch
import signal
import socket
import threading
from argparse import ArgumentParser, RawDescriptionHelpFormatter, SUPPRESS

from pyarco.Type import tag

import atheist
import atheist.const as const
from atheist.utils import base_abspath, RelativePathConverter, cout_config, str2bool
import atheist.plugins

from atheist.log import Log
from atheist.conf import settings
from atheist.iface import Manager, Public

import atheist.condition as condition
import atheist.reporter as reporter

try:
    from fs.osfs import OSFS
except ImportError:
    from .fs import OSFS


class ReporterManager(list):
    def report_case(self, case):
        for r in self:
            Log.debug("reporting case '%s' to '%s'", case.name, r.dst)
            r.add_case(case)


class Configurator(object):
    def __init__(self, argv):
        self.argv = argv
        self.parser = ArgParser()

        settings.load_namespace(self.parser.parse_minimal(self.argv))
        settings.load_configs()

        arguments = self.parser.parse_known_args(args=self.argv)
        settings.load_namespace(arguments[0])

        settings.args = [base_abspath(x, settings.curdir) for x in settings.args]

        self.process_overrides()

        self.check_settings_sanity()

        self.configure_verbosity()
        self.configure_timetag()
        self.configure_UI()
        self.add_environment_info()

 #        if settings.log:
#            filelog = logging.FileHandler(settings.log)
##            filelog.setFormatter(formatter)
#            filelog.setLevel(logging.DEBUG)
#            Log.addHandler(filelog)

        Log.debug('Log level is %s' % logging.getLevelName(Log.level))

        self.load_exclude_patterns()

        settings.RunnerClass = self.select_runner()

        settings.reporters = ReporterManager()
        if not settings.quiet:
            settings.reporters.append((reporter.ConsoleReporter()))

        settings.fs = OSFS('/')
        settings.relpath = RelativePathConverter(root = settings.base_dir)
        settings.relpath.enable = (os.getcwd() == settings.relpath.root)

    def select_runner(self):
        if settings.describe:
            return atheist.DescribeRunner

        if settings.list_only:
            return atheist.ListOnlyRunner

        return atheist.ThreadPoolRunner

    def add_plugin_options(self, plugins):
        for plugin in plugins:
            plugin.add_arguments(self.parser)

        arguments = self.parser.parse_args(args=self.argv)
        settings.load_namespace(arguments)

        for plugin in plugins:
            plugin.config(self.parser)

    def check_settings_sanity(self):
        if settings.stdout and settings.out_on_fail:
            self.error("-o/--stdout and -f/--out-on-fail are incompatible options")

        if settings.stderr and settings.out_on_fail:
            self.error("-e/--stderr and -f/--out-on-fail are incompatible options")

        if settings.verbose and settings.quiet:
            self.error("-q/--quiet and -v/--verbose are incompatible options")

        if settings.time_tag and (settings.verbose == 0 or settings.quiet):
            self.error("-t/--time-tag requires some verbosity")

        if settings.workers < 0:
            self.error("-w/--workers can not be negative.")

        for path in settings.pluginpath:
            if not os.path.exists(path):
                self.error("plugin directory '{0}' does not exist".format(path))

    def process_overrides(self):
        try:
            overrides = settings.get_overrides()
        except ValueError, e:
            self.error(e)

        for key, value in overrides.items():
            if '.' not in key:
                self.process_parser_override(key, value)

            settings[key] = value

    def process_parser_override(self, key, value):
        action = self.parser.get_option('--' + key)
        if action is None:
            self.error("Not valid config key: '%s'" % key)

        if action.default != SUPPRESS:
            self.error("--config setting is forbbiden for '%s'." % key)

        if action.type is not None:
            type_ = action.type
        elif action.const is not None:
            type_ = action.const.__class__
        else:
            type_ = str

        if type_ == bool:
            type_ = str2bool

        value = type_(value)
        Log.debug("%s = %s (%s)", key, repr(value), type_.__name__)

        settings[key] = value

    def configure_verbosity(self):
        if settings.verbose:
            settings.report_detail = 10
            settings.cols = 0

        if settings.verbose == 1:
            settings.loglevel = logging.INFO
        elif settings.verbose >= 2:
            settings.loglevel = logging.DEBUG
        if settings.quiet:
            settings.loglevel = logging.ERROR

        Log.setLevel(settings.loglevel)

    def configure_timetag(self):
        if not settings.time_tag:
            return

        settings.timetag_fmt = '%(asctime)s '
        formatter = atheist.log.CapitalLoggingFormatter(\
            settings.timetag_fmt + '[%(levelcapital)s] %(message)s')
        Log.handlers[0].setFormatter(formatter)  # change default formatter

    def configure_UI(self):
        if not sys.stdout.isatty():
            settings.plain = True
            settings.disable_bar = True

    def load_exclude_patterns(self):
        settings.exclude = const.EXCLUDE
        if settings.ignore:
            settings.exclude += [x.strip() for x in settings.ignore.split(',')]

        Log.debug("excluding %s" % settings.exclude)

    def add_environment_info(self):
        # FIXME: add support to add this from config file or --config
        try:
            if not 'user' in settings:
                settings.user = os.getlogin()
        except OSError:
            settings.user = 'atheist'

        settings.host = socket.gethostname()
        settings.cmdline = str.join(' ', self.argv)

    def error(self, msg):
        self.parser.exit(status=2, message="atheist: %s\n" % msg)

    def is_cmdline_valid(self):
        if any([settings.create,
                settings.gen_template]):
            return True

        if not any([settings.args,
                    settings.inline,
                    settings.commands]):
            return False

        return True


class InternalAPI:
    '''Namespace for atheist API'''

    def __init__(self, mng):
        import atheist.task

        self.mng = mng

        self._temp_n = 0
        self._temp_lock = threading.Lock()

        self.symbols = [x[1] for x in inspect.getmembers(
                self, inspect.ismethod) if hasattr(x[1], 'public')]

        pluginpath = settings.pluginpath + \
            [os.path.join(os.path.dirname(atheist.plugins.__file__))]

        self.plugins = []
        for dname in pluginpath:
            self.plugins.extend(atheist.plugins.Loader(dname))

        self.symbols.extend([x for x in self.plugins
                             if issubclass(x, (atheist.Task,
                                               atheist.TaskFactory,
                                               condition.Condition))])

        for module in [atheist, atheist.task, atheist.condition]:
            for name, symbol in inspect.getmembers(module, inspect.isclass):
                if symbol in self.symbols:
                    continue

                if issubclass(symbol, Public):
                    self.symbols.append(symbol)

        atheist.Task._mng = mng

        self.scope = dict((s.__name__, s) for s in self.symbols)

#        self.scope = {}
#        for s in self.symbols:
#            self.scope[s.__name__] = s

        for key, val in atheist.KeyWords.flag_enum().items():
            self.scope[key] = val

    def get_Task_classes(self):
        return [x for x in self.symbols if inspect.isclass(x)
                and issubclass(x, (atheist.Task, atheist.TaskFactory))]

    def get_Condition_classes(self):
        return [x for x in self.symbols if inspect.isclass(x)
                and issubclass(x, condition.Condition)]

    # FIXME find in Taskcases.tasks...
    @tag('public')
    def get_task(self, _id):
        for t in self.mng.suite.itertasks():
            if t.tid == _id:
                return t

        Log.error("There is no task with id '%s'" % _id)
        return None

    @tag('public')
    def temp_name(self, prefix='', suffix=''):
        with self._temp_lock:
            self._temp_n += 1
            return os.path.join(const.ATHEIST_TMP, 'temp-{0}{1}{2}'.format(
                    prefix + '-' if prefix else '',
                    self._temp_n,
                    suffix if suffix else ''))


class Atheist(Manager):
    instances = 0

    class NoTaskDefined(Exception):
        pass

    def __init__(self, argv=[]):

        assert Atheist.instances == 0, "Just one manager"
        Atheist.instances = 1

        signal.signal(signal.SIGPIPE, signal.SIG_DFL)
        signal.signal(signal.SIGINT, self.abort_handler)
        signal.signal(signal.SIGTERM, self.abort_handler)

        self.aborted = False
        self.abort_observers = []

        self.suite = atheist.Suite()

        configurator = Configurator(argv or sys.argv[1:])

        self.api = InternalAPI(self)
        configurator.add_plugin_options(self.api.plugins)

        self.exec_env = self.api.scope
        self.exec_env['args'] = settings.task_args.split(',')

        os.chdir(settings.base_dir)
        cout_config(settings.plain)

        if not configurator.is_cmdline_valid():
            configurator.parser.print_help()
            sys.exit(1)

    def abort_handler(self, signum, frame):
        if self.aborted:
            return

        print
        Log.warning("C-c pressed!")
        self.abort()

    def abort(self):
        self.aborted = True
        for ob in self.abort_observers:
            ob()

    def reload(self):
        def create_inline(name, code):
            Log.debug("inline script: %s" % code)
            fname = os.path.join(const.ATHEIST_TMP, 'inline-%s.test' % name)
            with file(fname, 'w') as fd:
                fd.write(code)

            return fname

        self.suite = atheist.Suite()

        inlines = settings.inline[:]
        for cmd in settings.commands:
            inlines.append('Test("%s", shell=True)' % cmd)

        for i, code in enumerate(inlines):
            fname = create_inline(i, code)
            case = atheist.TaskCase(self, fname)
            case.generated.append(fname)
            self.append_case(case)

        args = settings.args
        files = [x for x in args if os.path.isfile(x)]
        dirs =  [x for x in args if os.path.isdir(x)]

        rest = set(args).difference(set(files).union(dirs))
        if rest:
            for f in rest:
                Log.warning("Ignored file: %s", settings.relpath(f))

        for f in files:
            self.process_files('', [f], filter_excluded=False)

        for d in dirs:
            self.process_directory(d)

        if self.suite.num_tasks == 0:
            raise self.NoTaskDefined()

    def excluded(self, fname):
        return any(fnmatch.fnmatch(fname, pattern) for pattern in settings.exclude)

    def append_case(self, case):
        self.suite.append(case)
        case.load()

    def process_files(self, root, files, filter_excluded=True):
        for f in sorted([x for x in files]):
            if filter_excluded and (self.excluded(f) or not f.endswith(const.ATHEIST_EXT)):
                continue

#            if os.path.split(f)[1] in [DIR_SETUP, DIR_TEARDOWN]:
#                continue

            fullname = os.path.join(root, f)
            if not os.path.exists(fullname):
                Log.warning("No such file or directory: '%s', skipped.   ", fullname)
                continue

            self.append_case(atheist.TaskCase(self, fullname))

    def process_directory(self, dname):
        for root, dirs, files in os.walk(dname, followlinks=True):
            dirs.sort()
            Log.info("Entering directory '%s'" % settings.relpath(root))

            if not settings.skip_hooks:
                dir_setup = os.path.join(root, const.DIR_SETUP)
                if os.path.exists(dir_setup):
                    self.append_case(atheist.TaskCase(self, dir_setup, {},
                                                      atheist.task_mode.DIR_SETUP))

            self.process_files(root, files)

            if not settings.skip_hooks:
                dir_teardown = os.path.join(root, const.DIR_TEARDOWN)
                if os.path.exists(dir_teardown):
                    self.append_case(atheist.TaskCase(self, dir_teardown, {},
                                                      atheist.task_mode.DIR_TEARDOWN))

            for d in dirs[:]:
                if self.excluded(d):
                    dirs.remove(d)

    def run(self, runner_cls=None):
        if settings.gen_template:
            print atheist.file_template()
            return 0

        if settings.create:
            atheist.create_new_test()
            return 0

        if settings.clean_only or settings.clean_first:
            atheist.remove_files_and_dirs(atheist.get_generated_from_finished_atheists())
            if settings.clean_only:
                return 0

        atheist.assure_tmp_dir()

        if runner_cls == None:
            runner_cls = settings.RunnerClass

        retval = 0

        try:
            runner = runner_cls(self)
            retval = runner.run()
        except self.NoTaskDefined:
            Log.warning("No testfiles found.")
            retval = 1
        except atheist.ExecutionError:
            retval = 1

#        self.clean_async(
#            os.path.join(ATHEIST_TMP_BASE, self.cfg.pid),
#            [ATHEIST_TMP])

        if not settings.dirty:
            atheist.remove_files_and_dirs([const.ATHEIST_TMP])

        return retval

    def clean_async(self, fname, generated):
        if settings.dirty:
            atheist.save_dirty_filelist(fname + '.gen', generated)
        else:
            atheist.remove_files_and_dirs(generated)


class ArgParser(ArgumentParser):
    def __init__(self, *args, **kargs):
        ArgumentParser.__init__(
            self,
            prog = 'atheist',
            description = const.USAGE,
            epilog = '.',
            formatter_class = RawDescriptionHelpFormatter,
            argument_default = SUPPRESS,
            )

        self.plugin_group = self.add_argument_group(title="Plugin arguments")

    def parse_minimal(self, argv):
        self.add_argument('--config', metavar="key=val", action="append", default=[],
                          help="set/override config option (use '[section.]key=value')")
        self.add_argument('--config-file', metavar='FILE', default=None, help="Alternate config file")
        ns = ArgumentParser.parse_known_args(self, argv)[0]

        self.add_basic_arguments()
        self.add_output_arguments()

        return ns

    def add_basic_arguments(self):
        # FIXME: nargs should be + or *(0) depending on Runner
        self.add_argument(
            'args', nargs='*', default=None,
            help="files or directories")

        self.add_argument(
            '-a', '--task-args', metavar='ARGS',
            help='Colon-separated options for the tasks')

        self.add_argument(
            '-b', '--base-dir', metavar='PATH',
            help='Set working directory')

        # FIXME: clean-first by default???
        self.add_argument(
            '-C', '--clean-first', action='store_true',
            help="Remove previously generated files, then execute tasks")

        self.add_argument(
            '--clean-only', action='store_true',
            help="Execute nothing, only remove previously generated files")

        self.add_argument(
            '--create', metavar='FILE',
            help="Create and edit a new taskcase file template with default values")

        self.add_argument(
            '-x', '--exec', dest='commands', metavar='COMMAND', action='append', default=[],
            help='Equivalent to "-s Test(COMMAND, shell=True)"')

        # YAGNI?
        # FIXME: a test for this
        self.add_argument(
            '--ignore', metavar='PATTERN',
            help="Test files to ignore (glob patterns) separated with semicolon")

        self.add_argument(
            '-k', '--keep-going', action='store_true',
            help='Continue even with failed tasks')

        self.add_argument('-p', '--plugin-dir', dest='pluginpath', metavar='PATH',
                           action='append', default=[],
                           help='A directory containing plugins')

        self.add_argument(
            '-r', '--random', metavar='SEED', type=int,
            help='Shuffle taskcases using the specified seed (0:random seed)')

        self.add_argument(
            '-s', '--script', dest='inline', metavar='INLINE', action='append', default=[],
            help='Specifies an inline script')

        self.add_argument(
            '--save-stdout', action='store_true',
            help="Save stdout of all tasks")

        self.add_argument(
            '--save-stderr', action='store_true',
            help="Save stderr of all tasks")

        self.add_argument('-j', '--skip-hooks', action='store_true',
                          help='Skip _setup and _teardown files')

        self.add_argument('--timeout-scale', type=float,
                          help='Multiply task timeout by this factor')

        self.add_argument(
            '-w', '--workers', metavar='WORKERS', type=int,
            help='Number of simultaneous tasks (deafult:1) 0:auto-select')

    def add_output_arguments(self):
        group = self.add_argument_group(title='Output options')

        group.add_argument('--case-time',  action='store_true',
                           help="Print case execution time in reports")

        group.add_argument('--cols',  metavar='WIDTH', type=int,
                           help="Terminal width (in chars). Use 0 for unlimited.")

        group.add_argument('-d', '--describe', action='store_true',
                           help='Execute nothing, only describe tasks')

        group.add_argument('--disable-bar', action='store_true',
                           help="Do not show progress bar")

        group.add_argument('--dirty', action='store_true',
                           help="Do not remove generated files after task execution")

        # FIXME: test this
        group.add_argument('--fails-only', action='store_true',
                           help='Only failed task cases are included in the report')

        group.add_argument('-g', '--gen-template', action='store_true',
                           help='Generate a taskcase file template with default values')

        group.add_argument('-l', '--list-only', action='store_true',
                           help='List tasks but do not execute them')

        group.add_argument('-f', '--out-on-fail', action='store_true',
                           help='Print task output (stout/stderr) but only if it fail')

        group.add_argument('--plain', action='store_true',
                           help="Avoid color/escape codes in console output")

        group.add_argument('-q', '--quiet', action='store_true',
                           help='Do not show result summary nor warnings, only totals')

        # FIXME: test this
        group.add_argument(
            '-i', '--report-detail', metavar='LEVEL', type=int,
            help='Report verbosity (0:nothing, [1:case], 2:task, 3:composite, 4:condition. 5:auto/subcondition)')

        group.add_argument('--save-logs', action='store_true',
                           help="Save task logs on files")

        group.add_argument('-e', '--stderr', action='store_true',
            help='Print task process stderr')

        group.add_argument('-o', '--stdout', action='store_true',
                           help='Print task stdout')

        group.add_argument('-t', '--time-tag', action='store_true',
                           help='Include time info in the logs')

        group.add_argument('-v', '--verbose', action='count',
                           help='Incresse verbosity')

    # FIXME: rename to has_argument or has_action
    def has_option(self, prefix):
        return self.get_option(prefix) is not None

    def get_option(self, prefix):
        for action in self._actions:
            if prefix in action.option_strings:
                return action

        return None

    def get_all_prefixes(self):
        retval = []
        for group in self._action_groups:
            for action in group._group_actions:
                retval.extend(action.option_strings)

        return retval




## YAGNI?
##        self.add_option('--log', default=None,
##                          help="Log to specified filename")
##        self.add_option('-x', '--extension', dest='ext',
##                          default='.test',
##                          help='File extension for the task files')
