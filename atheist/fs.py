#!/usr/bin/python
# -*- coding:utf-8; tab-width:4; mode:python -*-

'''
This is a minimal version of the pyfilesystem OSFS class to avoid that
dependency. fs/osfs/__init__.py
'''


import sys
import os
import errno
import datetime
from functools import wraps


class ResourceError(Exception):
    def __init__(self, *args, **kargs):
        self.args = args

    def __str__(self):
        return '<{0}: {1}>'.format(self.__class__.__name__, str.join('-', self.args))


class ResourceNotFoundError(ResourceError): pass
class DirectoryNotEmptyError(ResourceError): pass
class DestinationExistsError(ResourceError): pass
class ResourceInvalidError(ResourceError): pass
class UnsupportedError(ResourceError): pass
class StorageSpaceError(ResourceError): pass
class PermissionDeniedError(ResourceError): pass
class ResourceLockedError(ResourceError): pass
class OperationFailedError(ResourceError): pass


def isprefix(path1, path2):
    """Return true is path1 is a prefix of path2.

    :param path1: An FS path
    :param path2: An FS path

    >>> isprefix("foo/bar", "foo/bar/spam.txt")
    True
    >>> isprefix("foo/bar/", "foo/bar")
    True
    >>> isprefix("foo/barry", "foo/baz/bar")
    False
    >>> isprefix("foo/bar/baz/", "foo/baz/bar")
    False

    """
    bits1 = path1.split("/")
    bits2 = path2.split("/")
    while bits1 and bits1[-1] == "":
        bits1.pop()
    if len(bits1) > len(bits2):
        return False
    for (bit1, bit2) in zip(bits1, bits2):
        if bit1 != bit2:
            return False
    return True


def convert_os_errors(func):
    """Function wrapper to convert OSError/IOError instances into FSErrors."""
    opname = func.__name__

    @wraps(func)
    def wrapper(self, *args, **kwds):
        try:
            return func(self, *args, **kwds)
        except (OSError, IOError), e:
            (exc_type, exc_inst, tb) = sys.exc_info()
            path = getattr(e, "filename", None)
            if path and path[0] == "/" and hasattr(self, "root_path"):
                path = os.path.normpath(path)
                if isprefix(self.root_path, path):
                    path = path[len(self.root_path):]
            if not hasattr(e, "errno") or not e.errno:
                raise OperationFailedError(opname, details=e), None, tb
            if e.errno == errno.ENOENT:
                raise ResourceNotFoundError(path, opname=opname, details=e), None, tb
            if e.errno == errno.ESRCH:
                raise ResourceNotFoundError(path, opname=opname, details=e), None, tb
            if e.errno == errno.ENOTEMPTY:
                raise DirectoryNotEmptyError(path, opname=opname, details=e), None, tb
            if e.errno == errno.EEXIST:
                raise DestinationExistsError(path, opname=opname, details=e), None, tb
            if e.errno == 183:  # some sort of win32 equivalent to EEXIST
                raise DestinationExistsError(path, opname=opname, details=e), None, tb
            if e.errno == errno.ENOTDIR:
                raise ResourceInvalidError(path, opname=opname, details=e), None, tb
            if e.errno == errno.EISDIR:
                raise ResourceInvalidError(path, opname=opname, details=e), None, tb
            if e.errno == errno.EINVAL:
                raise ResourceInvalidError(path, opname=opname, details=e), None, tb
            if e.errno == errno.EOPNOTSUPP:
                raise UnsupportedError(opname, details=e), None, tb
            if e.errno == errno.ENOSPC:
                raise StorageSpaceError(opname, details=e), None, tb
            if e.errno == errno.EPERM:
                raise PermissionDeniedError(opname, details=e), None, tb
            if e.errno == errno.EACCES:
                if sys.platform == "win32":
                    if e.args[0] and e.args[0] == 32:
                        raise ResourceLockedError(path, opname=opname, details=e), None, tb
                raise PermissionDeniedError(opname, details=e), None, tb
            # Sometimes windows gives some random errors...
            if sys.platform == "win32":
                if e.errno in (13, ):
                    raise ResourceInvalidError(path, opname=opname, details=e), None, tb
            raise OperationFailedError(opname, details=e), None, tb
    return wrapper


@convert_os_errors
def _os_stat(path):
    """Replacement for os.stat that raises FSError subclasses."""
    return os.stat(path)


class OSFS:
    def __init__(self, root_path):
        self.root_path = root_path

    def getsyspath(self, path):
        path = os.path.normpath(path)

        if os.path.isabs(path):
            return path

        return os.path.join(self.root_path, path)

    def exists(self, path):
        return os.path.exists(self.getsyspath(path))

    def isdir(self, path):
        return os.path.isdir(self.getsyspath(path))

    @convert_os_errors
    def getcontents(self, path):
        try:
            with open(self.getsyspath(path), 'rb') as fd:
                return fd.read()
        except IOError:
            raise ResourceNotFoundError(path)

    def _stat(self, path):
        """Stat the given path, normalising error codes."""
        sys_path = self.getsyspath(path)
        try:
            return _os_stat(sys_path)
        except ResourceInvalidError:
            raise ResourceNotFoundError(path)

    @convert_os_errors
    def getinfo(self, path):
        stats = self._stat(path)
        info = dict((k, getattr(stats, k)) for k in dir(stats) if not k.startswith('__'))
        info['size'] = info['st_size']
        #  TODO: this doesn't actually mean 'creation time' on unix
        ct = info.get('st_ctime', None)
        if ct is not None:
            info['created_time'] = datetime.datetime.fromtimestamp(ct)
        at = info.get('st_atime', None)
        if at is not None:
            info['accessed_time'] = datetime.datetime.fromtimestamp(at)
        mt = info.get('st_mtime', None)
        if mt is not None:
            info['modified_time'] = datetime.datetime.fromtimestamp(mt)
        return info
