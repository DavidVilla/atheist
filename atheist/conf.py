# -*- coding:utf-8; tab-width:4; mode:python -*-

import os

import configobj

from pyarco.Type import DictPathDeco, load_module_as_dict, Bunch

from atheist.log import Log
from atheist import defaults
import atheist.const as const


class Settings(Bunch):
    def __init__(self):
        super(Settings, self).__init__()
        self.configs = [
            ('/etc/default/atheist', Log.debug, Log.debug),
            (const.ATHEIST_CFG, Log.debug, Log.debug)]

        curdir = os.path.abspath(os.getcwd())
        items = curdir.split(os.sep)
        for i in range(2, len(items) + 1):
            cfg = os.path.join(*(['/'] + items[1:i] + ['.atheist.cfg']))
            self.configs.append((cfg, Log.info, Log.debug))

    def safe_get(self, key, default):
        try:
            return self[key]
        except KeyError:
            return default

    def load_namespace(self, ns):
        config_file = getattr(ns, 'config_file', None)
        if config_file is not None:
            self.configs.append((config_file, Log.info, Log.error))

        self.update(ns.__dict__)

    def load_configs(self):
        for cfg, ok_log, er_log in self.configs:
            try:
                self.load_from_config(cfg)
                ok_log("Loaded config file: %s", cfg)
            except IOError:
                er_log("Config file not found: %s", cfg)

    def load_from_config(self, fname):
        cobj = configobj.ConfigObj(fname, list_values=False, file_error=True)
        self.update(cobj)
        self.update(cobj.get('defaults', {}))

    def get_overrides(self):
        retval = {}
        for override in self.config:
            try:
                key, value = override.split('=')
            except ValueError:
                raise ValueError("Malformed --config option: '%s' (use --config key=value)" % override)

            retval[key] = value

        return retval


settings = DictPathDeco(Settings())
settings.update(load_module_as_dict(defaults))
