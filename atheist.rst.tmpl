=======
atheist
=======

--------------------------------
a general prupose test framework
--------------------------------

:Author: David.Villa@uclm.es
:date:   2011-08-08
:Manual section: 1


SYNOPSIS
========

{{ parser.format_usage() }}


ESCRIPTION
===========

``atheist`` allows you to specify the behaviour of your tests in a declarative way using
many data structures (such us Tasks, Conditions, Daemons, etc.) and provides detailed
statistic results. The main idea is that the programmer may describes the test behaviour
in a *.test* file and ``atheist`` runs every tests found. It is possible to check and
prepare the system by writting setup and teardown files that are executed after and before
each test.

Each test is represented by Task object which have many configuration attributes such us
execution delay, termination signal expected and many others. Tasks have pre and post
conditions that are been checked at execution time. You may save the test output and
compose Conditions objects to assure concrete states of the system during the tests
execution.

``atheist`` free all resources used by the user automatically at the end of the
execution. See the Atheist manual for more information about data structures and tools
provided by the framework.


OPTIONS
=======

This program follows the usual GNU command line syntax, with long
options starting with two dashes ('-'). A summary of options is
included below. For a complete description, see the Info files.

{% for group in parser._action_groups[1:] -%}
{{ group.title }}
{{ '-' * group.title|length }}
{% for opt in group._group_actions -%}
{% set short = opt.option_strings[0] if opt.option_strings|length == 2 else None %}
{% set long = opt.option_strings[1] if opt.option_strings|length == 2 else opt.option_strings[0] %}
{{ "%s%s%s%s%s"|format(
	short|default('', true),
 	' ' + opt.metavar if short and opt.metavar else '',
 	', ' if short and long else '',
 	long|default('', true),
 	' ' + opt.metavar if opt.metavar) }}

   {{opt.help}}
{% endfor %}
{% endfor %}



.. Local Variables:
..  coding: utf-8
..  mode: flyspell
..  ispell-local-dictionary: "american"
.. End:
